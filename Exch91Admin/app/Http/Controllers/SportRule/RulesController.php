<?php

namespace App\Http\Controllers\SportRule;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Sport;

class RulesController extends Controller
{   
    public static function getSportList()
    {
        $result = DB::table('tbl_sport')->select('*')->whereIn('status',[1,0])->orderBy('id', 'DESC')->limit(1000)->get();
        if (!empty($result)) {
            return response()->json(['status' => 1,'data'=>$result]);
        } else {
            return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again or contact admin!']]);
        }
    }
    public function showsportrulelist(Request $request)
    {
        $user = Auth::user();
        if($user->roleName != 'ADMIN'){
            return response()->json(['status' => 0, 'success' => ["message" => "Permission Denied!"]]);
        }
        $result= DB::connection('mysql2')->table('tbl_rules')->select('*')->whereIn('status',[1,0])->orderBy('id', 'DESC')->limit(1000)->get();
        if (!empty($result)) {
            return response()->json(['status' => 1,'data'=>$result]);
        } else {
            return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again or contact admin!']]);
        }
    }

    public function showsportrule($id)
    {
        $user = Auth::user();
        if($user->roleName != 'ADMIN'){
            return response()->json(['status' => 0, 'success' => ["message" => "Permission Denied!"]]);
        }
        $result= DB::connection('mysql2')->table('tbl_rules')->select('*')->where('id',$id)->first();
        if (!empty($result)) {
            return response()->json(['status' => 1,'data'=>$result]);
        } else {
            return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again or contact admin!']]);
        }
    }

    public static function getRules($id)
    {
        return DB::connection('mysql2')->table('tbl_rules')->select('*')->where('id',$id)->first();
    }

    public function createnewrule(Request $request)
    {
        $user = Auth::user();
        if($user->roleName != 'ADMIN'){
            return response()->json(['status' => 0, 'success' => ["message" => "Permission Denied!"]]);
        }
        if(!empty($request->category_name) && !empty($request->sub_category)) {

        $List = [
            'category_name' => $request->category_name,
            'sub_category' => $request->sub_category,
            'rule' => $request->rule
        ];

        $rs=null;
        if(isset($request->id) && $request->id !==0) {
            DB::connection('mysql2')->table('tbl_rules')->where('id', $request->id)->update($List);
            $data['title'] = 'Update Rule';
            $data['description'] = 'Update Rule('.$request->id.') '.$request->sub_category;
            return response()->json(['status' => 1, 'success' => ["message" => "Rules update successfully!"]]);

        }else{
            $List['status'] = 1;
            $data['title'] = 'Add New Rule';
            $data['description'] = 'Add Rule('.$request->category_name.') '.$request->sub_category;
            DB::connection('mysql2')->table('tbl_rules')->insert($List);
            return response()->json(['status' => 1, 'success' => ["message" => "Rules added successfully!"]]);
        }

        return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again or contact admin!']]);

        }else{
            return response()->json(['status' => 400, 'error' => ["message" => 'Bad request!']]);
        }
    }

    public function changerulestatus(Request $request)
    {
        $user = Auth::user();
        if($user->roleName != 'ADMIN'){
            return response()->json(['status' => 0, 'success' => ["message" => "Permission Denied!"]]);
        }
        $id= $request->id;
        $status= $request->status;
        $sportList= $this->changestatus($id,$status);
        if (!empty($sportList)) {
            return response()->json(['status' => 1,'data'=>$sportList, 'success' => ["message" => "Rules status change successfully!"]]);
        } else {
            return response()->json(['status' => 0, 'error' => ["message" => 'failed , please try again or contact admin!']]);

        }
    }

    public static function changestatus($id,$sportstatus)
    {
        $status=1;
        if($sportstatus=='Active'){ $status=0;}
        return DB::connection('mysql2')->table('tbl_rules')->where('id',$id)->update(['status' => $status]);
    }
}
