<?php

namespace App\Http\Controllers\Dashboard;
use App\Http\Controllers\Controller;
use App\Models\MainMenu;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainMenuController extends Controller
{

    /**
     * action list
     */
    public function list()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $list = MainMenu::getList(); $user = Auth::user();
            $onLineClient = DB::table('tbl_user')->select(['id'])
                ->where([['is_login',1],['status',1],['role',4]])->count();

            $data = ['list' => $list, 'onLineClient' => $onLineClient ];

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
