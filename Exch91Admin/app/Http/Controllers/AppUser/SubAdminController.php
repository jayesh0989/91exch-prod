<?php

namespace App\Http\Controllers\AppUser;

use App\Http\Controllers\Controller;
use App\Models\SubAdmin;
use Illuminate\Http\Request;

class SubAdminController extends Controller
{

    /**
     * SubAdmin Manage List
     * Action - Get
     * Created at FEB 2021 by 91exch
     */
    public function manage()
    {
        try{
            $data = SubAdmin::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * SubAdmin Reference List
     * Action - Get
     * Created at FEB 2021 by 91exch
     */
    public function reference($id)
    {
        try{
            $data = SubAdmin::getList($id);

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }else{
                $response = [ 'status' => 1, 'data' => [] ];
            }

            return response()->json($response, 200);
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * SubAdmin Create
     * Action - Post
     * Created at FEB 2021 by 91exch
     */
    public function create(Request $request)
    {
        try{
            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Bad request!' ] ];
                return $response; exit;
            }
            $response = SubAdmin::create($request->input());
            if( $response['status'] == 1 ) {
                $data['title'] = 'Create User';
                $data['description'] = 'New sub admin ( ' . $request->username . ' ) created';
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
