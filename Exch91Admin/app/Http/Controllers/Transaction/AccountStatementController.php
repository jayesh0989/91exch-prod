<?php

namespace App\Http\Controllers\Transaction;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;


class AccountStatementController extends Controller
{

    public function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }

    /**
     * mergeTable
     */
    public function mergeTable($requestData, $role){

        if( $role == 1 || $role == 6 ){
            $tbl = 'tbl_transaction_admin';
        }elseif ( $role == 4 ){
            $tbl = 'tbl_transaction_client';
        }else{
            $tbl = 'tbl_transaction_parent';
        }

        if( isset($requestData[ 'start' ]) && isset($requestData[ 'end' ]) ){
            $sd = explode('-',trim($requestData[ 'start' ]));
            $ed = explode('-',trim($requestData[ 'end' ]));
            $td = explode('-',trim(Carbon::now()->format('Y-m-d')));
            if( count($td) == 3 && count($sd) == 3 && count($ed) == 3){
                $sm = $sd[1]; $em = $ed[1]; $tm = $td[1];
                $sy = $sd[0]; $ey = $sd[0]; $ty = $td[0];
                $smy = $sm.$sy; $emy = $em.$ey; $tmy = $tm.$ty;

                if( $smy == $emy && $smy == $tmy ){
                    $tbl1 = $tbl.'_'.$tmy;
                    $tbl2 = null;
                }elseif($smy != $emy && $emy == $tmy){
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$tmy;
                }else{
                    $tbl1 = $tbl.'_'.$smy;
                    $tbl2 = $tbl.'_'.$emy;
                }
            }else{
                $tbl = $this->currentTable($tbl);
                $tbl1 = $tbl;
                $tbl2 = null;
            }
        }else{
            $tbl = $this->currentTable($tbl);
            $tbl1 = $tbl;
            $tbl2 = null;
        }

        return ['tbl1' => $tbl1,'tbl2' => $tbl2];
    }

    /**
     * action list
     */
    public function list($uid = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $setting = DB::table('tbl_common_setting')->select('value')
                ->where([['key_name','ACCOUNT_STATEMENT_BLOCK'],['status',1]])->first();

            if ( $setting != null && $setting->value == 0 ) {
                $response = [ 'status' => 1, 'data' => [], 'message' => 'Data not available on this moment ! Please try later !!' ];
                return response()->json($response);
            }

            if( $uid != null ){
                if( $uid == 1 && Auth::id() != 1){ return response()->json($response); }
                $user = DB::table('tbl_user')->where([['id',$uid],['status',1]])->first();
            }else{
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );
            $eType = 0; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                if( $requestData[ 'type' ] == 'settlement' ){
                    $eType = 2;
                }
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);

                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                if( $requestData[ 'type' ] == 'settlement' ){
                    $eType = 2;
                }
                $now = Carbon::now();
                if( $requestData[ 'ftype' ] == 'week' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(7)->format('Y-m-d');
                }elseif ( $requestData[ 'ftype' ] == 'month' ){
                    $end = $now->format('Y-m-d');
                    $start = $now->subDays(30)->format('Y-m-d');
                }else{
                    $end = $start = $now->format('Y-m-d');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( isset($requestData[ 'type' ]) && $requestData[ 'type' ] == 'admin-settlement' ){

                if( $user != null && $user->roleName == 'ADMIN' ){
                    $tbl = 'tbl_transaction_admin_settlement';

                    $query = DB::table($tbl)->where([['status',1]]);

                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }else{
                        $now = Carbon::now();
                        $end = $start = $now->format('Y-m-d');
                        $start = Carbon::parse($start); $end = Carbon::parse($end);
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }

                    $listArr = $query->orderBy('id','desc')->get();

                    if( $listArr != null ){
                        $transArr = [];
                        foreach ( $listArr as $list ) {
                            $amount = $list->amount;
                            $transArr[] = [
                                'description' => $list->description,
                                'balance' => '-',
                                'remark' => $list->remark,
                                'updated_on' => $list->updated_on,
                                'amount' => $amount,
                                'status' => $list->status,
                            ];
                        }

                        $adminSettlementAmount = 0;
                        $adminSettlement = DB::table('tbl_transaction_admin_settlement')
                            ->where([['userId', $user->id], ['status', 1]])->sum('amount');
                        if ( $adminSettlement != null) {
                            $adminSettlementAmount = round($adminSettlement);
                        }

                        $response = [ 'status' => 1, 'data' => $transArr , 'total' => $adminSettlementAmount ];
                    }

                }

            }else if( isset($requestData[ 'type' ]) && $requestData[ 'type' ] == 'wl-settlement' ){

                if( $user != null && ( ( $user->role == 1 && $user->roleName == 'ADMIN' ) || $user->role == 6 ) ){
                    $userName = $user->username; $eType = 2;
                    $tbl = 'tbl_transaction_admin';
                    $tbl = $this->currentTable($tbl);
                    $where = [['amount','!=',0],['userId',$uid],['eType',$eType],['systemId','!=',1]];
                    $query = DB::connection('mysql3')->table($tbl)
                        ->select(['mid','description','balance','current_balance','eType','remark','updated_on','amount','type','status'])
                        ->where($where);
                    if( $searchDate == true && $start != null && $end != null ){
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }else{
                        $now = Carbon::now();
                        $end = $start = $now->format('Y-m-d');
                        $start = Carbon::parse($start); $end = Carbon::parse($end);
                        $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                            ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                    }
                    $listArr = $query->orderBy('id','desc')->get();
                    if( $listArr != null ){
                        $transArr = [];
                        foreach ( $listArr as $list ) {
                            $amount = $list->amount;
                            if ($list->type != 'CREDIT') {
                                $amount = (-1) * $list->amount;
                            }
                            if($list->status != 1){
                                $list->description = $list->description. ' ( Canceled ) ';
                            }

                            if( $list->current_balance != 0 && $user->role == 4 ){
                                $list->balance = $list->current_balance;
                            }

                            $transArr[] = [
                                'description' => $list->description,
                                'balance' => $list->balance,
                                'remark' => $list->remark,
                                'updated_on' => $list->updated_on,
                                'amount' => $amount,
                                'status' => $list->status,
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $transArr ];

                    }
                }

            }else{
                if( $user != null ){

                    $userName = $user->username;
                    $tbls = $this->mergeTable($requestData,$user->role);
                    $tbl1 = $tbls['tbl1'];
                    if( strpos($tbl1,'_012021') > 0 ){
                        $tbl1 = str_replace('_012021','_022021',$tbl1);
                    }
                    $tbl2 = $tbls['tbl2'];
                    if( $user->role == 1 || $user->role == 6 ){
                        $where = [['userId',$uid]];
                    }elseif ( $user->role == 4 ){
                        $where = [['userId',$uid],['systemId',$user->systemId]];
                    }else{
                        $where = [['userId',$uid],['systemId',$user->systemId]];
                    }
                    array_push($where,['amount','!=',0]);
                    if( $eType != 0 ){
                        array_push($where,['eType',$eType]);
                    }

                    $query1 = DB::connection('mysql3')->table($tbl1)
                        ->select(['mid','description','balance','current_balance','eType','remark','updated_on','amount','type','status'])
                        ->where($where);

                    if( $eType == 0 && $user->role != 4 ){
                        $query1->whereIn('eType',[0,3]);
                    }

                    if( $searchDate == true && $start != null && $end != null ){
                        $query1->whereBetween('updated_on',[$start->format('Y-m-d').' 00:00:01',$end->format('Y-m-d').' 23:59:59']);
                    }else{
                        $now = Carbon::now();
                        $end = $start = $now->format('Y-m-d');
                        $start = Carbon::parse($start); $end = Carbon::parse($end);
                        $query1->whereBetween('updated_on',[$start->format('Y-m-d').' 00:00:01',$end->format('Y-m-d').' 23:59:59']);
                    }

                    $listArr1 = $query1->orderBy('id','desc')->get();
                    $listArr = [];
                    if( $tbl2 != null ){
                        $query2 = DB::connection('mysql3')->table($tbl2)
                            ->select(['mid','description','balance','current_balance','eType','remark','updated_on','amount','type','status'])
                            ->where($where);

                        if( $eType == 0 && $user->role != 4 ){
                            $query2->whereIn('eType',[0,3]);
                        }

                        if( $searchDate == true && $start != null && $end != null ){
                            $query2->whereBetween('updated_on',[$start->format('Y-m-d').' 00:00:01',$end->format('Y-m-d').' 23:59:59']);
                        }else{
                            $now = Carbon::now();
                            $end = $start = $now->format('Y-m-d');
                            $start = Carbon::parse($start); $end = Carbon::parse($end);
                            $query2->whereBetween('updated_on',[$start->format('Y-m-d').' 00:00:01',$end->format('Y-m-d').' 23:59:59']);
                        }

                        $listArr2 = $query2->orderBy('id','desc')->get();
                        if($listArr2->isNotEmpty() && $listArr1->isNotEmpty()){
                            if($listArr2) {
                                foreach ( $listArr2 as $data ){
                                    $listArr[] = $data;
                                }
                            }
                            if($listArr1) {
                                foreach ( $listArr1 as $data ){
                                    $listArr[] = $data;
                                }
                            }
                        }elseif($listArr2->isNotEmpty() && $listArr1->isEmpty()){
                            $listArr = $listArr2;
                        }else{
                            $listArr = $listArr1;
                        }
                    }else{
                        $listArr = $listArr1;
                    }

                    if( !empty($listArr) ){

                        if( $eType == 2 ){
                            $transArr = [];
                            foreach ( $listArr as $list ) {
                                $amount = $list->amount;
                                if ($list->type != 'CREDIT') {
                                    $amount = (-1) * $list->amount;
                                }
                                if($list->status != 1){
                                    $list->description = $list->description. ' ( Canceled ) ';
                                }

                                if( $list->current_balance != 0 && $user->role == 4 ){
                                    $list->balance = $list->current_balance;
                                }

                                $transArr[] = [
                                    'description' => $list->description,
                                    'balance' => $list->balance,
                                    'remark' => $list->remark,
                                    'updated_on' => $list->updated_on,
                                    'amount' => $amount,
                                    'status' => $list->status,
                                ];
                            }

                            $response = [ 'status' => 1, 'data' => $transArr ];

                        }elseif( $eType == 0 ){
                            $transArr = [];
                            foreach ( $listArr as $list ) {
                                $amount = $list->amount;
                                if ($list->type != 'CREDIT') {
                                    $amount = (-1) * $list->amount;
                                }
                                if($list->status != 1){
                                    $list->description = $list->description. ' ( Canceled ) ';
                                }

                                if( $list->current_balance != 0 ){
                                    $list->balance = $list->current_balance;
                                }

                                $transArr[] = [
                                    'description' => $list->description,
                                    'balance' => round($list->balance,2),
                                    'remark' => $list->remark,
                                    'updated_on' => $list->updated_on,
                                    'amount' => round($amount,2),
                                    'status' => $list->status,
                                    'mid' => $list->mid,
                                ];
                            }
                            $response = [ 'status' => 1, 'data' => $transArr ];

                        }else{
                            $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                        }

                    }else{
                        $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                    }

                }
            }
            if( !isset($response['total']) ){
                $response['total'] = '';
            }

            $now = Carbon::now()->format('Y-m-d');
            if($now){
                $nowArr = explode('-',$now);
                if( count($nowArr) > 1 ){
                    if( $nowArr[1] != 1 ){
                        $m = $nowArr[1]-1;
                        $y = $nowArr[0];
                    }else{
                        $m = 12;
                        $y = $nowArr[0]-1;
                    }
                    if( $m < 10 ){ $m = '0'.$m; }
                    $response['lastMonthUrl'] = '/reports/account-statement-month/'.$y.'-'.$m;
                    $response['lastMonth'] = 'Account Statement of '.$m.'-'.$y;
                }
            }

            if(isset($user->username)){
                $response['userName'] = $user->username;
            }else{
                $response['userName'] = 'This User';
            }


            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * action month
     */
    public function month($d = null)
    {

        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $setting = DB::table('tbl_common_setting')->select('value')
                ->where([['key_name','ACCOUNT_STATEMENT_BLOCK'],['status',1]])->first();

            if ( $setting != null && $setting->value == 0 ) {
                $response = [ 'status' => 1, 'data' => [], 'message' => 'Data not available on this moment ! Please try later !!' ];
                return response()->json($response);
            }

            if( $d != '' ){
                $ym = $tp = '';
                $date = explode('-',$d);
                if( count($date) == 2 ){
                    $m = $date[1];
                    $y = $date[0];
                    $ym = $y.'-'.$m;
                    $tp = $m.$y;
                }else{
                    return $response;
                }
            }else{
                return $response;
            }

            $requestData = json_decode( file_get_contents('php://input') , JSON_FORCE_OBJECT );

            if( isset( $requestData[ 'uid' ] ) && $requestData[ 'uid' ] != null ){
                $user = DB::table('tbl_user')->where([['id',$requestData[ 'uid' ]],['status',1]])->first();
                $uid = $user->id;
            }else{
                $user = Auth::user(); $uid = $user->id;
                if( $user->role == 6 ){ $uid = 1; }
            }
            $eType = 0; $searchDate = false; $start = $end = null;
            if( !isset( $requestData[ 'ftype' ] ) && isset( $requestData[ 'type' ] ) && isset( $requestData[ 'start' ] ) && isset( $requestData[ 'end' ] ) ) {
                if( $requestData[ 'type' ] == 'settlement' ){
                    $eType = 2;
                }
                $start = Carbon::parse($requestData[ 'start' ]);
                $end = Carbon::parse($requestData[ 'end' ]);
                $searchDate = true;
            }

            if( isset( $requestData[ 'ftype' ] ) ) {
                if( $requestData[ 'ftype' ] == '2week' ){
                    $start = Carbon::parse($ym.'-8');
                    $end = Carbon::parse($ym.'-15');
                }elseif ( $requestData[ 'ftype' ] == '3week' ){
                    $start = Carbon::parse($ym.'-16');
                    $end = Carbon::parse($ym.'-22');
                }elseif ( $requestData[ 'ftype' ] == '4week' ){
                    $start = Carbon::parse($ym.'-23');
                    $end = Carbon::parse($ym.'-31');
                }else{
                    $start = Carbon::parse($ym.'-1');
                    $end = Carbon::parse($ym.'-7');
                }

                $start = Carbon::parse($start); $end = Carbon::parse($end);
                $searchDate = true;
            }

            if( isset($requestData[ 'type' ]) && $requestData[ 'type' ] == 'settlement' ){ $eType = 2;
            }else if( isset($requestData[ 'type' ]) && $requestData[ 'type' ] == 'chip-history' ){ $eType = 1; }

            if( $user != null ){
                $conn = DB::connection('mysql3');
                $userName = $user->username;
                if( $user->role == 1 || $user->role == 6 ){
                    $tbl = 'tbl_transaction_admin';
                    $where = [['userId',$uid],['systemId',$user->systemId]];
                }elseif( $user->role == 4 ){
                    $tbl = 'tbl_transaction_client';
                    $where = [['userId',$uid],['systemId',$user->systemId]];
                }else{
                    $tbl = 'tbl_transaction_parent';
                    $where = [['userId',$uid],['systemId',$user->systemId]];
                }
                array_push($where,['amount','!=',0]);
                if($tp != ''){
                    $tbl1 = $tbl.'_'.$tp;
                    if( Schema::connection('mysql3')->hasTable($tbl1) ){
                        $tbl = $tbl1;
                    }
                }

                if( $eType != 0 ){
                    array_push($where,['eType',$eType]);
                }

                $query = $conn->table($tbl)
                    ->select(['mid','description','balance','current_balance','eType','remark','updated_on','amount','type','status'])
                    ->where($where);

                if( $eType == 0 && $user->role != 4 ){
                    $query->whereIn('eType',[0,3]);
                }

                if( $searchDate == true && $start != null && $end != null ){
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }else{
                    $now = Carbon::now();
                    $end = $start = $now->format('Y-m-d');
                    $start = Carbon::parse($start); $end = Carbon::parse($end);
                    $query->whereDate('updated_on','<=',$end->format('Y-m-d'))
                        ->whereDate('updated_on','>=',$start->format('Y-m-d'));
                }

                $listArr = $query->orderBy('id','desc')->get();

                if( $listArr != null ){

                    if( $eType == 2 || $eType == 1 ){
                        $transArr = [];
                        foreach ( $listArr as $list ) {
                            $amount = $list->amount;
                            if ($list->type != 'CREDIT') {
                                $amount = (-1) * $list->amount;
                            }
                            if($list->status != 1){
                                $list->description = $list->description. ' ( Canceled ) ';
                            }

                            if( $eType == 2 ){ $list->balance = '-'; }

                            $transArr[] = [
                                'description' => $list->description,
                                'balance' => $list->balance,
                                'remark' => $list->remark,
                                'updated_on' => $list->updated_on,
                                'amount' => $amount,
                                'status' => $list->status,
                            ];
                        }

                        $response = [ 'status' => 1, 'data' => $transArr ];

                    }elseif( $eType == 0 ){
                        $transArr = [];
                        foreach ( $listArr as $list ) {
                            $amount = $list->amount;
                            if ($list->type != 'CREDIT') {
                                $amount = (-1) * $list->amount;
                            }
                            if($list->status != 1){
                                $list->description = $list->description. ' ( Canceled ) ';
                            }

                            if( $list->current_balance != 0 ){
                                $list->balance = $list->current_balance;
                            }

                            $transArr[] = [
                                'description' => $list->description,
                                'balance' => round($list->balance,2),
                                'remark' => $list->remark,
                                'updated_on' => $list->updated_on,
                                'amount' => round($amount,2),
                                'status' => $list->status,
                                'mid' => $list->mid,
                            ];
                        }
                        $response = [ 'status' => 1, 'data' => $transArr, 'userName' => $userName ];

                    }else{
                        $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                    }

                }else{
                    $response = [ 'status' => 1, 'data' => [], 'userName' => $userName ];
                }

            }
            if( !isset($response['total']) ){
                $response['total'] = '';
            }

            if( (int)$m != 1 ){
                $m = (int)$m-1;
            }else{
                $m = 12;
                $y = $y-1;
            }
            if( $m < 10 ){ $m = '0'.$m; }
            $response['lastMonthUrl'] = '/reports/account-statement-month/'.$y.'-'.$m;
            $response['lastMonth'] = 'Account Statement of '.$m.'-'.$y;

            if(isset($user->username)){
                $response['userName'] = $user->username;
            }else{
                $response['userName'] = 'This User';
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
