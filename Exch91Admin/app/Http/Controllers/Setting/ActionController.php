<?php

namespace App\Http\Controllers\Setting;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use DB;

class ActionController extends Controller
{
    /**
     * Manage
     */
    public function manage()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $data = Setting::getList();

            if( $data != null ){
                $response = [ 'status' => 1, 'data' => $data ];
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }

    /**
     * Create
     */
    public function create(Request $request)
    {
        try{

            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
                return $response; exit;
            }

            $this->validate($request, [
                'key' => 'required|min:5',
                'value' => 'required|min:1',
            ]);
            $reqData = $request->input();
            $response = Setting::doCreate($reqData);
            if( $response['status'] == 1 ){
                $this->updateSettingsInRedis();
                $data['title'] = 'Setting';
                $data['description'] = 'Setting Key Created: '.$reqData['key'];
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Update
     */
    public function update(Request $request)
    {
        try{

            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
                return $response; exit;
            }

            $this->validate($request, [
                'id' => 'required',
                // 'value' => 'required|min:1',
            ]);
            $reqData = $request->input();
            $response = Setting::doUpdate($reqData);
            if( $response['status'] == 1 ){
                $this->updateSettingsInRedis();
                $data['title'] = 'Setting';
                $data['description'] = 'Setting Key Updated: '.$response['key'];
                $this->addNotification($data);
            }
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function updateSettingsInRedis(){
        $cache       = Redis::connection();
        $settingData = DB::table('tbl_common_setting')->select('id','key_name', 'value')->where('status', 1)->get();
        $settingStatus = DB::table('tbl_common_setting')->select('value')->where([['key_name','MARKET_SETTING_STATUS']])->first();
        if(!empty($settingStatus)){
            $cache->set('MARKET_SETTING_STATUS',$settingStatus->value);
        }
        if ($settingData != null) {
            foreach ($settingData as $data) {
                if ($data->key_name == 'MATCHODD_SETTING') {
                    $cache->set('MATCHODD_SETTING',$data->value);
                }
                if ($data->key_name == 'VIRTUAL_CRICKET_SETTING') {
                    $cache->set('VIRTUAL_CRICKET_SETTING',$data->value);
                }
                if ($data->key_name == 'OTHERMARKET_SETTING') {
                    $cache->set('OTHERMARKET_SETTING',$data->value);
                }
                if ($data->key_name == 'ODDEVEN_SETTING') {
                    $cache->set('ODDEVEN_SETTING',$data->value);
                }
                if ($data->key_name == 'KHADO_SETTING') {
                    $cache->set('KHADO_SETTING',$data->value);
                }
                if ($data->key_name == 'METER_SETTING') {
                    $cache->set('METER_SETTING',$data->value);
                }
                if ($data->key_name == "BALLBYBALL_SETTING") {
                    $cache->set('BALLBYBALL_SETTING',$data->value);
                }
                if ($data->key_name == 'WINNER_SETTING') {
                    $cache->set('WINNER_SETTING',$data->value);
                }
                if ($data->key_name == 'BOOKMAKER_SETTING') {
                    $cache->set('BOOKMAKER_SETTING',$data->value);
                }
                if ($data->key_name == 'SESSION_SETTING') {
                    $cache->set('SESSION_SETTING',$data->value);
                }
                if ($data->key_name == 'BINARY_SETTING') {
                    $cache->set('BINARY_SETTING',$data->value);
                }
                if ($data->key_name == 'FOOTBALL_EVENT_SETTING') {
                    $cache->set('FOOTBALL_EVENT_SETTING',$data->value);
                }
                if ($data->key_name == 'TENNIS_EVENT_SETTING') {
                    $cache->set('TENNIS_EVENT_SETTING',$data->value);
                }
                if ($data->key_name == 'CRICKET_EVENT_SETTING') {
                    $cache->set('CRICKET_EVENT_SETTING',$data->value);
                }
                if ($data->key_name == 'UPCOMING_EVENT_SETTING') {
                    $cache->set('UPCOMING_EVENT_SETTING',$data->value);
                }
            }
        }
    }
    /**
     * Status
     */
    public function status(Request $request)
    {
        try{

            if( isset( $_SERVER['HTTP_POSTMAN_TOKEN'] ) ){
                $response =  [ "status" => 0 , "error" => [ "code" => 400 , "message" => "Bad request!" ] ];
                return $response; exit;
            }

            $this->validate($request, [
                'id' => 'required',
                'status' => 'required',
            ]);

            $response = Setting::doUpdate($request->input());
            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
