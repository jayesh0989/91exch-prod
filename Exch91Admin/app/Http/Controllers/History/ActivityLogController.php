<?php

namespace App\Http\Controllers\History;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ActivityLogController extends Controller
{

    /**
     * Activity Log List
     */
    public function list()
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong! Data not available on this moment !' ] ];

        try{

            $user = Auth::user();

            if( $user != null ){

                $now = Carbon::now();
                $end = $now->format('Y-m-d');
                $start = $now->subDays(7)->format('Y-m-d');
                $start = Carbon::parse($start); $end = Carbon::parse($end);

                $query = DB::table('tbl_user_activity_log')
                        ->whereDate('created_on','<=',$end->format('Y-m-d'))
                        ->whereDate('created_on','>=',$start->format('Y-m-d'));

                $list = $query->orderBy('created_on', 'DESC')->get();

                if( $list->isNotEmpty() ){
                    DB::table('tbl_user_activity_log')
                        ->where([['is_seen',0]])->update(['is_seen' => 1]);
                    $response = [ 'status' => 1, 'data' => $list ];
                }else{
                    $response = [ 'status' => 1, 'data' => [] ];
                }
            }

            return response()->json($response, 200);

        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

}
