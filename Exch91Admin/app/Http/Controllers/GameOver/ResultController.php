<?php

namespace App\Http\Controllers\GameOver;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ResultController extends Controller
{

    /**
     * action Insert
     */
    public function actionInsert(Request $request)
    {
        $response = [ 'status' => 0, 'message' => 'Error' ];
        try{
            $conn = DB::connection('mongodb');
            // RECALL
            if( isset($request->eventId) && isset($request->marketId) && isset($request->recall) && $request->recall == 1 ){
                $tbl = 'tbl_market_result';
                $where = [['eventId',$request->eventId],['marketId',$request->marketId]];
                $result = $conn->table($tbl)->where($where)->first();
                if( !empty($result) ){
                    $updateArr = [
                        'sportId' => (int)$request->sportId,
                        'game_over' => 1,
                        'recall' => 1,
                        'updated_on' => date('Y-m-d H:i:s'),
                        'status' => 1
                    ];
                    if( $conn->table($tbl)->where($where)->update($updateArr) ){
                        $response = [ 'status' => 1, 'message' => 'Success' ];
                    }else{
                        $response = [ 'status' => 0, 'message' => 'Error1' ];
                    }
                }else{
                    $response = [ 'status' => 0, 'message' => 'Error2' ];
                }

            }
            // ABUNDANT

            if( isset($request->eventId) && isset($request->marketId)
                && isset($request->mType) && isset($request->recall) && $request->recall == 2 ){

                $tbl = 'tbl_market_result';
                $where = [['eventId',$request->eventId],['marketId',$request->marketId]];
                $result = $conn->table($tbl)->where($where)->first();
                if( empty($result) ){
                    $insertArr = [
                        'sportId' => (int)$request->sportId,
                        'eventId' => (int)$request->eventId,
                        'marketId' => $request->marketId,
                        'secId' => 0,
                        'mType' => $request->mType,
                        'winner' => 'ABUNDANT',
                        'game_over' => 0,
                        'recall' => 2,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'status' => 1
                    ];
                    if( $conn->table($tbl)->insert($insertArr) ){
                        $response = [ 'status' => 1, 'message' => 'Success' ];
                    }else{
                        $response = [ 'status' => 0, 'message' => 'Error1' ];
                    }
                }else{
                    $updateArr = [
                        'winner' => 'ABUNDANT',
                        'game_over' => 0,
                        'recall' => 2,
                        'updated_on' => date('Y-m-d H:i:s'),
                    ];
                    if( $conn->table($tbl)->where($where)->update($updateArr) ){
                        $response = [ 'status' => 1, 'message' => 'Success' ];
                    }else{
                        $response = [ 'status' => 0, 'message' => 'Error1' ];
                    }
                }

            }
            // GAME OVER
            if( isset($request->sportId) && isset($request->eventId) && isset($request->marketId)
                && isset($request->secId) && isset($request->mType) && isset($request->winner) ){
                $tbl = 'tbl_market_result';
                $where = [['eventId',$request->eventId],['marketId',$request->marketId]];
                $result = $conn->table($tbl)->where($where)->first();

                if( empty($result) ){
                    $insertArr = [
                        'sportId' => (int)$request->sportId,
                        'eventId' => (int)$request->eventId,
                        'marketId' => $request->marketId,
                        'secId' => (int)$request->secId,
                        'mType' => $request->mType,
                        'winner' => $request->winner,
                        'game_over' => 0,
                        'recall' => 0,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s'),
                        'status' => 1
                    ];
                    if( $conn->table($tbl)->insert($insertArr) ){
                        $response = [ 'status' => 1, 'message' => 'Success' ];
                    }else{
                        $response = [ 'status' => 0, 'message' => 'Error1' ];
                    }
                }else{
                    $result = (object)$result;
                    if( $result->recall == 1 ){
                        $recall = 1;
                        $gameOver = 1;
                    }else{
                        $recall = 0;
                        $gameOver = 0;
                    }
                    $updateArr = [
                        'sportId' => (int)$request->sportId,
                        'secId' => (int)$request->secId,
                        'mType' => $request->mType,
                        'winner' => $request->winner,
                        'game_over' => $gameOver,
                        'recall' => $recall,
                        'updated_on' => date('Y-m-d H:i:s'),
                        'status' => 1
                    ];
                    if( $conn->table($tbl)->where($where)->update($updateArr) ){
                        $response = [ 'status' => 1, 'message' => 'Success' ];
                    }else{
                        $response = [ 'status' => 0, 'message' => 'Error1' ];
                    }
                }

            }
            return response()->json($response);

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
        }

    }

}
