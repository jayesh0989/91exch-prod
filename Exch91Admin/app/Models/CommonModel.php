<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;
use mysql_xdevapi\Exception;

class CommonModel extends Model
{

    public static function currentTable($tbl){
        $cTime = date('Y-m-d H:i:s');
        $y = date('Y', strtotime($cTime));
        $m = date('m', strtotime($cTime));
        return $tbl.'_'.$m.$y;
    }

    // update Sport Block
    public static function updateSportBlock($userId, $parentId)
    {
        try {
            $blockData = DB::table('tbl_user_sport_status')
                ->select(['byuserId','sid'])->where([['uid',$parentId]])->get();

            if( $blockData->isNotEmpty() ){
                foreach ( $blockData as $block ){
                    $sportId = $block->sid;
                    if( $parentId == $block->byuserId ){
                        $userData = ['uid' => $userId,'sid' => $sportId,'byuserId' => $parentId ];
                        DB::table('tbl_user_sport_status')->insert($userData);
                    }else{
                        self::updateSportBlock($userId,$block->byuserId);
                    }
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    // update balance
    public static function updateProfitLossData($systemId, $clientId, $userId, $parentId, $level, $pl = 0)
    {

        try {

            $tbl = 'tbl_user_profit_loss';

            if ($level == 0) {

                $clientProfitLoss = [
                    'systemId' => $systemId,
                    'clientId' => $clientId,
                    'userId' => $userId,
                    'parentId' => $parentId,
                    'level' => $level,
                    'profit_loss' => 0,
                    'actual_profit_loss' => 0,
                ];

                if (DB::table($tbl)->insert($clientProfitLoss)) {
                    $level = $level + 1;

                    $pUser = DB::table('tbl_user')
                        ->select(['parentId', 'role'])
                        ->where([['id', $parentId]])->first();
                    if ($pUser != null) {
                        $userId = $parentId;
                        $parentId = $pUser->parentId;
                        self::updateProfitLossData($systemId, $clientId, $userId, $parentId, $level, $pl);
                    }

                }

            } else {

                $user = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['parentId', 'pl', 'role'])
                    ->where([['u.id', $userId]])->first();

                if ($user != null) {
                    if ($user->role == 1 && $user->pl == 0) {
                        $profitLoss = 0;
                        $actualProfitLoss = 100;
                    } else {
                        $profitLoss = $user->pl;
                        $actualProfitLoss = $user->pl - $pl;
                    }

                    $parantProfitLoss = [
                        'systemId' => $systemId,
                        'clientId' => $clientId,
                        'userId' => $userId,
                        'parentId' => $parentId,
                        'level' => $level,
                        'profit_loss' => $profitLoss,
                        'actual_profit_loss' => $actualProfitLoss,
                    ];

                    if (DB::table($tbl)->insert($parantProfitLoss)) {
                        $level = $level + 1;
                        if ($user->role != 1) {

                            $pUser = DB::table('tbl_user')
                                ->select(['parentId', 'role'])
                                ->where([['id', $parentId]])->first();

                            $userId = $parentId;
                            $parentId = $pUser->parentId;
                            $pl = $profitLoss;
                            self::updateProfitLossData($systemId, $clientId, $userId, $parentId, $level, $pl);
                        }
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            }

        } catch (Exception $e) {
            return false;
        }
    }

    // update balance
    public static function updateChipTransaction($uid, $pid, $amount, $remark, $type)
    {

        try {

            $userData = DB::table('tbl_user as u')
                ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                ->select(['name', 'balance','pl_balance','role', 'u.systemId'])
                ->where([['u.id', $uid]])->first();

            if ($userData != null) {
                $userName = $userData->name;
                $userBalance = $userData->balance;
                $systemId = $userData->systemId;

                if ($userData->role == 4) {
                    $tbl1 = 'tbl_transaction_client';
                    $userBalance = $userData->balance+$userData->pl_balance;
                } else {
                    $tbl1 = 'tbl_transaction_parent';
                    $userBalance = $userData->balance;
                }
                $tbl1 = self::currentTable($tbl1);
                $parentData = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['name', 'parentId', 'balance', 'role'])
                    ->where([['u.id', $pid]])->first();

                if ($parentData != null) {
                    $parentName = $parentData->name;
                    $parentBalance = $parentData->balance;
                    $pid2 = $parentData->parentId;

                    if ($parentData->role == 1) {
                        $tbl2 = 'tbl_transaction_admin';
                    } else {
                        $tbl2 = 'tbl_transaction_parent';
                    }
                    $tbl2 = self::currentTable($tbl2);

                    if ($type == 'WITHDRAWAL') {
                        $uType = 'DEBIT';
                        $pType = 'CREDIT';
                        $mType = 'Withdrawal';
                        $uDesc = 'Chip Withdrawal By ' . $parentName;
                        $pDesc = 'Chip Withdrawal From ' . $userName;
                    } else {
                        $uType = 'CREDIT';
                        $pType = 'DEBIT';
                        $mType = 'Deposit';
                        $uDesc = 'Chip Deposit By ' . $parentName;
                        $pDesc = 'Chip Deposit To ' . $userName;
                    }

                    $uTranData = [
                        'systemId' => $systemId,
                        'clientId' => $uid,
                        'userId' => $uid,
                        'childId' => 0,
                        'parentId' => $pid,
                        'eType' => 1, // 1 for chip transaction
                        'mType' => 'Chip ' . $mType,
                        'type' => $uType,
                        'amount' => $amount,
                        'balance' => $userBalance,
                        'description' => $uDesc,
                        'remark' => $remark,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];

                    $pTranData = [
                        'systemId' => $systemId,
                        'clientId' => $uid,
                        'userId' => $pid,
                        'childId' => $uid,
                        'parentId' => $pid2,
                        'eType' => 1, // 1 for chip transaction
                        'mType' => 'Chip ' . $mType,
                        'type' => $pType,
                        'amount' => $amount,
                        'balance' => $parentBalance,
                        'description' => $pDesc,
                        'remark' => $remark,
                        'created_on' => date('Y-m-d H:i:s'),
                        'updated_on' => date('Y-m-d H:i:s')
                    ];

                    if ( DB::connection('mysql3')->table($tbl1)->insert($uTranData) && DB::connection('mysql3')->table($tbl2)->insert($pTranData)) {
                        return true;
                    } else {
                        return false;
                    }

                } else {
                    return false;
                }

            } else {
                return false;
            }

        } catch (Exception $e) {
            return false;
        }

    }

    // update balance
    public static function updateUserChildData($uid, $pid, $role)
    {
        try {

            $tbl = 'tbl_user_child_data';
            $user = DB::table($tbl)->select(['super_masters', 'masters', 'clients'])
                ->where('uid', $pid)->first();

            $smArr = $mArr = $cArr = [];
            if ($role == 2) {
                if ($user != null) {
                    $smArr = json_decode($user->super_masters, 16);
                    if ($smArr != null && !in_array($uid, $smArr)) {
                        array_push($smArr, $uid);
                    } else {
                        $smArr[] = $uid;
                    }
                    DB::table($tbl)->where('uid', $pid)
                        ->update(['super_masters' => json_encode($smArr)]);
                } else {
                    array_push($smArr, $uid);

                    $insertArr = [
                        'uid' => $pid,
                        'super_masters' => json_encode($smArr),
                    ];

                    DB::table($tbl)->insert($insertArr);
                }

                $pUser = DB::table('tbl_user')->select(['parentId', 'role'])->where('id', $pid)->first();

                if ($pUser != null && $pUser->role != 1) {
                    self::updateUserChildData($uid, $pUser->parentId, 2);
                }

                return true;

            } elseif ($role == 3) {
                if ($user != null) {
                    $mArr = json_decode($user->masters, 16);
                    if ($mArr != null && !in_array($uid, $mArr)) {
                        array_push($mArr, $uid);
                    } else {
                        $mArr[] = $uid;
                    }
                    DB::table($tbl)->where('uid', $pid)
                        ->update(['masters' => json_encode($mArr)]);
                } else {
                    array_push($mArr, $uid);
                    $insertArr = [
                        'uid' => $pid,
                        'masters' => json_encode($mArr),
                    ];

                    DB::table($tbl)->insert($insertArr);
                }

                $pUser = DB::table('tbl_user')->select(['parentId', 'role'])->where('id', $pid)->first();

                if ($pUser != null && $pUser->role != 1) {
                    self::updateUserChildData($uid, $pUser->parentId, 3);
                }

                return true;

            } else {
                if ($user != null) {
                    $cArr = json_decode($user->clients, 16);
                    if ($cArr != null && !in_array($uid, $cArr)) {
                        array_push($cArr, $uid);
                    } else {
                        $cArr[] = $uid;
                    }
                    DB::table($tbl)->where('uid', $pid)
                        ->update(['clients' => json_encode($cArr)]);
                } else {
                    array_push($cArr, $uid);
                    $insertArr = [
                        'uid' => $pid,
                        'clients' => json_encode($cArr),
                    ];

                    DB::table($tbl)->insert($insertArr);
                }

                $pUser = DB::table('tbl_user')->select(['parentId', 'role'])->where('id', $pid)->first();

                if ($pUser != null && $pUser->role != 1) {
                    self::updateUserChildData($uid, $pUser->parentId, 4);
                }

                return true;
            }

            return false;

        } catch (Exception $e) {
            return false;
        }

    }

    // userChildData
    public static function userChildData($uid)
    {
        $superMastersArr = $mastersArr = $clientsArr = $userArr = [];

        $userChildData = DB::table('tbl_user_child_data')->select(['super_masters', 'masters', 'clients'])
            ->where([['uid', $uid]])->first();

        if ($userChildData != null) {

            if (isset($userChildData->super_masters) && $userChildData->super_masters != null) {
                $superMastersArr = json_decode($userChildData->super_masters);
            }
            if (isset($userChildData->masters) && $userChildData->masters != null) {
                $mastersArr = json_decode($userChildData->masters);
            }
            if (isset($userChildData->clients) && $userChildData->clients != null) {
                $clientsArr = json_decode($userChildData->clients);
            }

            if ($superMastersArr != null) {
                foreach ($superMastersArr as $ids) {
                    if (!in_array($ids, $userArr)) {
                        $userArr[] = $ids;
                    }
                }
            }
            if ($mastersArr != null) {
                foreach ($mastersArr as $ids) {
                    if (!in_array($ids, $userArr)) {
                        $userArr[] = $ids;
                    }
                }
            }
            if ($clientsArr != null) {
                foreach ($clientsArr as $ids) {
                    if (!in_array($ids, $userArr)) {
                        $userArr[] = $ids;
                    }
                }
            }
        }

        array_push($userArr, (int)$uid);
        return $userArr;

    }

    // get child count
    public static function getChildCount($uid, $role)
    {
        $count = DB::table('tbl_user')
            ->where([['status', 1],['parentId', $uid], ['role', $role]])->count();
        return $count;
    }

}



