<?php

namespace App\Models;

use App\Models\System\Session;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MainMenu extends Model
{
    public static function getList()
    {
        $user = Auth::user();

        $query = DB::table('tbl_main_menu')->select(['id','title','icon','url','class']);

        if( $user->roleName == 'ADMIN2' || $user->roleName == 'SM1'
            || $user->roleName == 'SM2' || $user->roleName == 'M1'){
            $query = $query->where([['status',1],['main_menu_id',0]])
                ->whereNotIn('id',['28']);
        }

        if( $user->roleName == 'SV' ){
            $query = $query->where([['status',1],['main_menu_id',0]])
                ->whereIn('id',['1','28']);
        }

        if( $user->roleName == 'ADMIN' || $user->roleName == 'SA' ){
            $query = $query->where([['status',1],['main_menu_id',0]]);
        }

       if( $user->roleName == 'SA' || $user->roleName == 'SV'){
           $query = $query->whereNotIn('id',['37']);
       }

        $mainMenu = $query->orderBy('position', 'ASC')->get();
        $menuArr = [];
        if( $mainMenu != null ){
            $menuArr = $childArr = [];
            foreach ( $mainMenu as $data ){
                $childArr = [];
                $query2 = DB::table('tbl_main_menu')->select(['id','title','icon','url','class'])
                    ->where([['status',1],['main_menu_id',$data->id]]);
                if ( $user->roleName == 'SA' ){
                    $query2 = $query2->whereNotIn('id',['32','33','17']);
                }else if ( $user->roleName == 'SM1' ){
                    $query2 = $query2->whereNotIn('id',['29','30','31','32','33','17']);
                }else if ( $user->roleName == 'SM2' ){
                    $query2 = $query2->whereNotIn('id',['21','29','30','31','32','33','17']);
                }else if ( $user->roleName == 'M1' ) {
                    $query2 = $query2->whereNotIn('id',['21','22','29','30','31','32','33','17']);
                }else if ( $user->roleName == 'ADMIN2' ) {
                    $query2 = $query2->whereNotIn('id',['29','30','31','32','33','17']);
                }else if ( $user->roleName == 'SV' ) {
                    $query2 = $query2->whereNotIn('id',['30','31','32','33','17']);
                }else{
                    // do nothing
                }

                $childMenu = $query2->orderBy('position', 'ASC')->get();

                if( $childMenu != null ){
                    foreach ( $childMenu as $data1 ){

                        if( trim($data1->title) == 'p&l live game' ){
                            $data1->url = '/reports/live-game-profit-loss/99';
                        }
                        if( trim($data1->title) == 'p&l live game 2' ){
                            $data1->url = '/reports/live-game2-profit-loss/999';
                        }

                        $childArr[] = [
                            'id' => $data1->id,
                            'title' => $data1->title,
                            'icon' => $data1->icon,
                            'url' => $data1->url,
                            'class' => $data1->class,
                        ];
                    }
                }

                $menuArr[] = [
                    'id' => $data->id,
                    'title' => $data->title,
                    'icon' => $data->icon,
                    'url' => $data->url,
                    'child' => !empty($childArr) ? $childArr : false,
                    'class' => $data->class
                ];


            }

        }

        return $menuArr;
    }


}



