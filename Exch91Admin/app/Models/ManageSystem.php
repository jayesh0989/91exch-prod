<?php

namespace App\Models;

use App\Models\System\Session;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ManageSystem extends Model
{
    // list
    public static function getList($rid = false)
    {

        $user = Auth::user();

        $list = [];
        $systemData = DB::table('tbl_system')->select('uid','systemname','operatorId','system_key','logo','theme_id')->where('status',[1,2])->orderBy('id', 'DESC')->get();
        if( $systemData->isNotEmpty() ) {
            foreach ($systemData as $system_Data) {
                $query = DB::table('tbl_user as u')
                    ->leftjoin('tbl_user_info as ui', 'u.id', '=', 'ui.uid')
                    ->select(['u.id as id', 'u.name', 'u.username', 'u.is_login', 'pl', 'balance', 'pl_balance', 'expose', 'pName', 'remark'])
                    ->where([['u.status', 1], ['u.role', 1]]);


                $query->where([['u.id', $system_Data->uid], ['u.status', 1], ['u.role', 1]]);
                $userData = $query->get();


                if ($userData->isNotEmpty()) {
                    foreach ($userData as $data) {
                        $isLock = $isBlock = 1;
                        $status1 = DB::table('tbl_user_block_status')->select(['uid'])
                            ->where([['uid', $data->id], ['type', 1]])->first();
                        if ($status1 != null) {
                            $isBlock = 0;
                        }

                        $status2 = DB::table('tbl_user_block_status')->select(['uid'])
                            ->where([['uid', $data->id], ['type', 2]])->first();
                        if ($status2 != null) {
                            $isLock = 0;
                        }

                        $smCount = CommonModel::getChildCount($data->id, 2);
                        $mCount = CommonModel::getChildCount($data->id, 3);
                        $cCount = CommonModel::getChildCount($data->id, 4);

                        $list[] = [
                            'systemid' => $system_Data->operatorId,
                            'system_name' => $system_Data->systemname,
                            'system_key' => $system_Data->system_key,
                            'theme_id' => $system_Data->theme_id,
                            'id' => $data->id,
                            'name' => $data->name,
                            'username' => $data->username,
                            'is_login' => $data->is_login,
                            'pl' => $data->pl,
                            'balance' => $data->balance,
                            'pl_balance' => $data->pl_balance,
                            'expose' => $data->expose,
                            'pName' => $data->pName,
                            'remark' => $data->remark,
                            'isLock' => $isLock,
                            'isBlock' => $isBlock,
                            'smCount' => $smCount,
                            'mCount' => $mCount,
                            'cCount' => $cCount,
                        ];
                    }
                }
            }
        }
        return $list;
    }

    // create
    public static function create($data)
    {
        $response = [ 'status' => 0, 'error' => [ 'message' => 'Something Wrong!' ] ];

        $pUser = Auth::user();
        $parentId = $pUser->id; // current user id
        $roleName = 'ADMIN2';
        $user = new User();
        $user->name = $data['name'];
        $user->username = $data['username'];
        $user->systemId = $data['operatorId'];
        $user->parentId = $pUser->id;
        $user->role = 1;
        $user->roleName = $roleName;
        $user->password = bcrypt( $data['password'] );

        if( $user->save() ){

            $userInfo = [
                'systemId' => $user->systemId,
                'uid' => $user->id,
                'pName' => $pUser->username,
                'pl' => $data['pl'],
                'balance' => $data['balance'],
                'pl_balance' => 0,
                'expose' => 0,
                'remark' => $data['remark']
            ];

            $settlementSummary = [
                'uid' => $user->id,
                'pid' => $user->parentId,
                'systemId' => $user->systemId,
                'role' => $user->roleName,
                'name' => $user->name.' [ '.$user->username.' ]'
            ];

            $pUserInfo = UserInfo::where('uid',$parentId)->first();
            $pUserInfo->balance = ($pUserInfo->balance-$data['balance']);

            if( $pUserInfo->balance < 0 ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered chip balance is more them parent user\'s balance ! So this user can\'t create!' ] ];
                DB::table('tbl_user')->delete($user->id);
                return $response;
            }

            if( ($pUserInfo->pl-$data['pl']) < 1 && $pUserInfo->pl > 0 ){
                $response = [ 'status' => 0, 'error' => [ 'message' => 'Entered pl is more them parent user\'s pl ! So this user can\'t create!' ] ];
                DB::table('tbl_user')->delete($user->id);
                return $response;
            }

            if( DB::table('tbl_user_info')->insert($userInfo)
                && DB::table('tbl_settlement_summary')->insert($settlementSummary) ){

                if( $pUserInfo->save() ){

                    $systemInfo = [
                        'systemname' =>  $data['systemname'],
                        'uid' => $user->id,
                        'operatorId' =>  $data['operatorId'],
                        'system_key' => $data['systemkey'],
                        'logo' => $data['logo'],
                        'theme_id' =>  $data['themeId'],
                        'status' => 1
                    ];

                    if( DB::table('tbl_system')->insert($systemInfo) ){
                        CommonModel::updateChipTransaction($user->id,$parentId,$data['balance'],$data['remark'],'DEPOSIT');
                        $response = [
                            'status' => 1,
                            'success' => [
                                'message' => 'Created successfully!'
                            ]
                        ];
                    }else{
                        DB::table('tbl_user_info')->where('uid',$user->id)->delete();
                    }
                }else{
                    DB::table('tbl_user_info')->where('uid',$user->id)->delete();
                }

            }else{
                DB::table('tbl_user')->delete($user->id);
            }

        }

        return $response;
    }

}



