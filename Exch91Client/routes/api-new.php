<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Login Or Auth
Route::get('site-mode', 'AuthController@siteMode');
Route::post('login', 'AuthController@login');
Route::get('android-version', 'AuthController@androidVersion');

Route::middleware('auth:api')->group(function () {
    //Logout and change pass
    Route::get('logout', 'AuthController@logout')->name('logout');
    Route::post('change-password', 'AuthController@changePassword');

    // menu list
    Route::get('menu-list', 'Dashboard\MenuController@menuList');

    // Dashboard
    Route::get('dashboard/get-user-data', 'Dashboard\ActionController@getUserData');
    Route::get('dashboard/get-match-unmatch-data', 'Dashboard\ActionController@getMatchUnmatchData');
    Route::get('dashboard/data-list/{type}', 'Dashboard\ActionController@getDataList');
    Route::get('dashboard/inplay-today-tomorrow', 'Dashboard\ActionController@getDataInplayTodayTomorrow');

    // Event
    Route::get('dashboard/event-details/cricket/{id}', 'Dashboard\DetailController@actionDataCricket');
    Route::get('dashboard/event-details/tennis/{id}', 'Dashboard\DetailController@actionDataTennis');
    Route::get('dashboard/event-details/football/{id}', 'Dashboard\DetailController@actionDataFootball');
    Route::get('dashboard/event-details/election/{id}', 'Dashboard\DetailController@actionDataElection');
    Route::get('dashboard/event-details/binary/{id}', 'Dashboard\DetailController@actionDataBinary');
    // new sport
    Route::get('dashboard/event-details/volleyball/{id}', 'Dashboard\DetailController@actionDataVolleyball');
    Route::get('dashboard/event-details/basketball/{id}', 'Dashboard\DetailController@actionDataBasketball');
    Route::get('dashboard/event-details/dart/{id}', 'Dashboard\DetailController@actionDataDart');
    Route::get('dashboard/event-details/table-tennis/{id}', 'Dashboard\DetailController@actionDataTableTennis');
    Route::get('dashboard/event-details/badminton/{id}', 'Dashboard\DetailController@actionDataBadminton');
    Route::get('dashboard/event-details/kabaddi/{id}', 'Dashboard\DetailController@actionDataKabaddi');
    Route::get('dashboard/event-details/boxing/{id}', 'Dashboard\DetailController@actionDataBoxing');
    Route::get('dashboard/event-details/jackpot/{id}', 'Dashboard\DetailController@actionDataJackpot');
    Route::get('dashboard/event-details/cricket-casino/{id}', 'Dashboard\DetailController@actionDataCricketCasino');
    Route::post('dashboard/jackpot-details', 'Dashboard\DetailController@actionJackpotDetails');

    Route::get('dashboard/event-details/live-games/{id}', 'Dashboard\DetailController@actionDataLiveGames');
    Route::get('dashboard/event-details/live-games2/{id}', 'Dashboard\DetailController@actionDataLiveGames');

    // book api
    Route::post('dashboard/fancy/book-data', 'Dashboard\BookController@actionBookDataFancy');
    Route::post('dashboard/fancy3/book-data', 'Dashboard\BookController@actionBookDataFancy3');
    Route::post('dashboard/ball-session/book-data', 'Dashboard\BookController@actionBookDataBallSession');
    Route::post('dashboard/khado-session/book-data', 'Dashboard\BookController@actionBookDataKhadoSession');
    Route::post('dashboard/casino/book-data', 'Dashboard\BookController@actionBookDataCasino');
    Route::post('dashboard/binary/book-data', 'Dashboard\BookController@actionBookDataBinary');

    //History
    Route::post('history/chip-history', 'History\ChipHistoryController@list');
    Route::post('history/chip-history/{id}', 'History\ChipHistoryController@list');

    Route::post('history/bet-history', 'History\BetHistoryController@list');
    Route::post('history/teenpatti-bet-history', 'History\BetHistoryController@listTeenpatti');
    Route::post('history/current-bets', 'History\BetHistoryController@listCurrentBets');

    //Account Statement
    Route::post('transaction/account-statement', 'Transaction\AccountStatementController@list');
    Route::post('transaction/account-statement-month/{date}', 'Transaction\AccountStatementController@month');
    Route::post('transaction/market-bet-list', 'Transaction\AccountStatementController@actionMarketBetList');

    //setting
    Route::get('setting/get-data-rules', 'Setting\ActionController@getDataRules');
    Route::post('setting/set-bet-options', 'Setting\ActionController@setBetOptions');


    //Bet place api
    Route::post('bet-place-test', 'Event\AppBetPlaceTestController@index');
    Route::post('bet-place', 'Event\AppBetPlaceController@index');

    //Profit Loss
    Route::post('transaction/profit-loss', 'Transaction\ProfitLossController@list');
    Route::post('transaction/profit-loss-by-event', 'Transaction\ProfitLossController@listEventDetail');
    Route::post('transaction/profit-loss-by-market', 'Transaction\ProfitLossController@listMarketDetail');
    Route::post('transaction/teenpatti-profit-loss', 'Transaction\ProfitLossController@listTeenPatti');


});



