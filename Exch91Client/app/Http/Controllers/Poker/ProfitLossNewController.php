<?php

namespace App\Http\Controllers\Poker;
use App\Http\Controllers\Controller;
use App\Model\TeenpattiResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;


class ProfitLossNewController extends Controller
{


    public function activityLog($message)
    {
        Log::channel('activity-log')->info($message);
    }

    /**
     * Action Game Result
     */
    public function actionGameResult(Request $request)
    {
        $response = ["errorDescription" => "General", "errorCode" => 2];

        try {
            $data = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);

            //Production server
            $key = '8ec5d127-225d-4d94-a64d-8be11ca0f9a2';
            //Developmret server
            // $key = "3c6fe53a-4a56-4bda-aba3-215fc896f1fc";
            $generatehashkey = base64_encode(hash_hmac("sha256", file_get_contents('php://input'), $key, true));
            $hashKey = $request->header('hash');
            // if ($hashKey == $hashKey) {


            if($generatehashkey == $hashKey) {
                $array = array('api' => 'profit-loss', 'request' => $data);
                $this->activityLog(json_encode($array));

                if (isset($data) && isset($data['marketId']) && isset($data['operatorId'])
                    && isset($data['winnerDescription']) && $data['winnerDescription'] != '') {

                    $userArr = $userIds = $betArr = [];
                    $betVoid = false;
                    $eventId = isset($data['eventId']) ? $data['eventId'] : 0;
                    $roundId = $marketId = $data['marketId'];
                    $gameName = $data['marketName'];
                    $userData = $data['profitLossDetails'];
                    $operatorId = $data['operatorId'];
                    $winner = $winnerDescription = trim($data['winnerDescription']);
                    //echo "test--2"; exit;
                    if ($winnerDescription == null || empty($winnerDescription) || $winnerDescription == '') {
                        $betVoid = true;
                    }
                    $response["operatorId"] = $operatorId;
                    $response["marketId"] = $marketId;


                    if (isset($data['requestId'])) {
                        $checkRequest = DB::table('access_request')->select('requestId')
                            ->where('requestId', $data['requestId'])->first();
                        if ($checkRequest != null) {
                            $responseData = [
                                "operatorId" => $operatorId,
                                "marketId" => $marketId,
                                "errorCode" => 2,
                                "errorDescription" => "General"
                            ];
                            return json_encode($responseData);
                        }

                        $resultArr = [
                            'requestId' => $data['requestId'],
                        ];

                        DB::table('access_request')->insert($resultArr);
                    }


                    $getBet = DB::connection('mongodb')->table('tbl_user_market_expose')->select('eid')
                        ->where('mid', $marketId)->first();
                    if ($getBet != null) {
                        $getBet=(object)$getBet;
                        $eventId = $getBet->eid;
                    }

                    if ($getBet == null && $eventId == 0) {
                        $getBet = DB::connection('mongodb')->table('tbl_bet_history_teenpatti')->select('eid')
                            ->where('mid', $marketId)->first();
                        if ($getBet != null) {
                            $getBet=(object)$getBet;
                            $eventId = $getBet->eid;
                        }
                    }
                    $mType = $marketName = '';
                    $resultCheck = DB::connection('mysql3')->table('tbl_teenpatti_result')->select(['user_data'])
                        ->where([['mid', (string)$marketId], ['sid', 999], ['game_over', 2], ['status', 1]])->first();
                    if ($resultCheck != null) {
                        // do nothing
                    } else {

                        foreach ($userData as $res) {
                            $userId = $res['userId'];
                            $exposure = $res['exposure'];
                            $userArr[] = [
                                'event_id' => $eventId,
                                'market_id' => $marketId,
                                'exposure' => $exposure,
                                'user_id' => $res['userId'],
                                'pl' => trim($res['pl'])
                            ];

                            $betArr[] = [
                                'user_id' => $res['userId'],
                                'bets' => $res['betList']
                            ];

                        }

                        $description = 'Live Game 2 ';
                        if (isset($gameName)) {
                            $description = $description . ' -> ' . $gameName;
                        }
                        if ($roundId != 0) {
                            $description = $description . ' #Round -> ' . $roundId;
                        }


                        /*  $model = new TeenpattiResult();

                          $model->eid = $eventId;
                          $model->mid = $marketId;
                          $model->round = $roundId;
                          $model->sid = 999;
                          $model->game_over = 2;
                          $model->winner = $winner;
                          $model->description = $description;
                          $model->result = $winnerDescription;
                          $model->user_data = json_encode($userArr);
                          $model->bet_data = json_encode($betArr);
                          $model->result_data = json_encode($data);
                          $model->status = 1;
                          $model->requestId = $data['requestId'];
                          $model->betvoid = $betVoid == true ? 1 : 0;

                          if ($model->save()) {
                              $response["errorDescription"] = "Success";
                              $response["errorCode"] = 1;
                          }*/

                        $resultData = $description . "->" . $winnerDescription;


                        $gameData = DB::table('tbl_live_game')
                            ->select(['sportId', 'eventId', 'name', 'mType', 'is_block', 'min_stake', 'max_stake', 'max_profit_limit', 'suspend'])
                            ->where([['status', 1], ['eventId', $eventId]])->first();

                        if ($gameData != null) {
                            $mType = $gameData->mType;
                            $marketName = $gameData->name;
                        }

                        //echo "test"; exit;
                        //run time game over functionality

                        $this->gameOverResultLiveGames($marketId, $betArr);
                        $this->transactionResultLiveGames($roundId, $eventId, $marketId, $userArr, $mType, $marketName, $resultData);
                        //end run time game over functionality


                    }
                    return json_encode($response);
                } else {

                    if (isset($data) && isset($data['marketId']) && isset($data['operatorId'])) {
                        $userArr = $userIds = $betArr = [];
                        $betVoid = false;
                        $marketId = $data['marketId'];
                        $operatorId = $data['operatorId'];
                        $updated_on = date('Y-m-d H:i:s');

                        DB::connection('mongodb')->table('tbl_bet_pending_teenpatti')->where([['result', 'PENDING'], ['mid', $marketId]])->update(['result' => 'CANCELED']);

                        $userData = $data['profitLossDetails'];
                        if (!empty($userData)) {

                            DB::connection('mongodb')->table('tbl_user_market_expose')->where('mid', $marketId)->update(['status' => 2]);

                            foreach ($userData as $res) {
                                $userId = $res['userId'];
                                $exposure = $res['exposure'];
                                if ($exposure == 1) {
                                    $exposure = 0;
                                }
                                $mType = 'teenpatti';
                                $this->updateUserExpose($userId, 0, $marketId, $mType, $exposure);
                            }
                        }

                        $responseData = [
                            "operatorId" => $operatorId,
                            "marketId" => $marketId,
                            "errorCode" => 1,
                            "errorDescription" => "Success"
                        ];
                        return json_encode($responseData);
                    }
                }
                return json_encode($response);
            } else {
                $response = ["errorDescription" => "HashNotMatched ", "errorCode" => 2];
                $array = array('api' => 'profit-loss', 'request' => $response);
                $this->activityLog(json_encode($array));
                return json_encode($response);
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    /**
     * Action Game Recall
     */
    public function actionGameRecall(Request $request)
    {
        $response = ["errorDescription" => "General", "errorCode" => 2];
        try {
            $data = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
            // $str = json_encode($data);
            //Production server
            $key = '8ec5d127-225d-4d94-a64d-8be11ca0f9a2';
            //Developmret server
            //$key = "3c6fe53a-4a56-4bda-aba3-215fc896f1fc";
            $generatehashkey = base64_encode(hash_hmac("sha256", file_get_contents('php://input'), $key, true));
            $hashKey = $request->header('hash');
            // if ($hashKey == $hashKey) {
            if($generatehashkey == $hashKey) {
                if (isset($data) && isset($data['marketId']) && isset($data['operatorId'])) {

                    $array = array('api' => 'rollback-profit-loss', 'request' => $data);
                    $this->activityLog(json_encode($array));
                    $marketId = $data['marketId'];
                    $operatorId = $data['operatorId'];

                    /*
                     $userArr = $userIds = $betArr = []; $betVoid = false;
                     $roundId = $data['marketId'];
                    $eventId = isset($data['eventId']) ? $data['eventId'] : 0;
                      $gameName = $data['marketName'];
                      $userData = $data['profitLossDetails'];
                      $winner = $winnerDescription = $data['winnerDescription'];
                      if( $winnerDescription == null || empty($winnerDescription) || $winnerDescription == '' ){
                          $betVoid = true;
                      }*/


                    if (isset($data['requestId'])) {
                        $checkRequest = DB::table('access_request')->select('requestId')
                            ->where('requestId', $data['requestId'])->get();
                        if ($checkRequest != null && !empty($checkRequest)) {
                            $cnt = 0;
                            foreach ($checkRequest as $checkRequestData) {
                                if ($cnt == 1) {
                                    $responseData = [
                                        "operatorId" => $operatorId,
                                        "marketId" => $marketId,
                                        "errorCode" => 2,
                                        "errorDescription" => "General"
                                    ];
                                    return json_encode($responseData);
                                }
                                $cnt++;
                            }
                        }

                        $resultArr = ['requestId' => $data['requestId']];
                        DB::table('access_request')->insert($resultArr);
                    }

                    $resultCheck = DB::connection('mysql3')->table('tbl_teenpatti_result')->select(['user_data', 'requestId', 'eid'])
                        ->where([['mid', $marketId], ['sid', 999]])->first();
                    if ($resultCheck != null) {
                        $eventId = $resultCheck->eid;
                        DB::connection('mysql3')->table('tbl_teenpatti_result')->where([['mid', $marketId], ['sid', 999]])
                            ->update(['game_over' => 5, 'status' => 1]);
                        $response = [
                            "operatorId" => $operatorId,
                            "marketId" => $marketId,
                            "errorCode" => 1,
                            "errorDescription" => "Success"
                        ];

                        return json_encode($response);
                        exit;
                    } else {
                        return json_encode($response);
                    }


                }
                return json_encode($response);
            } else {
                $response = ["errorDescription" => "HashNotMatched ", "errorCode" => 2];
                $array = array('api' => 'profit-loss', 'request' => $response);
                $this->activityLog(json_encode($array));
                return json_encode($response);
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    public function updateUserExpose($uid,$marketId)
    {
        $mongodb = DB::connection('mongodb'); $expose = 0;

        $mongodb->table('tbl_user_market_expose')
            ->where([['uid',(int)$uid],['mid',$marketId]])->update(['status' => 2]);

        $userMarketExpose = $mongodb->table('tbl_user_market_expose')
            ->select('expose')->where([['uid',(int)$uid],['status',1]])->get();
        if ($userMarketExpose->isNotEmpty()) {
            foreach ($userMarketExpose as $data) {
                $data = (object)$data;
                if ((int)$data->expose > 0) {
                    $expose = $expose + (int)$data->expose;
                }
            }
            $expose = round($expose);
        }

        $updated_on = date('Y-m-d H:i:s');
        $updateData = [
            'expose' => $expose,
            'updated_on' => $updated_on
        ];

        DB::table('tbl_user_info')->where('uid', $uid)->update($updateData);


    }

    //Game Over Result TeenPatti
    public function gameOverResultLiveGames($marketId, $betData)
    {
        $mongodb = DB::connection('mongodb');
        if ($betData != null) {
            foreach ($betData as $bets) {
                foreach ($bets['bets'] as $bet) {
                    $tbl = 'tbl_bet_pending_teenpatti';
                    $orderId = trim($bet['id']);
                    $betPl = (int)$bet['pl'];
                    $where = [['mid', (int)$marketId],['orderId', (int)$orderId], ['result', 'PENDING']];
                    if ($betPl < 0) {
                        $loss = round((-1)*($betPl), 2);
                        $updateArr = ['result' => 'LOSS','loss' => (int)$loss];
                    }elseif ($betPl > 0){
                        $win = round($betPl, 2);
                        $updateArr = ['result' => 'WIN', 'win' => (int)$win];
                    }else{
                        $updateArr = ['result' => 'CANCELED', 'win' => 0];
                    }

                    $mongodb->table($tbl)->where($where)->update($updateArr);

                }

            }

        }

    }


    // get Current Balance New
    public function getCurrentBalance($uid, $amount, $type, $role)
    {
        $conn = DB::connection('mysql');
        if ($type != 'CREDIT') {
            $amount = (-1) * $amount;
        }

        $updated_on = date('Y-m-d H:i:s');
        $updateArr = [
            'pl_balance' => $conn->raw('pl_balance + ' . $amount),
            'updated_on' => $updated_on
        ];

        if ($conn->table('tbl_user_info')
            ->where([['uid', $uid]])->update($updateArr)) {

            $cUserInfo = $conn->table('tbl_user_info')
                ->select(['balance', 'pl_balance', 'updated_on'])->where([['uid', $uid]])->first();
            $balance = $cUserInfo->balance;
            $plBalance = $cUserInfo->pl_balance;

            if ($role != 'CLIENT') {
                return round(($balance), 2);
            } else {
                return round(($balance + $plBalance), 2);
            }
        } else {
            $updated_on = date('Y-m-d H:i:s');
            $cUserInfo = $conn->table('tbl_user_info')
                ->select(['balance', 'pl_balance', 'updated_on'])->where([['uid', $uid]])->first();
            $betTimeDiff = (strtotime($updated_on) - strtotime($cUserInfo->updated_on));
            if ($betTimeDiff == 0) {
                usleep(1000);
                $cUserInfo = $conn->table('tbl_user_info')
                    ->select(['balance', 'pl_balance', 'updated_on'])->where([['uid', $uid]])->first();
            }

            $balance = $cUserInfo->balance;
            $newPlBalance = $cUserInfo->pl_balance;

            $updateArr = [
                'pl_balance' => $newPlBalance,
                'updated_on' => $updated_on
            ];

            if ($conn->table('tbl_user_info')
                ->where([['uid', $uid]])->update($updateArr)) {
                if ($role != 'CLIENT') {
                    return round(($balance), 2);
                } else {
                    return round(($balance + $newPlBalance), 2);
                }
            }
        }
    }


    //transaction Result MatchOdds
    public function transactionResultLiveGames($roundId, $eventId, $marketId, $userData, $mType, $marketName, $resultData)
    {
        if ($userData != null) {
            $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth = $month . $year;
            // echo "<pre>"; print_r($userData); exit;
            //$cnt=0;
            foreach ($userData as $users) {
                $uid = $users['user_id'];
                $amount = $users['pl'];
                if ($amount != 0) {
                    $this->updateTransactionClientHistory($uid, $roundId, $eventId, $marketId, $amount, $mType, $marketName, $resultData, $currentMonth);
                }

                $this->updateUserExpose($uid,$marketId);

            }

            $this->updateTransactionParentHistory($marketId);
        }
    }

    // Function to get the Description
    public function setDescription($marketName, $roundId)
    {
        $description = $marketName . ' > Round #' . $roundId;
        return $description;
    }

    // Update Transaction History
    public function updateTransactionClientHistory($uid, $roundId, $eventId, $marketId, $amount, $mType, $marketName, $resultData, $currentMonth)
    {
        $updated_on = date('Y-m-d H:i:s');
        if ($amount > 0) {
            $type = 'CREDIT';
        } else {
            $amount = (-1) * $amount;
            $type = 'DEBIT';
        }

        $summaryData = [];

        $cUser = DB::table('tbl_user')->select(['parentId', 'systemId'])->where([['id', $uid]])->first();

        $parentId = $cUser->parentId;
        $systemId = $cUser->systemId;
        $description = $this->setDescription($marketName, $roundId);

        $summaryData['uid'] = $uid;
        $summaryData['ownPl'] = $amount;
        $summaryData['type'] = $type;

        $this->userSummaryUpdate($uid,$amount,0, $type);
        $balance = $this->getCurrentBalance($uid,$amount,$type,'CLIENT');

        if (empty($balance) && $balance == '') { $balance = 0; }

        $resultArr = [
            'systemId' => $systemId,
            'clientId' => $uid,
            'userId' => $uid,
            'childId' => 0,
            'parentId' => $parentId,
            'sid' => 999,
            'eid' => $eventId,
            'mid' => $marketId,
            'round' => $roundId,
            'result' => $resultData,
            'eType' => 0,
            'mType' => $mType,
            'type' => $type,
            'amount' => round($amount, 2),
            'p_amount' => 0,
            'c_amount' => 0,
            'balance' => $balance,
            'description' => $description,
            'remark' => 'Transactional Entry',
            'status' => 1,
            'created_on' => $updated_on,
            'updated_on' => $updated_on
        ];

        $updatedOn = date('Y-m-d H:i:s');
        $year = date('Y', strtotime($updatedOn));
        $month = date('m', strtotime($updatedOn));
        $currentMonth=$month.$year;
        $tableName='tbl_transaction_client_'.$currentMonth;


        if( DB::connection('mysql3')->table($tableName)->insert($resultArr) ){
            // do something
        }

    }

    // Update Transaction Parent History
    public function updateTransactionParentHistory($marketId)
    {
        try {
            $conn = DB::connection('mysql');
            $conn3 = DB::connection('mysql3');
            $updatedOn = date('Y-m-d H:i:s');
            $year = date('Y', strtotime($updatedOn));
            $month = date('m', strtotime($updatedOn));
            $currentMonth=$month.$year;
            $tableName='tbl_transaction_client_'.$currentMonth;
            $clientTransData = $conn3->table($tableName)
                ->where([['status', 1],['mid',$marketId]])->get();

            if ( $clientTransData->isNotEmpty() ) {
                $newUserArr = [];
                foreach ($clientTransData as $transData){
                    $userId = $transData->userId;
                    $type = $transData->type;
                    $amount = $transData->amount;
                    $sid = $transData->sid;
                    $eid = $transData->eid;
                    $mid = $transData->mid;
                    $result = $transData->result;
                    $eType = $transData->eType;
                    $mType = $transData->mType;
                    $description = $transData->description;

                    if( $type == 'CREDIT' ){ $pType = 'DEBIT'; }else{ $pType = 'CREDIT'; }

                    $parentUserData = $conn->table('tbl_user_profit_loss as pl')
                        ->select(['pl.userId as userId','pl.parentId as parentId','pl.actual_profit_loss as apl','pl.profit_loss as gpl','ui.balance as balance','ui.pl_balance as pl_balance'])
                        ->leftjoin('tbl_user_info as ui', 'pl.userId', '=', 'ui.uid')
                        ->where([['pl.clientId',$userId],['pl.actual_profit_loss','!=',0]])->orderBy('pl.userId','desc')->get();

                    $childId = $userId;

                    foreach ( $parentUserData as $user ){
                        $cUser = $conn->table('tbl_user')->select(['systemId','parentId','role'])
                            ->where([['id',$user->userId]])->first();
                        $systemId = $cUser->systemId;
                        $parentId = $cUser->parentId;
                        $role = $cUser->role;
                        $tAmount = ( $amount*$user->apl )/100;
                        $pTAmount = ( $amount*(100-$user->gpl) )/100;
                        $cTAmount = ( $amount*($user->gpl-$user->apl) )/100;

                        if( isset($newUserArr[$user->userId]) ){

                            if( $pType != 'CREDIT' ){
                                $tAmount = (-1)*$tAmount;
                                $pTAmount = (-1)*$pTAmount;
                                $cTAmount = (-1)*$cTAmount;
                            }

                            $tAmount = $newUserArr[$user->userId]['tAmount']+$tAmount;
                            $pTAmount = $newUserArr[$user->userId]['pTAmount']+$pTAmount;
                            $cTAmount = $newUserArr[$user->userId]['cTAmount']+$cTAmount;

                            $newUserArr[$user->userId]['tAmount'] = $tAmount;
                            $newUserArr[$user->userId]['pTAmount'] = $pTAmount;
                            $newUserArr[$user->userId]['cTAmount'] = $cTAmount;

                        }else{

                            if( $pType != 'CREDIT' ){
                                $tAmount = (-1)*$tAmount;
                                $pTAmount = (-1)*$pTAmount;
                                $cTAmount = (-1)*$cTAmount;
                            }

                            $newUserArr[$user->userId] = [
                                'role' => $role,
                                'systemId' => $systemId,
                                'clientId' => $userId,
                                'userId' => $user->userId,
                                'childId' => $childId,
                                'parentId' => $parentId,
                                'tAmount' => $tAmount,
                                'pTAmount' => $pTAmount,
                                'cTAmount' => $cTAmount,
                                'sid' => $sid,
                                'eid' => $eid,
                                'mid' => $mid,
                                'result' => $result,
                                'eType' => $eType,
                                'mType' => $mType,
                                'description' => $description,
                            ];
                        }
                        $childId = $user->userId;
                    }

                }

                if( $newUserArr != null ){
                    foreach ( $newUserArr as $userData ){
                        if( $userData['tAmount'] < 0 ){
                            $pType = 'DEBIT';
                            $amount = (-1)*round($userData['tAmount'],2);
                            $pAmount = (-1)*round($userData['pTAmount'],2);
                            $cAmount = (-1)*round($userData['cTAmount'],2);
                        }else{
                            $pType = 'CREDIT';
                            $amount = round($userData['tAmount'],2);
                            $pAmount = round($userData['pTAmount'],2);
                            $cAmount = round($userData['cTAmount'],2);
                        }

                        $this->userSummaryUpdate($userData['userId'],$amount,$pAmount,$pType);

                        $ubalance = 0;
                        $SummaryData1 = DB::connection('mysql')->table('tbl_settlement_summary')->select('ownPl', 'ownComm')
                            ->where('uid', $userData['userId'])->first();
                        if (!empty($SummaryData1)) {
                            $updated_on = date('Y-m-d H:i:s');
                            $ubalance = 0;
                            $ubalance = $SummaryData1->ownPl + $SummaryData1->ownComm;
                            DB::connection('mysql')->table('tbl_user_info')->where([['uid', $userData['userId']]])
                                ->update(['pl_balance' => $ubalance, 'updated_on' => $updated_on]);
                        }

                        $currentBalance = $ubalance;

                        if(empty($currentBalance) && $currentBalance==''){
                            $currentBalance=0;
                        }
                        // $balance = $this->getCurrentBalance($userData['userId'], $amount, $pType,'PARENT');

                        $resultArr = [
                            'systemId' => $userData['systemId'],
                            'clientId' => $userData['clientId'],
                            'userId' => $userData['userId'],
                            'childId' => $userData['childId'],
                            'parentId' => $userData['parentId'],
                            'sid' => $userData['sid'],
                            'eid' => $userData['eid'],
                            'mid' => $userData['mid'],
                            'result' => $userData['result'],
                            'eType' => $userData['eType'],
                            'mType' => $userData['mType'],
                            'type' => $pType,
                            'amount' => $amount,
                            'p_amount' => $pAmount,
                            'c_amount' => $cAmount,
                            'balance' => $currentBalance,
                            'description' => $userData['description'],
                            'remark' => 'Transactional Entry',
                            'status' => 1,
                            'created_on' => date('Y-m-d H:i:s'),
                            'updated_on' => date('Y-m-d H:i:s')
                        ];

                        $updatedOn = date('Y-m-d H:i:s');
                        $year = date('Y', strtotime($updatedOn));
                        $month = date('m', strtotime($updatedOn));
                        $currentMonth=$month.$year;
                        if( $userData['role'] == 1 ){
                            //  $tbl = 'tbl_transaction_admin';
                            $tbl = 'tbl_transaction_admin_'.$currentMonth;
                        }else {
                            // $tbl = 'tbl_transaction_parent';
                            $tbl = 'tbl_transaction_parent_' . $currentMonth;
                        }
                        if( $conn3->table($tbl)->insert($resultArr) ){
                            //return ['status' => 1];
                        }

                    }
                }
            }
        } catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    //user Summary Update New
    public function userSummaryUpdate($uid,$ownPl,$parentPl,$type)
    {
        $conn = DB::connection('mysql');
        $userSummary = $conn->table('tbl_settlement_summary')
            ->where([['status', 1], ['uid', $uid]])->first();
        if ($userSummary != null) {

            $updatedOn = date('Y-m-d H:i:s');
            if (isset($type) && $type != 'CREDIT') {
                $ownPl = (-1) * round($ownPl, 2);
                $parentPl = (-1) * round($parentPl, 2);
            }

            $updateArr = [
                'ownPl' => $conn->raw('ownPl + ' . $ownPl),
                'parentPl' => $conn->raw('parentPl + ' . $parentPl),
                'updated_on' => $updatedOn
            ];

            if ($conn->table('tbl_settlement_summary')
                ->where([['status', 1], ['uid', $uid]])->update($updateArr)) {
                // do something
            } else {
                $userSummary = $conn->table('tbl_settlement_summary')
                    ->where([['status', 1], ['uid', $uid]])->first();
                $updatedOn = date('Y-m-d H:i:s');
                $betTimeDiff = (strtotime($updatedOn) - strtotime($userSummary->updated_on));
                if ($betTimeDiff == 0) {
                    usleep(1000);
                    $userSummary = $conn->table('tbl_settlement_summary')
                        ->where([['status', 1], ['uid', $uid]])->first();
                }
                $updateArr = [
                    'ownPl' => $userSummary->ownPl + $ownPl,
                    'parentPl' => $userSummary->parentPl + $parentPl,
                    'updated_on' => date('Y-m-d H:i:s')
                ];
                if ($conn->table('tbl_settlement_summary')
                    ->where([['status', 1], ['uid', $uid]])->update($updateArr)) {
                    // do something
                }
            }

        } else {
            $updatedOn = date('Y-m-d H:i:s');
            if (isset($type) && $type != 'CREDIT') {
                $ownPl = (-1) * round($ownPl, 2);
                $parentPl = (-1) * round($parentPl, 2);
            }
            $ownComm = 0;
            $parentComm = 0;
            $insertArr = ['uid' => $uid, 'updated_on' => $updatedOn,
                'ownPl' => $ownPl, 'ownComm' => $ownComm,
                'parentPl' => $parentPl, 'parentComm' => $parentComm];

            $conn->table('tbl_settlement_summary')->insert($insertArr);

        }

    }


}
