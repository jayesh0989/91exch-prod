<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;
use League\Flysystem\Config;


class AppListController extends Controller
{

    public function applist(Request $request)
    {

      try {

          //echo   $h_key= Config::get('siteVars.HASH_KEY');
          $eventData = null;
          $response = ['status' => 0, 'message' => 'Bad request !!!', 'data' => null];
          // $key='$ome$alt';
          $key = $this->haskKey();
          $requestId = $request->tnp;
          $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
          $hashKey = $request->header('hash');
          $hashAuth = $this->HashAuth($generatehashkey, $hashKey);
          if ($hashAuth == 1) {


              $uid = Auth::user()->id;
              $updatedOn = strtotime(Auth::user()->updated_at->toDateTimeString());

              $sportId = 0;
              $parentId = 1;
              $response = ['status' => 0, 'message' => 'Bad request !!!', 'data' => null];
              $curr_time = date("Y-m-d h:i:s");
              $curr_date_time = strtotime($curr_time);

              $cache = Redis::connection();

              if (isset($cache)) {

                  $eventDataArray = null;
                  $sportList = $cache->get("SportList");
                  $sportList = json_decode($sportList);

                //  print_r($sportList); die('s');

                  if (!empty($sportList)) {
                      foreach ($sportList as $sport_List) {
                          if ($sport_List->is_block != 1) {
                              $sportId = $sport_List->sportId;
                              $sport_name = $sport_List->slug;
                              $event_Data = $cache->get("Event_List_" . $sportId);
                              $eventData1 = json_decode($event_Data);
                                if(isset($eventData1->eventData)) {
                                    foreach ($eventData1->eventData as $key => $value) {

                                        $value->sport_name = $sport_name;
                                        $eventData[] = $value;

                                    }
                                }
                              //echo "<pre>";  print_r($eventData); exit;
                          }
                      }
                  }
                 /* if (!empty($sportList)) {
                      foreach ($sportList as $sport_List) {
                          if ($sport_List->is_block != 1) {
                              $sportId = $sport_List->sportId;
                              $sport_name = $sport_List->slug;
                              $event_Data_array = $cache->get("Event_List_" . $sportId);
                              $eventData_1 = json_decode($event_Data_array);
                              $eventDataList = $eventData_1->eventData;


                              foreach ($eventDataList as $key => $value) {

                                  $value->sport_name = $sport_name;
                                  $eventData[] = $value;

                              }


                          }
                      }
                  }*/

              } else {

                  if ($sportId != 0) {
                      $where = [['game_over', 0], ['status', 1], ['sport_id', $sportId]];
                  } else {
                      $where = [['game_over', 0], ['status', 1]];
                  }
              }

              $responseData = [];
              // echo "<pre>";  print_r($eventData); exit;
              if ($eventData != null) {


                  // $unblockEvents = $this->checkUnBlockList($uid, $eventData);
                  $unblockSport = $this->checkUnBlockSportList($uid);
                //  print_r($eventData); die('tytty');
                  $eventData = array_reverse($eventData, true);
                  $i = 0;
                  $k = 0;
                  $j = 0;
                  foreach ($eventData as $event) {

                      $eventId = $event->eventId;
                      $unblockEvents = $this->getBlockStatusAdmin( $eventId, 'event');
                 // print_r($unblockEvents);die();
                    if ($unblockEvents==0) {

                      $blockEventList = $this->checkUnBlockList($uid);
                              //dd($blockEventList);
                              if(!in_array($eventId, $blockEventList)){

                      $sportId = $event->sportId;


                      $runners = json_decode($event->runners, true);

                      if (!empty($runners)) {
                        # code...
                      
                      $runner3 = 0;
                      $runner1 = $runners[0]['runner'];
                      $runner2 = $runners[1]['runner'];
                      if (isset($runners[2]['runner'])) {
                          $runner3 = $runners[2]['runner'];
                      }
                    }

                      $eventData1 = $cache->get("Fancy_3_" . $eventId);
                      $eventData1 = json_decode($eventData1, 16);
                      $count1 = 0;
                      if (isset($eventData1)) {
                          if ($eventData1 != null || $eventData1 != 0 || $eventData1 != ' ') {
                              $count1 = count($eventData1);
                          } else {
                              $count1 = 0;
                          }
                      }

                      //fancy 2 count
                      $eventData2 = $cache->get("Fancy_2_" . $eventId);
                      $eventData2 = json_decode($eventData2, 16);
                      $count2 = 0;
                      if (isset($eventData2)) {

                          if ($eventData2 != null || $eventData2 != 0 || $eventData2 != ' ') {
                              $count2 = count($eventData2);
                          } else {
                              $count2 = 0;
                          }
                      }
                      //fancy count
                      $eventData3 = $cache->get("Fancy_" . $eventId);
                      $eventData3 = json_decode($eventData3, 16);
                      $count3 = 0;
                      if (isset($eventData3)) {

                          if ($eventData3 != null || $eventData3 != 0 || $eventData3 != ' ') {
                              $count3 = count($eventData3);
                          } else {
                              $count3 = 0;
                          }
                      }

                      $fancycount = $count1 + $count2 + $count3;


                      if ($fancycount > 0) {
                          $isFancy = 1;
                      } else {
                          $isFancy = 0;
                      }

                    

                          if ($sportId == '4' && $event->game_over != 1 && $event->is_block != 1) {
                              
                              $time = $event->time * 1000;
                              $var = [
                                  'slug' => 'cricket',
                                  'type' => $event->type,
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => $runner1 ? $runner1 : 0,
                                  //'runner2' => $runner2 ? $runner2 : 0,
                                  //'runner3' => $runner3 ? $runner3 : 0,
                                  'is_fancy' => $isFancy,
                                  'is_tv' => isset($event->isTv) ? $event->isTv : 0,
                              ];
                              if (isset($request->platform) && $request->platform == 'desktop') {
                                  $var['odds'] = [];

                                  $eventDat = $cache->get("Match_Odd_" . $eventId);
                                  $eventDat = json_decode($eventDat);
                                  if (!empty($eventDat)) {

                                      $marketId = $eventDat->marketId;
                                      $matchOdd_odds = $cache->get($marketId);

                                      if (!empty($matchOdd_odds)) {
                                          $odds = json_decode($matchOdd_odds);
                                          $Match_odds = $odds->odds;

                                          foreach ($Match_odds as $key => $value) {

                                              $var['odds'][] = [

                                                  'backPrice1' => $value->backPrice1,
                                                  'layPrice1' => $value->layPrice1
                                              ];
                                          }
                                      }
                                  }

                              }
                              $responseData['cricket'][] = $var;


                          } else if ($sportId == '10' && $event->game_over != 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $responseData['jackpot'][] = [
                                  'slug' => 'jackpot',
                                  'type' => $event->type,
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => $runner1 ? $runner1 : 0,
                                  //'runner2' => $runner2 ? $runner2 : 0,
                                  //'runner3' => $runner3 ? $runner3 : 0,
                                  'is_fancy' => 1,
                                  'isJackpot' => 1,
                                  'isGhoda' => 1,
                                  'is_tv' => isset($event->isTv) ? $event->isTv : 0,
                                  'closeTime' => $this->getEventCloseTime($event->time),
                              ];


                          } else if ($sportId == '2' && $event->game_over != 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $var = [
                                  'slug' => 'tennis',
                                  'type' => $event->type,
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => $runner1 ? $runner1 : 0,
                                  //'runner2' => $runner2 ? $runner2 : 0,
                                  //'runner3' => $runner3 ? $runner3 : 0,
                                  'is_fancy' => $isFancy,
                                  'is_tv'=>isset($event->isTv) ? $event->isTv : 0,

                              ];
                              if (isset($request->platform) && $request->platform == 'desktop') {
                                  $var['odds'] = [];

                                  $eventDat = $cache->get("Match_Odd_" . $eventId);
                                  $eventDat = json_decode($eventDat);
                                  if (!empty($eventDat)) {

                                      $marketId = $eventDat->marketId;
                                      $matchOdd_odds = $cache->get($marketId);

                                      if (!empty($matchOdd_odds)) {
                                          $odds = json_decode($matchOdd_odds);
                                          $Match_odds = $odds->odds;

                                          foreach ($Match_odds as $key => $value) {

                                              $var['odds'][] = [

                                                  'backPrice1' => $value->backPrice1,
                                                  'layPrice1' => $value->layPrice1
                                              ];
                                          }
                                      }
                                  }

                              }
                              $responseData['tennis'][] = $var;

                          } else if ($sportId == '1' && $event->game_over != 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $var = [
                                  'slug' => 'football',
                                  'type' => $event->type,
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => $runner1 ? $runner1 : 0,
                                  //'runner2' => $runner2 ? $runner2 : 0,
                                  //'runner3' => $runner3 ? $runner3 : 0,
                                  'is_fancy' => $isFancy,
                                  'is_tv'=>isset($event->isTv) ? $event->isTv : 0,

                              ];
                              if (isset($request->platform) && $request->platform == 'desktop') {

                                  $var['odds'] = [];

                                  $eventDat = $cache->get("Match_Odd_" . $eventId);
                                  $eventDat = json_decode($eventDat);
                                  //print_r($eventDat);
                                  if (!empty($eventDat)) {

                                      $marketId = $eventDat->marketId;
                                      $matchOdd_odds = $cache->get($marketId);

                                      if (!empty($matchOdd_odds)) {
                                          $odds = json_decode($matchOdd_odds);
                                          $Match_odds = $odds->odds;


                                          foreach ($Match_odds as $key => $value) {

                                              $var['odds'][] = [

                                                  'backPrice1' => $value->backPrice1,
                                                  'layPrice1' => $value->layPrice1
                                              ];
                                          }
                                      }
                                  }

                              }
                              $responseData['football'][] = $var;

                          } else if ($sportId == '99' && $event->game_over != 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $responseData['teenpatti'][] = [
                                  'slug' => 'teenpatti',
                                  'type' => $event->type,
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  'is_favorite' => 0,

                              ];


                          } else if ($sportId == '6' && $event->game_over != 1 && $event->status == 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $responseData['election'][] = [
                                  'slug' => 'election',
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'type' => $event->type,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => '',
                                  //'runner2' => '',
                                  //'runner3' => '',
                                  'is_fancy' => $isFancy,
                                  'is_tv'=>isset($event->isTv) ? $event->isTv : 0,
                              ];


                          } else if ($sportId == '11' && $event->game_over != 1 && $event->status == 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $responseData['cricket_casino'][] = [
                                  'slug' => 'cricket_casino',
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'type' => $event->type,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => $runner1,
                                  //'runner2' => $runner2,
                                  //'runner3' => $runner3


                              ];


                          }else if ($sportId == '7' && $event->game_over != 1 && $event->status == 1 && $event->is_block != 1) {

                              $time = $event->time * 1000;
                              $responseData['binary'][] = [
                                  'slug' => 'binary',
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'type' => $event->type,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => '',
                                  //'runner2' => '',
                                  //'runner3' => ''


                              ];
                          }else if (($sportId == '998917' || $sportId == '7522' || $sportId == '3503' || $sportId == '22' || $sportId == '13' || $sportId == '14' || $sportId == '16') && ($event->game_over != 1 && $event->status == 1 && $event->is_block != 1)) {
                              $sport_name = strtolower($event->sport_name);
                              $time = $event->time * 1000;
                              $responseData[$sport_name][] = [
                                  'slug' => $sport_name,
                                  'sportId' => $sportId,
                                  'eventId' => $eventId,
                                  'type' => $event->type,
                                  'title' => $event->name,
                                  'league' => $event->league,
                                  'time' => $time,
                                  //'runner1' => '',
                                  //'runner2' => '',
                                  //'runner3' => '',
                                  'is_fancy' => $isFancy,
                                  'is_tv' => isset($event->isTv) ? $event->isTv : 0,
                              ];


                          }
                        }//here

                      }

                      $i++;

                      if ($sportId == '998917' || $sportId == '7522' || $sportId == '3503' || $sportId == '22' || $sportId == '13' || $sportId == '14' || $sportId == '16') {

                          $sport_name = strtolower($event->sport_name);
                          if (in_array(1, $unblockSport)) {
                              $responseData[$sport_name] = [];
                              $responseData['is'.$event->sport_name.'Block'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                          }
                      }
                      if (in_array(1, $unblockSport)) {
                          $responseData['football'] = [];
                          $responseData['isFootballBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(2, $unblockSport)) {
                          $responseData['tennis'] = [];
                          $responseData['isTennisBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(4, $unblockSport)) {
                          $responseData['cricket'] = [];
                          $responseData['isCricketBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(999999, $unblockSport)) {
                          $responseData['teenpatti'] = [];
                          $responseData['isTeenPattiBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(6, $unblockSport)) {
                          $responseData['election'] = [];
                          $responseData['isElectionBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(10, $unblockSport)) {
                          $responseData['jackpot'] = [];
                          $responseData['isJackpotBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(11, $unblockSport)) {
                          $responseData['cricket_casino'] = [];
                          $responseData['isCricketCasinoBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }
                      if (in_array(7, $unblockSport)) {
                          $responseData['binary'] = [];
                          $responseData['isBinaryBlock'] = ['status' => 1, 'msg' => 'This Sport Block by Parent!'];
                      }

                  }
              }

              $response = ["status" => 1, 'code' => 200, "data" => ["items" => $responseData], "message" => 'Data Found!!'];

              return $response;
          } else {
              return $response;
          }

       }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


    public function getBlockStatusAdmin($eventIdOrSportId,$type)
    {
        if( $type == 'event' ) {
            $eventId = $eventIdOrSportId;
            $redis = Redis::connection();
            $blockEventIdsJson = $redis->get('Block_Event_Market');
            $blockEventIds = json_decode($blockEventIdsJson);
            if (!empty($blockEventIds) && in_array($eventId, $blockEventIds)) {
                return 1;
            } else {
                return 0;
            }
        }
    }

//check database function
public function checkUnBlockList($uId){

         $eventID =[];
         $user = DB::table('tbl_user_event_status')->select('eid')->where('uid',$uId)->get();       
        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $eventID[] = $value->eid; 
            }

        }
       
        return $eventID;
        
}

public function checkUnBlockSportList($uId){
    
        $sportId =[];
        $user = DB::table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $sportId[] = $value->sid; 
            }

        }
        return $sportId;
}

//isJackpot
public function getEventCloseTime($time){

        $ctime = time();
        $closeTime = ( $time )-$ctime;

        $title = 'This event is closed';
        if( $closeTime > 0 ){
            if( $closeTime > 60 ){

                $hours = floor($closeTime / 3600);
                $minutes = floor(($closeTime / 60) % 60);

                if( $hours > 0 ){
                    $title = 'Event is close in '.$hours.' hrs and '.$minutes.' minutes.';
                }else{
                    $title = 'Event is close in '.$minutes.' minutes.';
                }

            }else{
                $title = 'Event is close in '.round($closeTime).' seconds.';
            }

        }

        return $title;
}

}
