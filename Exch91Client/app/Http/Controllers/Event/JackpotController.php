<?php

namespace App\Http\Controllers\Event;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;


class JackpotController extends Controller
{

    public function jackpotDynamicList(Request $request)
    {      
    try{

       $eventName='';
       $listDataArray=[]; $fancycount=0; $khadocount=0;
       $response = ['status'=>0,'message'=>'Bad request !!!','data'=>null];

       if (null != $request->route('id')) {
            $eventId = $request->route('id');
               $cache = Redis::connection();
            
                        $eventData=$cache->get("Event_".$eventId);
                        $eventData = json_decode($eventData);
                            
            if ($eventData != null) {

                        $cache = Redis::connection();
                        //single fancy count
                        $eventData1=$cache->get("Fancy_3_".$eventId);
                        $eventData1 = json_decode($eventData1,16);
                        $count1 = 0;
                        if(isset($eventData1)){
                        if($eventData1 != null || $eventData1 != 0 || $eventData1 != ' ' ){ $count1 = count($eventData1) ;}else{$count1 = 0;}
                        }
                       
                         //fancy 2 count
                        $eventData2=$cache->get("Fancy_2_".$eventId);
                        $eventData2 = json_decode($eventData2, 16);
                        $count2 = 0;
                        if(isset($eventData2)){

                        if($eventData2 != null || $eventData2 != 0 || $eventData2 != ' ' ){ $count2 = count($eventData2) ;}else{$count2 = 0;}
                        }
                        //fancy count
                        $eventData3=$cache->get("Fancy_".$eventId);
                        $eventData3 = json_decode($eventData3, 16);
                        $count3=0;
                        if(isset($eventData3)){
                       
                        if($eventData3 != null || $eventData3 != 0 || $eventData3 != ' ' ){ $count3 = count($eventData3) ;}else{$count3 = 0;}
                        }

                        $fancycount = $count1 +  $count2 +$count3; 
                    
              
                        //khado count
                        $eventDataKhado=$cache->get("Khado_".$eventId);
                        $eventDataKhado = json_decode($eventDataKhado);
                        //print_r($eventDataKhado); die('pop');
                        if($eventDataKhado == 0 || $eventDataKhado == null){ $khadocount = 0 ;}else{$khadocount = count($eventDataKhado);}
                  

                $eventName = $eventData->name;

                $listData=$cache->get("Jackpot_type");
                $listData = json_decode($listData);
                
                  if(!empty($listData)){
                    foreach ($listData as $key => $value) {
                         $totalcount=0;         
                         $jackpot = $cache->get("Jackpot_data_".$value->id."_". $eventId);
                         $jackpot = json_decode($jackpot);
                        
                        if(!empty($jackpot)){
                     
                          $totalcount =1;
                         $jackpotArr[] = $jackpot;
                        }
      
                        if($value->is_fancy == 1){
                            $is_fancy = 1;
                            $is_jackpot = 0;
                        }else{
                            $is_fancy = 0;
                            $is_jackpot = 1;

                        }

                        
                        $listDataArray[]=array('title'=>$value->title,'slug'=>$value->slug,'is_jackpot'=>$is_jackpot,'is_fancy'=>$is_fancy,'totalcount'=>$totalcount);
                     }

                  }

                      

                $response = ["status" => 1,'code'=> 200,"data" => ["title" => $eventName ,'items' => $listDataArray,'fancycount'=>$fancycount,'khadocount'=>$khadocount],'messsage'=>'Data Found !!'];
            } else{
                $response = ["status" => 1,'code'=>200, "data" => null, "msg" => "Data Not Found !!"];
            }
        }
 

    return $response;

     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }


    }
    


 //Event: Jackpot Fancy
    public function jackpotFancy(Request $request)
    {

        try{

          $response = ['status'=>0,'message'=>'Bad request !!!','data'=>null];

          $marketIdsArr = [];
        if ( null != $request->route('id')) {
     
            $uid = Auth::user()->id;
            $updatedOn = $this->updatedOn($uid);
            $eventId = $request->route('id');

            if (in_array($eventId, $this->checkUnBlockList($uid))) {
                $response = ["status" => 1, 'code'=>200,"updatedOn" => $updatedOn, "data" => null, "msg" => "This event is closed !!"];
                return $response;
                exit;
            }

            $eventArr = null;
             $items = [];

          
             $cache = Redis::connection();
            
             $eventData=$cache->get("Event_".$eventId);
             $event = json_decode($eventData);
              
            if( $event != null){

                $title = $event->name;

                if( isset($cache)){
                    $cache = Redis::connection();
                    $fancy2=$cache->get("Fancy_2_".$eventId);
                    $marketList = json_decode($fancy2);      
                }

                $items = [];
                    $itemsFancy = [];
                if ($marketList != null) {
                    $suspended = $ballRunning = 0;

                    $dataVal = []; $info = '';
                foreach ($marketList as $data) {

                   if($data->status == 1 && $data->game_over == 0){
                    
                        $suspended = $ballRunning = 1;
                        $minStack = $data->min_stack;
                        $maxStack = $data->max_stack;
                        $maxProfitLimit = $data->max_profit_limit;
                        $info = $data->info;

                        if ($suspended != 1) {
                            $suspended = $data->suspended;
                        }
                        if ($ballRunning != 1) {
                            $ballRunning = $data->ball_running;
                        }

                        $betDelay = 0;
     

                        $itemsFancy[] = [
                            'id' => $data->id,
                            'eventId' => $data->eid,
                            'marketId' => $data->marketId,
                            'title' => $data->title,
                            'mType' => 'fancy2',
                            'suspended' => $suspended,
                            'ballRunning' => $ballRunning,
                            'sportId' => 10,
                            'slug' => 'jackpot',
                            'is_book' => $this->isBookOn($uid,$data->marketId, 'fancy2'),
                            'minStack' => $minStack,
                            'maxStack' => $maxStack,
                            'maxProfitLimit' => $maxProfitLimit,
                            'betDelay' => $betDelay,
                            'info' => $info,
                            'no' => 0,
                            'no_rate' => 0,
                            'yes' =>   0,
                            'yes_rate' =>  0,
                        ];
                         if(!empty($data->marketId)){
                        $marketIdsArr[] =  $data->marketId;
                         }
                    }
                 }
                }


                $cache = Redis::connection();
                $fancy3=$cache->get("Fancy_3_".$eventId);
                $marketList = json_decode($fancy3);
                
                $itemsFancy3 = [];
                if ($marketList != null) {
                        $suspended = $ballRunning = 0;

                    $dataVal = []; $info = '';
                    foreach ($marketList as $data) {

                    
                     if($data->status == 1 && $data->game_over == 0){
                        $suspended = $ballRunning = 0;

                        $minStack = $data->min_stack;
                        $maxStack = $data->max_stack;
                        $maxProfitLimit = $data->max_profit_limit;
                        $info = $data->info;

                        if ($suspended != 1) {
                            $suspended = $data->suspended;
                        }
                        if ($ballRunning != 1) {
                            $ballRunning = $data->ball_running;
                        }

                        $betDelay = 0;


                        $itemsFancy3[] = [
                            'id' => $data->id,
                            'eventId' => $data->eid,
                            'marketId' => $data->marketId,
                            'title' => $data->title,
                               'mType' => 'fancy3',
                            'suspended' => $suspended,
                            'ballRunning' => $ballRunning,
                            'sportId' => 10,
                            'slug' => 'jackpot',
                            'is_book' => $this->isBookOn($uid,$data->marketId, 'fancy3'),
                            'minStack' => $minStack,
                            'maxStack' => $maxStack,
                            'maxProfitLimit' => $maxProfitLimit,
                            'betDelay' => $betDelay,
                            'info' => $info,
                          
                            'no' => 0,
                            'no_rate' => 0,
                            'yes' => 0,
                            'yes_rate' => 0,
                        ];

                         if(!empty($data->marketId)){
                        $marketIdsArr[] =  $data->marketId;
                         }
                      
                      }
                  }     
                }

              $items = ['fancy2' => $itemsFancy,'fancy3' => $itemsFancy3];

              $response = ["status" => 1,'code'=>200, "updatedOn" => strtotime($updatedOn), "data" => [ 'title' => $title, "items" => $items , 'marketIdsArr' => $marketIdsArr ],'message'=>'Data Found !!'];
               

            }else{
                $response = ["status" => 0,'code'=>404, "data" => null, "message" => "This event is closed !!"];
                return $response;
                exit;
            }
        }
        return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

     //Event: Jackpot Khado
    public function jackpotKhado(Request $request)
    {
          
      try{

        $response = ['status'=>0,'code'=>400,'message'=>'Bad request !!!','data'=>null];
          
        if ( null != $request->route('id')) {
     
            $uid = Auth::guard('api')->user()->id;
            $eventId = $request->route('id');
            $marketIdsArr = [];
            $updatedOn = $this->updatedOn($uid);

            if (in_array($eventId, $this->checkUnBlockList($uid))) {
                $response = ["status" => 1, 'code'=>200,"updatedOn" => $updatedOn, "data" => null, "msg" => "This event is closed !!"];
                return $response;
                exit;
            }

            $eventArr = null;
            $items = [];
             $cache = Redis::connection();
            
             $eventData=$cache->get("Event_".$eventId);
             $event = json_decode($eventData);
           if(isset($event->status) && $event->status== 1 && $event->game_over == 0){
            if( $event != null){

                $title = $event->name;

                 $cache = Redis::connection();
                if( isset($cache)){ 
                    $Khado=$cache->get("Khado_".$eventId);
                    $marketList = json_decode($Khado);
                   
                }

                $items = [];
                $itemsKhado = [];
                if ($marketList != null) {
                    $suspended = $ballRunning = 0;
                    $dataVal = []; $info = '';
                    foreach ($marketList as $data) {

                    
                        $suspended = $ballRunning = 0;
                        $minStack = $data->min_stack;
                        $maxStack = $data->max_stack;
                        $maxProfitLimit = $data->max_profit_limit;
                        $info = $data->info;
                        if ($suspended != 1) {
                            $suspended = $data->suspended;
                        }
                        if ($ballRunning != 1) {
                            $ballRunning = $data->ball_running;
                        }

                        $betDelay = 0;
                     

                        $itemsKhado[] = [
                            'id' => $data->id,
                            'eventId' => $data->eid,
                            'marketId' => $data->marketId,
                            'title' => $data->title,
                            'suspended' => $suspended,
                            'ballRunning' => $ballRunning,
                            'mType' => 'khado',
                            'sportId' => 10,
                            'slug' => 'jackpot',
                            'is_book' => $this->isBookOn($uid,$data->marketId,'khado'),
                            'minStack' => $minStack,
                            'maxStack' => $maxStack,
                            'maxProfitLimit' => $maxProfitLimit,
                            'betDelay' => $betDelay,
                            'info' => $info,
                            'difference' => $data->diff,
                              'no' => 0,
                            'no_rate' => 0,
                            'yes' => 0,
                            'yes_rate' => 0,
                        ];

                        if(!empty($data->marketId)){
                          $marketIdsArr[] =  $data->marketId ;
                        }else{
                            $marketIdsArr = [];
                        }
                    }
                }

                $items = ['khado' => $itemsKhado];
                $response = ["status" => 1,'code'=>200, "updatedOn" => strtotime($updatedOn), "data" => [ 'title' => $title, "items" => $items , 'marketIdsArr' => $marketIdsArr],'message'=>'Data Found !!'];

            }else{
                $response = ["status" => 0,'code'=>404,  "data" => null, "message" => "This event is closed !!"];
                return $response;
                exit;
            }
         }

        }
        return $response;

      }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }




 //Event: Big Jackpot List
    public function jackpotList(Request $request)
    {

      try{
          $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
          $key=$this->haskKey();
          $requestId=$request->tnp;
          $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
          $hashKey = $request->header('hash');
          $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
          if($hashAuth==1) {


              $response = ['status' => 0, 'code' => 400, 'message' => 'Bad request !!!', 'data' => null];

              $request_data = json_decode(file_get_contents('php://input'), JSON_FORCE_OBJECT);
              
              $eventArr = $marketIdsArr1 = [];
              if (json_last_error() == JSON_ERROR_NONE) {
                  $r_data = $request_data;

                  $uid = Auth::user()->id;
                  $eventId = $r_data['event_id'];
                  $eventSlug = $r_data['slug'];

                  $updatedOn = strtotime(Auth::user()->updated_at->toDateTimeString());

                  if (in_array($eventId, $this->checkUnBlockList($uid))) {
                      $response = ["status" => 1, 'code' => 200, "updatedOn" => $updatedOn, "data" => null, "msg" => "This event is closed !!"];
                      return $response;
                      exit;
                  }

                  $eventArr = null;

                  $cache = Redis::connection();
                  $event = $cache->get("Event_" . $eventId);
                  $event = json_decode($event);
                  if ($event->status == 1 && $event->game_over == 0) {
                      if ($event != null) {


                          $jkGametype = $cache->get("Jackpot_type");
                          $jkGametype = json_decode($jkGametype);

                          foreach ($jkGametype as $data) {

                              $type = $data->id;
                              if ($eventSlug == $data->slug) {
                                  $gametitle = $data->title;
                                  $jackpotGame = $cache->get("Jackpot_data_" . $data->id . "_" . $eventId);
                                  $jackpotGame = json_decode($jackpotGame);

                                  if (!empty($jackpotGame)) {
                                      $Jackarraymkts = [];
                                      
                                       foreach ($jackpotGame as $item) {
                                          $marketId = $item->marketId;
                                          $type_id = $item->type;

                                          $eventArr[] = [
                                              'event_id' => $item->eid,
                                              'market_id' => $marketId,
                                              'title' => $item->title,
                                              'suspended' => $item->suspended,
                                              'gameid' => $type_id
                                          ];

                                       }
                                  }
                              }

                          }

                          $title = $event->name;
                          // Jackpot Data
                          $response = response()->json(["status" => 1, 'code' => 200, "updatedOn" => $updatedOn, "data" => ['title' => $title, 'game_title' => $gametitle, "items" => $eventArr], 'message' => 'Data Found !!']);

                      } else {
                          $response = ["status" => 1, 'code' => 200, "updatedOn" => $updatedOn, "data" => null, "message" => "This event is closed !!"];
                          return $response;
                          exit;
                      }
                  }
              }
              return $response;

          }else{
              return $response;
          }
     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }


//Event: Jackpot detail
public function jackpotDetail(Request $request){
          
        try{
            $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
            $key=$this->haskKey();
            $requestId=$request->tnp;
            $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
            $hashKey = $request->header('hash');
            $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
            if($hashAuth==1) {

                $eventArr = $marketIdsArr1 = [];
                if (json_last_error() == JSON_ERROR_NONE) {

                    $gametitle = '';
                    if (null != $request->id && isset($request->market_id) && $request->market_id != null) {
                        $uid = Auth::user()->id;
                        $eventId = $request->id;
                        $marketId = $request->market_id;
                        $gameid = $request->gameid;
                        $updatedOn = $this->updatedOn($uid);

                        $eventArr = null;
                        $cache = Redis::connection();
                        $event = $cache->get("Event_" . $eventId);

                        $eventData = json_decode($event);
                        if ($eventData != null) {
                            $rnr1 = $rnr2 = $subTitle = 'Undefined';
                            $title = $eventData->name;
                            $sportId = $eventData->sportId;
                            $eventTime = $eventData->time;
                            $runners = json_decode($eventData->runners);
                            $runner1 = $runners[0]->runner;
                            $runner2 = $runners[1]->runner;


                            $jackpotGame = $cache->get("Jackpot_market_" . $gameid . "_" . $eventId . "_" . $marketId);
                            $jackpot = json_decode($jackpotGame);


                            if (!empty($jackpot) && isset($jackpot->jackpotmarketList->game_over)) {

                                if ($jackpot->jackpotmarketList->game_over == 0) {

                                    $subTitle = $jackpot->jackpotmarketList->title;

                                    $jkGametype = $cache->get("Jackpot_type");
                                    $jkGametype = json_decode($jkGametype);
                                    foreach ($jkGametype as $data) {
                                        if ($gameid == $data->id) {
                                            $gametitle = $data->title;
                                        }
                                    }

                                }
                                

                                $marketList = $cache->get("Jackpot_" . $gameid . "_" . $marketId);
                                $marketList = json_decode($marketList);

                                if (!empty($marketList)) {
                                    $items = $betList = []; $itemList=null;
                                    if ($marketList != null) {


                                        $where = ([['mType', 'jackpot'], ['uid', $uid], ['status', 1], ['result', 'PENDING'], ['mid', $marketId]]);
                                        $betList = DB::connection('mongodb')->table('tbl_bet_pending_4')->select('id', 'uid', 'mType', 'sid', 'secId', 'price', 'size', 'win', 'loss', 'bType', 'result', 'description', 'created_on')

                                            ->where($where)
                                            ->orderBy('created_on', 'DESC')
                                            ->get();


                                       $jackpot_activity_id= $cache->get("jackpot_activity_id_" . $gameid . "_" . $marketId);

                                        $result = $newbetresult = $betresult = [];
                                        if ($betList != null && !empty($betList) && !$betList->isEmpty()) {

                                            $betList = (object) $betList;
                                            $min = $max = $totalLoss = $actualprofitloss=$jackpot_activity_id_old = 0;
                                          $betcount=count($betList); $oldbetcount=0;
                                            $userkey="Jackpot_userData_".$uid."_".$marketId;
                                            if ($cache->exists($userkey)) {
                                                $itemList = $cache->get("Jackpot_userData_".$uid."_".$marketId);
                                                $itemList=json_decode($itemList,16);
                                                $oldbetcount=$itemList['betcount'];
                                                if(isset($itemList['jackpot_activity_id'])) {
                                                    $jackpot_activity_id_old = $itemList['jackpot_activity_id'];
                                                }
                                            }


                                          //  $oldbetcount=1;
                                            if($betcount == $oldbetcount && $jackpot_activity_id==$jackpot_activity_id_old){
                                                $items=$itemList['data'];
                                            }else {

                                           // echo "<pre>";    print_r($betList); exit;
                                                foreach ($betList as $index => $bet) {

                                                    $totalLoss = $totalLoss + $bet['loss'];
                                                    $betArray[] = array('secId' => $bet['secId'], 'uid' => $bet['uid'], 'loss' => $bet['loss'], 'win' => $bet['win']);
                                                }

                                                foreach ($marketList as $key => $jackpotMarkets) {
                                                    if($jackpotMarkets->status==1) {
                                                        $totalWin = $totalLoss2 = $usertotalLoss = 0;
                                                        if (!empty($betArray)) {
                                                            foreach ($betArray as $bet) {
                                                                if ($bet['secId'] == $jackpotMarkets->secId) {
                                                                    $totalWin = $totalWin + $bet['win'];
                                                                    $usertotalLoss = $usertotalLoss + $bet['loss'];
                                                                }
                                                            }
                                                        }
                                                        $profitLoss = 0;
                                                        if ($totalWin > 0) {
                                                            $userLoss = $totalLoss - $usertotalLoss;
                                                            $finalexpose = $userLoss - $totalWin;
                                                            $profitLoss = -$finalexpose;
                                                        } else {
                                                            $profitLoss = -$totalLoss;
                                                        }

                                                        $items[] = [
                                                            'id' => $jackpotMarkets->id,
                                                            'sportId' => 10,
                                                            'event_id' => $eventId,
                                                            'market_id' => $jackpotMarkets->mid,
                                                            'sec_id' => $jackpotMarkets->secId,
                                                            'team_a' => $jackpotMarkets->col_1,
                                                            'team_a_player' => $jackpotMarkets->col_2,
                                                            'team_b' => $jackpotMarkets->col_3,
                                                            'team_b_player' => $jackpotMarkets->col_4,
                                                            'slug' => 'jackpot',
                                                            'rate' => $jackpotMarkets->rate,
                                                            'suspended' => $jackpotMarkets->suspended,
                                                            'profitloss' => $profitLoss,

                                                        ];
                                                    }
                                                }

                                                $resultArray = array('betcount' => $betcount, 'data' => $items,'jackpot_activity_id'=>$jackpot_activity_id);
                                                $resultArray1 = json_encode($resultArray);
                                                $cache->set("Jackpot_userData_" . $uid . "_" . $marketId, $resultArray1);
                                            }
                                        } else {
                                           // print_r($marketList);
                                         
                                            if ($marketList != null) {
                                                foreach ($marketList as $key => $jackpotMarkets) {
                                                    $profitLoss = 0;
                                                    $items[] = [
                                                        'id' => $jackpotMarkets->id,
                                                        'sportId' => 10,
                                                        'event_id' => $eventId,
                                                        'market_id' => $jackpotMarkets->mid,
                                                        'sec_id' => $jackpotMarkets->secId,
                                                        'team_a' => $jackpotMarkets->col_1,
                                                        'team_a_player' => $jackpotMarkets->col_2,
                                                        'team_b' => $jackpotMarkets->col_3,
                                                        'team_b_player' => $jackpotMarkets->col_4,
                                                        'slug' => 'jackpot',
                                                        'rate' => $jackpotMarkets->rate,
                                                        'suspended' => $jackpotMarkets->suspended,
                                                        'profitloss' => $profitLoss,

                                                    ];

                                                }
                                            }
                                        }
                                    }

                                    $jackpotData = $items;
                                    if (!empty($jackpotData)) {
                                        $jackpotSetting = null;
                                        $jackpotSetting = $cache->get("Jackpot_market_setting_" . $gameid . "_" . $eventId . "_" . $marketId);
                                        if (!empty($jackpotSetting)) {
                                            $jackpotSetting = json_decode($jackpotSetting);
                                        }
                                        $commentary = null;
                                        $com = $cache->get("Jackpot_info_" . $marketId);
                                        $comment = json_decode($com, true);
                                        if ($comment != null || $comment != '') {
                                            $commentary = [
                                                'rule' => $comment['rules'],
                                                'commentry' => $comment['commentary']
                                            ];
                                        }


                                        $eventArr = [
                                            'title' => $title,
                                            'sub_title' => $subTitle,
                                            'game_title' => $gametitle,
                                            'runner1' => $runner1,
                                            'runner2' => $runner2,
                                            'sport' => 'Jackpot',
                                            'sport_id' => $sportId,
                                            'jackpot' => $jackpotData,
                                            'setting' => $jackpotSetting,
                                            'commentary' => $commentary
                                        ];

                                        $this->marketIdsArr[] = $marketId;

                                        if ($betList != null) {
                                            $response = ["status" => 1, "updatedOn" => strtotime($updatedOn), "data" => ["items" => $eventArr, 'marketIdsArr' => $this->marketIdsArr, "betList" => $betList, "count" => count($betList)]];
                                        } else {
                                            $response = ["status" => 1, "updatedOn" => strtotime($updatedOn), "data" => ["items" => $eventArr, 'marketIdsArr' => $this->marketIdsArr, "betList" => [], "count" => 0]];
                                        }
                                        /* else of check !empty(market) */
                                    } else {
                                        $response = ["status" => 1, 'code' => 200, "updatedOn" => strtotime($updatedOn), "data" => null, "msg" => "This event is closed !!"];
                                    }
                                } else {
                                    $response = ["status" => 1, 'code' => 200, "updatedOn" => $updatedOn, "data" => null, "msg" => "No data found !!"];
                                }

                                /* else of check jackpot->game_over */
                            } else {
                                $response = ["status" => 1, 'code' => 200, "updatedOn" => strtotime($updatedOn), "data" => null, "msg" => "This event is closed !!"];
                            }

                        } else {
                            $response = ["status" => 1, 'code' => 200, "updatedOn" => $updatedOn, "data" => null, "msg" => "This event is closed !!"];
                        }

                    }

                }
                return $response;
            }else{
                return $response;
            }
     }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }
    


 //check database function
 public function checkUnBlockList($uId)
    { 
        $eventID =[];
        $user = DB::connection('mongodb')->table('tbl_user_event_status')->select('eid')->where('uid',$uId)->get();

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $eventID[] = $value->eid; 
            }

        }
         
        return $eventID;

 } 

     //Event: isBookOn
public function isBookOn($uid,$marketId, $sessionType){
    
        if($sessionType == 'khado'){

            $findBet = DB::connection('mongodb')->table("tbl_bet_pending_3")
                      ->where([['result','PENDING'],['uid',$uid],['mid',$marketId],['status',1]])
                      ->get();

        }elseif($sessionType == 'fancy3' || $sessionType == 'fancy2'){

              $findBet = DB::connection('mongodb')->table("tbl_bet_pending_2")
                      ->where([['result','PENDING'],['uid',$uid],['mid',$marketId],['status',1]])
                      ->get();
        }else{

        $findBet = DB::connection('mongodb')->table("tbl_bet_pending_4")
                      ->where([['result','PENDING'],['uid',$uid],['mid',$marketId],['status',1]])
                      ->get();
        }

        if (!$findBet->isEmpty()) {
            return '1';
        }
        return '0';
}


 //check database function
public function checkUnBlockSportList($uid){
      $sportId =[];
    
        $user = DB::connection('mongodb')->table('tbl_user_sport_status')->select('sid')->where('uid',$uId)->get();

        if(!empty($user) && $user != null){

            foreach ($user as $value) {
               $sportId[] = $value->sid; 
            }

        }
        
        return $sportId;
   

    }

     //check database function
    public function updatedOn($uid)
    {
      
        $user = DB::connection('mongodb')->table('tbl_user_info')->select('updated_on')->where('uid',$uid)->first();

        $updated_on = isset($user->updated_on)?$user->updated_on:null;

        return $updated_on;
    }

}
