<?php

namespace App\Http\Controllers\Event;
use App\Model\FancyFunction;
use App\Model\LotteryFunction;
use App\Model\JackpotFunction;
use App\Model\BinaryFunction;
use App\Model\KhadoBallbyballFunction;
use App\Model\MatchOddFunction;
use App\Model\BetFairFunction;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redis;



class BetPlaceController extends Controller
{
    private $marketData;
    private $marketOddsData;
    private $eventData;
    private $eventLimitData;
    private $sportData;
    private $sportMarketLimit;
    /**
     * Action requestGenerate
     * GET Request
     * for generate bet place token
     */
    public function requestGenerate()
    {
        $response =  [ "status" => 0, "code" => 400, "message" => "Bad request!"];
        try {
            $userId = Auth::id();
            $token = md5(uniqid(rand(), true));
            $redis = Redis::connection();
            $redisDataKey = $userId . '-BetRequestToken';
            $redisData = ['token' => $token];
            $redisDataJson = json_encode($redisData);
            $redis->set($redisDataKey, $redisDataJson);

            $response = ["status" => 1, "code" => 200, "token" => $token];
            return $response;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    /**
     * Action requestBetPlace
     * POST Request
     */
    public function requestBetPlace(Request $request)
    {
        $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bad request!"];

//        if( !$this->checkOrigin() ){
//            return response()->json($response);
//        }

        try{
            $user = Auth::user();
            $userId = $user->id;
            $redis = Redis::connection();
            $t1 = round(microtime(true) * 1000);
            if ( isset($request->csrf_token) && !empty($request->csrf_token) ){

                // new csrf_token check
                // $this->checkCsrfToken($userId,$request->csrf_token,$redis);

                if ( isset($request->price) && isset($request->size) && isset($request->rate)
                    && isset($request->bet_type) && isset($request->market_id) && !empty($request->size)
                    && !empty($request->rate) && !empty($request->bet_type) && !empty($request->market_id) ) {
                    $responseData = $this->verifyWithRedis($userId, $request, $redis);

                    if( $responseData['status'] == 1 ){

                        $betData = $responseData['data'];
                        $eventData = $this->eventData;

                        if ( empty($eventData) || $eventData->bet_allowed != 1 || $eventData->type == 'CLOSED' || $eventData->is_block == 1) {
                            $response['message'] = 'Bet can not place due to event closed or bet is not allowed !';
                            return $response; exit;
                        }

                        if( isset($eventData->type) && $eventData->type == 'UPCOMING' ){
                            if( isset( $eventLimitData->upcoming_min_stake ) && $eventLimitData->upcoming_min_stake > $betData['size'] ){
                                $response['message'] = 'Minimum stack value is '.$eventLimitData->upcoming_min_stake;
                                return $response; exit;
                            }
                            if( isset( $eventLimitData->upcoming_max_stake ) && $eventLimitData->upcoming_max_stake < $betData['size'] ){
                                $response['message'] = 'Maximum stack value is '.$eventLimitData->upcoming_max_stake;
                                return $response; exit;
                            }
                        }

                        $sportData = $this->sportData;
                        if ( empty( $sportData ) || $sportData->is_block == 1 || $sportData->status != 1 ) {
                            $response['message'] = 'Bet can not place due to sport blocked or inactive !!';
                            return $response; exit;
                        }

                        $marketData = $this->marketData;
                        if ( !empty( $marketData ) ){
                            if ( isset( $marketData->bet_allowed ) && $marketData->bet_allowed == 0 ) {
                                $response['message'] = 'Bet can not place due to market bet is not allowed !';
                                return $response; exit;
                            }
                            if ( isset( $marketData->game_over ) && $marketData->game_over == 1 ) {
                                $response['message'] = 'Bet can not place due to game is over !';
                                return $response; exit;
                            }
//                            if ( isset( $marketData->suspended ) && $marketData->suspended == 1 ) {
//                                $response['message'] = 'Bet can not place due to market suspended !1';
//                                return $response; exit;
//                            }
                            if ( isset( $marketData->min_stack ) && $marketData->min_stack > $betData['bSize'] ) {
                                $response['message'] = 'Minimum stack value is ' . $marketData->min_stack;
                                return $response; exit;
                            }
                            if ( isset( $marketData->max_stack ) && $marketData->max_stack < $betData['bSize'] ) {
                                $response['message'] = 'Maximum stack value is ' . $marketData->max_stack;
                                return $response; exit;
                            }
                        }

                        if( $betData['mType'] == 'jackpot' ){
                            $jackpotLimitJson = $redis->get('Jackpot_market_setting_'.$betData['gameId'].'_'.$betData['eventId'].'_'.$betData['marketId']);
                            $jackpotLimit = json_decode($jackpotLimitJson);
                            if( !empty( $jackpotLimit ) ){
                                if( $jackpotLimit->min_stack > $betData['bSize'] ){
                                    $response['message'] = 'Minimum stack value is '.$jackpotLimit->min_stack;
                                    return $response; exit;
                                }
                                if( $jackpotLimit->max_stack < $betData['bSize'] ){
                                    $response['message'] = 'Maximum stack value is '.$jackpotLimit->max_stack;
                                    return $response; exit;
                                }
                            }else{
                                $response['message'] = 'Bet can not placed due to invalid data !';
                                return $response; exit;
                            }
                        }

                        $sportMarketLimit = $this->sportMarketLimit;
                        if( !empty( $sportMarketLimit ) ) {
                            if ( isset( $sportMarketLimit->min_stack ) && $sportMarketLimit->min_stack > $betData['bSize'] && $sportMarketLimit->min_stack != 0) {
                                $response['message'] = 'Minimum stack value is ' . $sportMarketLimit->min_stack;
                                return $response; exit;
                            }
                            if ( isset( $sportMarketLimit->min_stack ) && $sportMarketLimit->max_stack < $betData['bSize'] && $sportMarketLimit->max_stack != 0) {
                                $response['message'] = 'Maximum stack value is ' . $sportMarketLimit->max_stack;
                                return $response; exit;
                            }
                        }

                        if ( !empty( $user ) ) {
                            $betData['userId'] = $userId;
                            $betData['systemId'] = $user->systemId;
                            $betData['clientName'] = $user->username;

                            if ( $user->status != 1 || $user->role != 4 || $user->is_login != 1 ) {
                                $response['message'] = 'Bet can not place due to user status or role invalid !';
                                return $response; exit;
                            }

                            //Check User Block Status
                            $checkUserBlockStatus = DB::table('tbl_user_block_status')->where([['uid', $userId]])->first();
                            if ( !empty( $checkUserBlockStatus ) ) {
                                $response['message'] = 'Your account is blocked or bet locked by parent! Plz contact administrator !';
                                return $response; exit;
                            }

                            //Check User Profit Loss
                            $checkUserProfitLoss = DB::table('tbl_user_profit_loss')
                                ->where([['clientId', $userId ],['userId', 1]])->first();
                            if( empty( $checkUserProfitLoss ) ){
                                $response['message'] = 'Invalid user! Plz contact administrator !1';
                                return $response; exit;
                            }

                            //Check User Balance Info
                            $uAvaBal = $uBal = $uExpBal = $uPlBal = 0;
                            $userBalanceInfo = DB::table('tbl_user_info')
                                ->select(['balance','pl_balance','expose','pName'])->where([['uid',$userId]])->first();
                            if ( !empty( $userBalanceInfo ) ) {
                                $betData['masterName'] = $userBalanceInfo->pName;
                                $uBal = isset($userBalanceInfo->balance) ? (int)$userBalanceInfo->balance : 0;
                                $uExpBal = isset($userBalanceInfo->expose) ? (int)$userBalanceInfo->expose : 0;
                                $uPlBal = isset($userBalanceInfo->pl_balance) ? (int)$userBalanceInfo->pl_balance : 0;
                                $uAvaBal = ($uBal - $uExpBal + $uPlBal);
                                if ( round($uAvaBal) < 0 || ( round($uAvaBal) == 0 && round($uExpBal) == 0 ) ) {
                                    $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Insufficient Funds!"];
                                    return $response; exit;
                                }

                            } else {
                                $response['message'] = 'Invalid user! Plz contact administrator !2';
                                return $response; exit;
                            }
                        }else{
                            $response['message'] = 'Invalid user! Plz contact administrator !3';
                            return $response; exit;
                        }

                        $marketOddsData = $this->marketOddsData;
                        if( !empty($marketOddsData) ){
                            if( isset($marketOddsData->suspended) && $marketOddsData->suspended == 1 ){
                                $response['message'] = 'Bet can not place due to market suspended !2';
                                return $response; exit;
                            }
                            if (isset($marketOddsData->ballrunning) && $marketOddsData->ballrunning == 1) {
                                $response['message'] = 'Bet can not place due to ball running !';
                                return $response; exit;
                            }
                            if (isset($marketOddsData->ballRunning) && $marketOddsData->ballRunning == 1) {
                                $response['message'] = 'Bet can not place due to ball running !';
                                return $response; exit;
                            }
                            if (isset($marketOddsData->ball_running) && $marketOddsData->ball_running == 1) {
                                $response['message'] = 'Bet can not place due to ball running !';
                                return $response; exit;
                            }
                            if (isset($marketOddsData->difference)){
                                $betData['difference'] = $marketOddsData->difference;
                            }
                        }

                        if( isset($betData['mType']) ){
                            $sportMarketLimit = $this->sportMarketLimit;
                            if( $betData['mType'] != 'jackpot' ){
                                $checkBetAccepted = $this->checkBetAccepted($t1,$sportMarketLimit,$betData);
                                if ($checkBetAccepted['is_true'] == false) {
                                    $response['message'] = $checkBetAccepted['msg'];
                                    return $response; exit;
                                } else {
                                    if (isset($checkBetAccepted['rate']) && $checkBetAccepted['rate'] != 0 && $checkBetAccepted['rate'] != '-') {
                                        $oddsRate = $checkBetAccepted['rate'];
                                    }else{
                                        $response['message'] = 'Bet can not place due to rate not match !';
                                        return $response; exit;
                                    }
                                }
                            }

                            if( $this->eventLimitData != null ){
                                $eventLimitData = $this->eventLimitData;
                                $checkArr = ['match_odd','completed_match','tied_match','goals','set_market','winner'];
                                if( in_array($betData['mType'],$checkArr) && isset($eventLimitData->max_odd_limit) && ( $oddsRate > $eventLimitData->max_odd_limit ) ){
                                    $response['message'] = 'Your max odd limit is '.$eventLimitData->max_odd_limit.'! Bet can not placed !';
                                    return $response; exit;
                                }
                            }

                        }

                        $betInsertData = [
                            'systemId' => $betData['systemId'],
                            'uid' => $betData['userId'],
                            'sid' => $betData['sportId'],
                            'eid' => $betData['eventId'],
                            'mid' => $betData['marketId'],
                            'secId' => isset($betData['secId']) ? $betData['secId'] : 0,
                            'price' => $betData['bPrice'],
                            'size' => $betData['bSize'],
                            'bType' => $betData['bType'],
                            'rate' => isset($oddsRate) ? $oddsRate : $betData['bRate'],
                            'mType' => $betData['mType'],
                            'runner' => isset($betData['runnerName']) ? $betData['runnerName'] : $betData['marketName'],
                            'market' => $betData['marketName'],
                            'event' => $betData['eventName'],
                            'sport' => $betData['sportName'],
                            'ip_address' => $betData['ipAddress'],
                            'client' => $betData['clientName'],
                            'master' => $betData['masterName'],
                            'diff' => 0,
                            'is_match' => 0,
                            'ccr' => 0
                        ];

                        if( isset($betData['runnerName']) ){
                            $betInsertData['description'] = $betData['sportName'].' -> '.$betData['eventName'].' -> '.$betData['marketName'].' -> '.$betData['runnerName'];
                        }else{
                            $betInsertData['description'] = $betData['sportName'].' -> '.$betData['eventName'].' -> '.$betData['marketName'];
                        }

                        if( in_array($betData['mType'],['fancy','fancy2','ballbyball','khado','meter']) ){

                            if( $betData['mType'] == 'khado') {
                                $betInsertData['diff'] = $betData['bPrice'] + $betData['difference'];
                            }

                            if( $betData['mType'] == 'meter' ){
                                $betInsertData['loss'] = $betInsertData['win'] = 0;
                            }else{
                                if( $betData['bType'] == 'yes' && $betData['bRate'] != null ){
                                    $betInsertData['win'] = round(( $betData['bSize'] * $betData['bRate'] ) / 100 );
                                    $betInsertData['loss'] = $betData['bSize'];
                                }elseif( $betData['bType'] == 'no' && $betData['bRate'] != null ){
                                    $betInsertData['loss'] = round(( $betData['bSize'] * $betData['bRate'] ) / 100 );
                                    $betInsertData['win'] = $betData['bSize'];
                                }else{
                                    $betInsertData['loss'] = $betInsertData['win'] = $betData['bSize'];
                                }
                            }
                            $betInsertData['is_match'] = 1;
                            $betInsertData['secId'] = 0;
                        }elseif( $betData['mType'] == 'fancy3' || $betData['mType'] == 'oddeven' ){
                            if( $betData['bType'] == 'back' ){
                                $betInsertData['win'] = 0;
                                if ($betData['bPrice'] > 1) {
                                    $betInsertData['win'] = ($betData['bPrice'] - 1) * $betData['bSize'];
                                }
                                $betInsertData['loss'] = $betData['bSize'];
                            }else{
                                $betInsertData['loss'] = 0;
                                if ($betData['bPrice'] > 1) {
                                    $betInsertData['loss'] = ($betData['bPrice'] - 1) * $betData['bSize'];
                                }
                                $betInsertData['win'] = $betData['bSize'];
                            }
                            $betInsertData['is_match'] = 1;
                            $betInsertData['secId'] = 0;
                        }elseif( $betData['mType'] == 'bookmaker' ){
                            if( $betData['bType'] == 'back' ){
                                $betInsertData['win'] = ( $betData['bSize'] * $betData['bPrice'] ) / 100;
                                $betInsertData['loss'] = $betData['bSize'];
                            }else{
                                $betInsertData['loss'] = ( $betData['bSize'] * $betData['bPrice'] ) / 100;
                                $betInsertData['win'] = $betData['bSize'];
                            }
                            $betInsertData['is_match'] = 1;
                        }elseif( $betData['mType'] == 'virtual_cricket' ){
                            if( $betData['bPrice'] > 1 ){
                                $betInsertData['win'] = ( $betData['bPrice'] - 1 ) * $betData['bSize'];
                            }else{
                                $betInsertData['win'] = 0;
                            }
                            $betInsertData['loss'] = $betData['bSize'];
                            $betInsertData['is_match'] = 1;
                        }elseif( $betData['mType'] == 'cricket_casino' ){
                            $betInsertData['win'] = 0;
                            if( in_array($betData['bPrice'],[0,1,2,3,4,5,6,7,8,9]) ){
                                $betInsertData['win'] = ( $betData['bRate'] - 1 ) * $betData['bSize'];
                            }
                            $betInsertData['loss'] = $betData['bSize'];
                            $betInsertData['is_match'] = 1;
                        }else{
                            if( $betData['bType'] == 'back' && trim( $betInsertData['rate'] ) >= trim( $betInsertData['price'] ) ){
                                $betInsertData['is_match'] = 1;
                                $betInsertData['price'] = $betInsertData['rate'];
                            }
                            if( $betData['bType'] == 'lay' && trim( $betInsertData['rate'] ) <= trim( $betInsertData['price'] ) ){
                                $betInsertData['is_match'] = 1;
                                $betInsertData['price'] = $betInsertData['rate'];
                            }
                            if( $betData['bType'] == 'back' ){
                                if( $betData['bPrice'] > 1 ){
                                    $betInsertData['win'] = ( $betData['bPrice'] - 1 ) * $betData['bSize'];
                                }else{
                                    $betInsertData['win'] = 0;
                                }
                                $betInsertData['loss'] = $betData['bSize'];
                            }else{
                                $betInsertData['win'] = $betData['bSize'];
                                if( $betData['bPrice'] > 1 ){
                                    $betInsertData['loss'] = ( $betData['bPrice'] - 1 ) * $betData['bSize'];
                                }else{
                                    $betInsertData['loss'] = $betData['bSize'];
                                }
                            }

                            if( in_array( $betData['mType'], ['match_odd','goals','set_market'] ) ){
                                $commRate = $this->clientCommissionRate();
                                if( $betData['bType'] == 'back' ){
                                    $betInsertData['ccr'] = round ( ( ( $betInsertData['win'] ) * $commRate )/100 );
                                }else{
                                    $betInsertData['ccr'] = round ( ( ( $betInsertData['size'] ) * $commRate )/100 );
                                }
                            }

                            if( $betInsertData['is_match'] != 1){
                                $response['message'] = 'Odd change, Un match bet can not accepted !';
                                return $response; exit;
                            }

                            if( isset( $eventLimitData->overall_profit_limit ) && $eventLimitData->overall_profit_limit < $betInsertData['win'] ){
                                $response['message'] = 'Maximum profit value is '.$eventLimitData->overall_profit_limit;
                                return $response; exit;
                            }

                        }


                        if( $betInsertData != null ){
                            $mTypeArr1 = ['set_market','goals','winner','match_odd','completed_match','tied_match','bookmaker','virtual_cricket'];
                            $mTypeArr2 = ['fancy','fancy2','fancy3','oddeven'];
                            $mTypeArr3 = ['meter','khado','ballbyball'];
                            $mTypeArr4 = ['cricket_casino','jackpot'];
                            $mTypeArr5 = ['USDINR','GOLD','SILVER','EURINR','GBPINR','ALUMINIUM','COPPER','CRUDEOIL','ZINC'];
                            if( in_array($betData['mType'],$mTypeArr1) ){
                                $tbl = 'tbl_bet_pending_1';
                            }else if( in_array($betData['mType'],$mTypeArr2) ){
                                $tbl = 'tbl_bet_pending_2';
                            }else if( in_array($betData['mType'],$mTypeArr3) ){
                                $tbl = 'tbl_bet_pending_3';
                            }else if( in_array($betData['mType'],$mTypeArr4) ){
                                $tbl = 'tbl_bet_pending_4';
                            }else if( in_array($betData['mType'],$mTypeArr5) ){
                                $tbl = 'tbl_bet_pending_5';
                            }else{
                                dd($betData['mType']);
                            }

                            //dd($betInsertData);
                            $betId = DB::table($tbl)->insertGetId($betInsertData);
                            if( $betId != null ){
                                $oldExpose = $this->getAllUserMarketExpose($betData);
                                $newExposeData = $this->getNewUserMarketExpose($betData,$redis);
                                $newExpose = $newExposeData['expose'];
                                $newProfit = $newExposeData['profit'];
                                $runnerExpose = isset($newExposeData['runnerExpose']) ? $newExposeData['runnerExpose'] : null;
                                $newExpBal = $oldExpose+$newExpose;
                                $uAvaBal = 0;
                                if( $uPlBal > 0 ){ $uAvaBal = ( $uBal + $uPlBal );
                                }else{ $uAvaBal = ( $uBal -(-1)*$uPlBal ); }
                                if( $uAvaBal > 0 ){ $uAvaBal = ( $uAvaBal -(-1)*$newExpBal); }

                                if ( round($uAvaBal) < 0 ) {
                                    DB::table($tbl)->where([['id',$betId]])->delete();
                                    $response['message'] = 'Insufficient Funds !';
                                    return $response; exit;
                                }else{
                                    $checkMaxProfitLimit = $this->checkMaxProfitLimit($newProfit,$newExpose,$betData);
                                    if( ( $checkMaxProfitLimit['is_true'] == false ) ){
                                        DB::table($tbl)->where([['id',$betId]])->delete();
                                        $response['message'] = $checkMaxProfitLimit['msg'];
                                        return $response; exit;
                                    }else{
                                        $this->setBookData($betData);
                                        $cDateTime = date('Y-m-d H:i:s');
                                        $updateUserArr = []; $updatedTime = time();
                                        $newExpBal = (-1)*($newExpBal);
                                        $updateBetArr = ['status' => 1,'created_on' => $cDateTime,'updated_on' => $cDateTime];
                                        DB::table($tbl)->where([['id',$betId]])->update($updateBetArr);

                                        $updateUserArr = ['balance' => round($uAvaBal),'expose' => round($newExpBal),'mywallet' => round($uBal) , "updated_time" => $updatedTime ];
                                        DB::table('tbl_user_info')
                                            ->where([['uid',$userId]])->update(['expose' => round($newExpBal),'updated_on' => $cDateTime]);

                                        if( !empty( $runnerExpose ) ){
                                            foreach ( $runnerExpose as $rnrData) {
                                                $this->updateUserProfitExpose($betData,$rnrData);
                                            }
                                        }
                                        $this->updateUserProfitExpose2($betData,$newExpose,$newProfit);

                                        $response = ['status' => 1,'code' => 200];
                                        $response['data'] = ['balance' => $updateUserArr,'betId' => md5($betId) ];
                                        $response['message'] = 'Bet '.$betData['bType'].' RUN,</br>Placed '.$betData['bSize'].' @ '.$betData['bPrice'].' Odds </br> Matched '.$betData['bSize'].' @ '.$betData['bRate'].' Odds';
                                    }
                                }
                            }
                        }

                        if( isset( $response ) && ( $response['status'] == 1 )
                            && ( $response['code'] == 200 ) && ( $response['data'] != null ) ){
                            $logData['title'] = 'Bet Place';
                            $logData['description'] = 'Bet Place Activity';
                            $logData['request'] = $request->input();
                            $logData['response'] = $response;
                            if( $this->marketOddsData != null ){
                                $logData['currentOdds'] = json_encode($this->marketOddsData);
                            }
                            $this->addNotificationBetPlace($logData);
                        }

                        return $response; exit;

                    }else{
                        return response()->json($responseData);
                    }

                }else{
                    return $response;
                }
            }else{
                return $response;
            }

            }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // check checkCsrfToken
    public function checkCsrfToken($userId,$csrfToken,$redis) {
        if( isset( $csrfToken ) ){
            $redisDataKey = $userId . '-BetRequestToken';
            $redisDataJson = $redis->get($redisDataKey);
            $redisData = json_decode($redisDataJson);

            if( !empty($redisData) && isset($redisData->token) ){
                if( $redisData->token != $csrfToken ){
                    $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!1"];
                    return $response; exit;
                }else{
                    $redis->del($redisDataKey);
                }

            }else{
                $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!2"];
                return $response; exit;
            }

        }else{
            $response =  [ "status" => 0 ,"code" => 400 ,"data" => null, "message" => "Bet Place Call Invalid !!3"];
            return $response; exit;
        }
    }

    // check verify data with redis data
    public function verifyWithRedis($uid,$request,$redis){

        $response = ["status" => 0, "code" => 400, "data" => null, "message" => "Invalid Data! Plz Try Again 123"];

        try{
            $betData = [];
            $betData['ipAddress'] = $this->getClientIp();
            $marketId = $request->market_id;
            $secId = $request->sec_id;
            if( isset($request->gameId) ){
                $gameId = $betData['gameId'] = $request->gameId;
                $eventId = $request->event_id;
                $marketDataJson = $redis->get('Jackpot_market_'.$gameId.'_'.$eventId.'_'.$marketId);
                $marketData = json_decode($marketDataJson);
            }else{
                $marketDataJson = $redis->get('Market_'.$marketId);
                $marketData = json_decode($marketDataJson);
            }


            $this->marketData = $marketData;
            if( !empty($marketData) ){
                $betData['marketId'] = $marketId;
                $betData['secId'] = $secId;
                $betData['bPrice'] = $request->price;
                $betData['bRate'] = $request->rate;
                $betData['bSize'] = $request->size;
                $betData['bType'] = $request->bet_type;
                $betData['difference'] = 0;
                if( isset($marketData->event_id) ){
                    $betData['eventId'] = $marketData->event_id;
                }
                if( isset($marketData->eventId) ){
                    $betData['eventId'] = $marketData->eventId;
                }
                if( isset($marketData->eid) ){
                    $betData['eventId'] = $marketData->eid;
                }
                if( isset($marketData->sportId) ){
                    $betData['sportId'] = $marketData->sportId;
                }
                if( isset($marketData->sid) ){
                    $betData['sportId'] = $marketData->sid;
                }
                if( isset($marketData->mType) ){
                    $betData['mType'] = $marketData->mType;
                }
                if( !isset($marketData->mType) && isset($marketData->slug) ){
                    $betData['mType'] = $marketData->slug;
                }

                if( isset($marketData->title) ){
                    $betData['marketName'] = trim($marketData->title);
                }
                if( isset($marketData->marketName) ){
                    $betData['marketName'] = trim($marketData->marketName);
                }
                if( isset($marketData->runners) ){
                    $secIdArr = []; $secIdRunner = [];
                    $marketRunnersData = json_decode($marketData->runners);
                    foreach ( $marketRunnersData as $runners ){
                        $secIdArr[] = $runners->secId;
                        $secIdRunner[$runners->secId] = $runners->runner;
                    }
                    if( isset( $marketRunnersData[0]->mType ) ){
                        $betData['mType'] = trim($marketRunnersData[0]->mType);
                    }
                    if( $secIdArr != null && !in_array( $secId , $secIdArr ) ){
                        return $response;
                    }
                    if( $secIdRunner != null && isset( $secIdRunner[$secId] ) ){
                        $betData['runnerName'] = trim($secIdRunner[$secId]);
                    }

                }

                // if mType is not found
                if( isset($marketData->marketId) && !isset($betData['mType']) ){
                    if( strpos($marketData->marketId,'F2') > 0 ){
                        $betData['mType'] = 'fancy2';
                    }
                    if( strpos($marketData->marketId,'F3') > 0 ){
                        $betData['mType'] = 'fancy3';
                        if( isset( $betData['sportId'] ) && $betData['sportId'] == 11 ){
                            $betData['mType'] = 'cricket_casino';
                        }
                    }
                    if( strpos($marketData->marketId,'OE') > 0 ){
                        $betData['mType'] = 'oddeven';
                    }
                    if( strpos($marketData->marketId,'BB') > 0 ){
                        $betData['mType'] = 'ballbyball';
                    }
                    if( strpos($marketData->marketId,'KD') > 0 ){
                        $betData['mType'] = 'khado';
                    }
                    if( strpos($marketData->marketId,'MT') > 0 ){
                        $betData['mType'] = 'meter';
                    }
                }

                $eventId = $betData['eventId'];
                $eventDataJson = $redis->get("Event_" . $eventId);
                $eventData = json_decode($eventDataJson);
                $this->eventData = $eventData;
                if( !empty( $eventData ) ){
                    if( isset( $eventData->name ) ){
                        $betData['eventName'] = $eventData->name;
                    }

                    $eventLimitDataJson = $redis->get("Event_limit_" . $eventId);
                    $eventLimitData = json_decode($eventLimitDataJson);
                    $this->eventLimitData = $eventLimitData;
                }

                $sportId = $betData['sportId'];
                $sportDataJson = $redis->get("Sport_" . $sportId);
                $sportData = json_decode($sportDataJson);
                $this->sportData = $sportData;
                if( !empty( $sportData ) ){
                    if( isset( $sportData->name ) ){
                        $betData['sportName'] = $sportData->name;
                    }
                }

                if ( isset($betData['mType']) && $betData['mType'] != 'cricket_casino' ) {
                    $marketOddsDataJson = $redis->get($marketId);
                    $marketOddsData = json_decode($marketOddsDataJson);
                    $this->marketOddsData = $marketOddsData;
                }
            }

            if( strpos($marketId,'JKPT') > 0 ){
                $betData['mType'] = 'jackpot';
                $betData['sportId'] = 10;
            }

            $mType = $betData['mType'];
            $sportId = $betData['sportId'];

            $this->sportMarketLimitData($uid,$mType,$sportId);

            $response = ["status" => 1, "data" => $betData];
            return $response;

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    public function sportMarketLimitData($uid,$mType,$sportId){
        $sportMarketLimit = [];
        $mTypeArr1 = ['bookmaker'];
        $mTypeArr2 = ['set_market','goals','winner','match_odd','tied_match','completed_match'];
        $mTypeArr3 = ['fancy','fancy2','fancy3','oddeven'];
        $mTypeArr4 = ['meter','khado','ballbyball'];
        if( $sportId == 1 ){ $key = 'football_'; } elseif ( $sportId == 2 ){ $key = 'tennis_'; } else { $key = 'cricket_'; }

        if( in_array($mType,$mTypeArr1) ){
            $select = $key.'bookmaker';
        }elseif ( in_array($mType,$mTypeArr2) ){
            $select = $key.'matchodd';
        }elseif ( in_array($mType,$mTypeArr3) || in_array($mType,$mTypeArr4) ){
            $select = $key.'fancy';
        }else{
            $select = $key.'fancy';
        }

        $sportMarketLimit = DB::table('tbl_user_sport_market_limits')
            ->select($select)->where([['uid',$uid]])->first();

        if ( $sportMarketLimit != null ) {
            $sportMarketLimit = json_decode($sportMarketLimit->$select);
        }

        $this->sportMarketLimit = $sportMarketLimit;
    }

    /* check odds and bet delay for all market */
    public function checkBetAccepted($t1,$sportMarketLimit,$betData)
    {
        try{
            $maxDelay = 1000000; $maxDelay1 = 10;
            $t2 = round(microtime(true) * 1000);
            $mType = $betData['mType'];
            $bType = $betData['bType'];
            $bRate = $betData['bRate'];
            $bPrice = $betData['bPrice'];
            $sportId = $betData['sportId'];
            $secId = $betData['secId'];
            $marketId = $betData['marketId'];
            if( !empty($this->marketData) && $this->marketData != null ){
                $market = $this->marketData;
                $betDelay = isset( $market->bet_delay ) ? $market->bet_delay : 0;

                if(!empty($sportMarketLimit)) {
                    if (!empty($sportMarketLimit->bet_delay)) {
                        if($sportMarketLimit->bet_delay > $betDelay){
                            $betDelay = $sportMarketLimit->bet_delay;
                        }
                    }
                }

                if( $betDelay != 0 ){
                    $betDelay = ( $betDelay*1000000 - ( $t2-$t1 ) );
                    usleep($betDelay);
                }

                $marketOddsData = $this->marketOddsData;
                if( !empty($marketOddsData) ){
                    $ctime = time();
                    if( isset($marketOddsData->lastUpdatedTime) ){
                        $dff = $ctime-$marketOddsData->lastUpdatedTime;
                    }else{
                        $dff = $ctime-$marketOddsData->time;
                    }

                    $mTypeArr1 = ['set_market','goals','winner','match_odd','tied_match','completed_match'];
                    $mTypeArr2 = ['bookmaker','virtual_cricket'];
                    $mTypeArr3 = ['fancy','USDINR','GOLD','SILVER','EURINR','GBPINR','ALUMINIUM','COPPER','CRUDEOIL','ZINC'];
                    $mTypeArr4 = ['meter','fancy2','ballbyball','khado'];
                    $mTypeArr5 = ['oddeven','fancy3'];

                    if( in_array($mType,$mTypeArr1) ){
                        if( $dff < $maxDelay1 ){
                            $rate = $diff = 0;
                            if (isset($marketOddsData->lastUpdatedTime)) {
                                $diff = time() - $marketOddsData->lastUpdatedTime;
                            }

                            if ($diff <= $betDelay && $marketOddsData->status != 'SUSPENDED') {
                                foreach ($marketOddsData->odds as $odds) {
                                    if ($odds->selectionId == $secId) {
                                        if ($bType == 'back') {
                                            $rate = $odds->backPrice1;
                                        }
                                        if ($bType == 'lay') {
                                            $rate = $odds->layPrice1;
                                        }
                                    }
                                }

                                if( $sportId != 4 ){ $rateDiff = 20; }else{ $rateDiff = 10; }
                                if( $bType == 'back' && ( trim($rate) - trim($bRate) )*100 > $rateDiff ){
                                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to rate not match !' ];  exit;
                                }
                                if( $bType == 'lay' && ( trim($bRate) - trim($rate) )*100 > $rateDiff ){
                                    return [ 'is_true' => false , 'msg' => 'Bet can not place due to rate not match !' ];  exit;
                                }

                                return ['is_true' => true, 'rate' => $rate];  exit;
                            }
                        }else{
                            return [ 'is_true' => false , 'msg' => 'Bet can not place due to rate stuck for long time !' ];  exit;
                        }
                    }
                    if( in_array($mType,$mTypeArr2) ){
                        $runnersArr = []; $runnersData = $marketOddsData->runners;
                        if( $runnersData != null ){
                            foreach ( $runnersData as $runner ){
                                $runnersArr[$runner->secId] = [
                                    'lay' => $runner->lay,
                                    'back' => $runner->back,
                                    'suspended' => $runner->suspended,
                                ];
                            }
                        }
                        if( $runnersArr != null && $mType == 'bookmaker') {
                            if ( isset($runnersArr[$secId])
                                && ( $runnersArr[$secId]['suspended'] == 1 ) ) {
                                return ['is_true' => false, 'msg' => 'Bet can not place due to market suspended!3'];
                            }
                            if ($bType == 'lay') {
                                // old && trim((int)$runners[$request->sec_id]['lay']) <= trim((int)$request->price)
                                if (isset($runnersArr[$secId]) && trim((int)$runnersArr[$secId]['lay']) != 0
                                    && trim((int)$runnersArr[$secId]['lay']) == trim((int)$bPrice)) {
                                    return ['is_true' => true, 'rate' => $bRate, 'price' => $bPrice];
                                } else {
                                    return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !1!'];
                                }
                            } else {
                                // old && trim((int)$runners[$request->sec_id]['back']) >= trim((int)$request->price)
                                if (isset($runnersArr[$secId]) && trim((int)$runnersArr[$secId]['back']) != 0
                                    && trim((int)$runnersArr[$secId]['back']) == trim((int)$bPrice)) {
                                    return ['is_true' => true, 'rate' => $bRate, 'price' => $bPrice];
                                } else {
                                    return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !2!'];
                                }
                            }
                        }

                        if($runnersArr != null && $mType == 'virtual_cricket') {
                            if ( isset($runnersArr[$secId])
                                && ( $runnersArr[$secId]['suspended'] == 1 ) ) {
                                return ['is_true' => false, 'msg' => 'Bet can not place due to market suspended!4'];
                            }
                            if ($bType == 'lay') {
                                if (isset($runnersArr[$secId]) && trim($runnersArr[$secId]['lay']) == trim($bPrice)) {
                                    return ['is_true' => true, 'rate' => $bRate, 'price' => $bPrice];
                                } else {
                                    return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !3!'];
                                }
                            } else {
                                if (isset($runnersArr[$secId]) && trim($runnersArr[$secId]['back']) == trim($bPrice)) {
                                    return ['is_true' => true, 'rate' => $bRate, 'price' => $bPrice];
                                } else {
                                    return ['is_true' => false, 'msg' => 'Rate changed! Bet can not placed !4!'];
                                }
                            }
                        }
                    }
                    if( in_array($mType,$mTypeArr3) ){
                        $dff = $ctime-$marketOddsData->time;
                        if( $dff < $maxDelay ){
                            $rate = 0;
                            if( $bType == 'no' ){
                                $price = $marketOddsData->data->no;
                                $rate = $marketOddsData->data->no_rate;
                            }
                            if( $bType == 'yes' ){
                                $price = $marketOddsData->data->yes;
                                $rate = $marketOddsData->data->yes_rate;
                            }

                            if( ( $bType == 'no' && $bRate == $rate && $bPrice <= $price )
                                || ( $bType == 'yes' && $bType == $rate && $bType >= $price ) ){
                                return [ 'is_true' => true , 'rate' => $rate , 'price' => $price];  exit;
                            }else{
                                return [ 'is_true' => false , 'msg' => 'Bet can not placed !!1' ];  exit;
                            }
                        }else{
                            return [ 'is_true' => false , 'msg' => 'Bet can not placed !!2' ];   exit;
                        }
                    }
                    if( in_array($mType,$mTypeArr4) ){

                        if( $marketOddsData->suspended == 1 || $marketOddsData->ball_running == 1 ){
                            return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running!' ];  exit;
                        }


                        if ( $bType == 'no') {
                            if( ( $mType != 'khado' ) && ( trim( (int)$marketOddsData->no ) == trim( (int)$bPrice ) )
                                && ( trim( (int)$marketOddsData->no_rate ) == trim( (int)$bRate ) ) ){
                                return [ 'is_true' => true , 'rate' => $bRate , 'price' => $bPrice ];
                            }elseif( ( $mType == 'khado' ) && ( trim( (int)$marketOddsData->no_rate ) == trim( (int)$bRate ) ) ){
                                return [ 'is_true' => true , 'rate' => $bRate , 'price' => $bPrice ];
                            }else{
                                return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !5!' ];
                            }
                        }else{
                            if( ( $mType != 'khado' ) && ( trim( (int)$marketOddsData->yes ) == trim( (int)$bPrice ) )
                                && ( trim( (int)$marketOddsData->yes_rate ) == trim( (int)$bRate ) ) ){
                                return [ 'is_true' => true , 'rate' => $bRate , 'price' => $bPrice ];
                            }elseif( ( $mType == 'khado' ) && ( trim( (int)$marketOddsData->yes_rate ) == trim( (int)$bRate ) ) ){
                                return [ 'is_true' => true , 'rate' => $bRate , 'price' => $bPrice ];
                            }else{
                                return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !6!' ];
                            }

                        }

                    }
                    if( in_array($mType,$mTypeArr5) ){
                        if( $marketOddsData->suspended == 1 || $marketOddsData->ball_running == 1 ){
                            return [ 'is_true' => false , 'msg' => 'Bet can not place due to market suspended or ball running !' ];  exit;
                        }

                        if ( $bType == 'back') {
                            if( isset($marketOddsData->odd) && ( trim( $marketOddsData->odd ) == trim( $bPrice ) ) ){
                                return [ 'is_true' => true , 'rate' => $bPrice , 'price' => $bPrice ];
                            }elseif( isset($marketOddsData->back) && ( trim( $marketOddsData->back ) == trim( $bPrice ) ) ){
                                return [ 'is_true' => true , 'rate' => $bPrice , 'price' => $bPrice ];
                            }else{
                                return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !7!' ];
                            }
                        }else{
                            if( isset($marketOddsData->even) && ( trim( $marketOddsData->even ) == trim( $bPrice ) ) ){
                                return [ 'is_true' => true , 'rate' => $bPrice , 'price' => $bPrice ];
                            }elseif( isset($marketOddsData->lay) && ( trim( $marketOddsData->lay ) == trim( $bPrice ) ) ){
                                return [ 'is_true' => true , 'rate' => $bPrice , 'price' => $bPrice ];
                            }else{
                                return [ 'is_true' => false , 'msg' => 'Rate changed! Bet can not placed !8!' ];
                            }
                        }
                    }

                }else{
                    if( $mType == 'cricket_casino' ){
                        return [ 'is_true' => true , 'rate' => $bRate , 'price' => $bPrice ];
                    }else{
                        return [ 'is_true' => false , 'msg' => 'Bet can not placed !!3' ];
                    }
                }

            }else{
                return [ 'is_true' => false , 'msg' => 'Bet can not placed !!4' ];
            }

        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Function get Profit Loss MatchOdd
    public function getProfitLossMatchOdd($betData,$secId)
    {
        $marketId = $betData['marketId'];
        $userId = $betData['userId'];
        $mType = $betData['mType'];
        $tbl = 'tbl_bet_pending_1';
        $where = [['mid',$marketId],['uid',$userId],['result','PENDING'],['mType',$mType]];
        $allList = DB::table($tbl)->select(['win','loss','secId','bType','is_match'])
            ->where($where)->whereIn('status',[1,5])->get();
        $backWin = $layWin = $backLoss = $layLoss = $unMatchLoss = 0;
        if( $allList->isNotEmpty() ){
            // print_r($allList);
            foreach ( $allList as $list ){
                if( $list->is_match == 1){
                    if( $list->secId == $secId && $list->bType == 'back'){
                        $backWin = $backWin+$list->win;
                    }elseif( $list->secId != $secId && $list->bType == 'lay'){
                        $layWin = $layWin+$list->win;
                    }elseif( $list->secId == $secId && $list->bType == 'lay'){
                        $layLoss = $layLoss+$list->loss;
                    }elseif( $list->secId != $secId && $list->bType == 'back'){
                        $backLoss = $backLoss+$list->loss;
                    }else{
                        $unMatchLoss = $unMatchLoss+$list->loss;
                    }
                }else{
                    $unMatchLoss = $unMatchLoss+$list->loss;
                }
            }
        }

        $totalWin = $backWin+$layWin;
        $totalLoss = $backLoss+$layLoss+$unMatchLoss;
        $total = $totalWin-$totalLoss;
        $totalLossEx = $backLoss+$layLoss;
        $totalExpose = $totalWin-$totalLossEx;
        $runnerExpose = ['totalExpose' => $totalExpose,'totalWin' => $totalWin,'totalLossEx' => $totalLossEx,'secId' => $secId];

        return ['total' => $total,'runnerExpose' => $runnerExpose];
    }

    // Function get Profit Loss Fancy
    public function getProfitLossFancy($betData)
    {
        $marketId = $betData['marketId'];
        $userId = $betData['userId'];
        $mType = $betData['mType'];
        if( in_array($mType,['ballbyball','khado']) ){
            $tbl = 'tbl_bet_pending_3';
        }else{
            $tbl = 'tbl_bet_pending_2';
        }
        $betList = DB::table($tbl)->select(['bType','price','win','loss'])
            ->where([['mType',$mType],['uid',$userId],['result','Pending'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();
        $result = $newbetresult = [];
        if( $betList->isNotEmpty() ){
            $result = $betResult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betResult[] = [
                    'price' => $bet->price,
                    'bType' => $bet->bType,
                    'loss' => $bet->loss,
                    'win' => $bet->win
                ];
                if ($index == 0) { $min = $bet->price; $max = $bet->price; }
                if ($min > $bet->price){ $min = $bet->price; }
                if ($max < $bet->price){ $max = $bet->price; }
            }

            $min = $min-1; $max = $max+1;
            $win = $loss = 0;
            $count = $min;
            $totalBetCount = count($betResult);
            foreach ( $betResult as $key => $value ) {
                $val = $value['price'] - $count;
                $minval = $value['price'] - $min;
                $maxval = $max-$value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newResult = [];
                $top = $bottom = $profitCount = $lossCount = 0;

                for( $i= 0; $i < $minval; $i++){
                    if($bType == 'no'){
                        $top = $top+$value['win'];
                        $profitCount++;
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => $value['win']
                        ];
                    }else{
                        $bottom = $bottom + $value['loss'];
                        $lossCount++;
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose'=> (-1)*$value['loss']
                        ];
                    }
                    $count++;
                }

                for( $i= 0; $i <= $maxval; $i++){
                    if($bType == 'no'){
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => (-1)*$value['loss']
                        ];
                        $bottom = $bottom + $value['loss'];
                        $lossCount++;
                    }else{
                        $top = $top + $value['win'];
                        $profitCount++;
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => $value['win']
                        ];
                    }

                    $count++;
                }
                $result[] = [
                    'count' => $value['price'],
                    'bType' => $value['bType'],
                    'profit' => $top,
                    'loss' => $bottom,
                    'profitCount' => $profitCount,
                    'lossCount' => $lossCount,
                    'newResult' => $newResult
                ];
            }

            $newBetArray = $newBetResult = [];
            $totalMaxCount = $max-$min;
            if( $totalMaxCount > 0 ){
                for( $i = 0; $i < $totalMaxCount; $i++ ){
                    $newBetArray1 = []; $finalExpose = 0;
                    for( $x = 0; $x < $totalBetCount; $x++ ){
                        $expose = $result[$x]['newResult'][$i]['expose'];
                        $finalExpose = $finalExpose+$expose;
                        $newBetArray1[] = [
                            'bPrice' => $result[$x]['count'],
                            'bType' => $result[$x]['bType'],
                            'expose' => $expose
                        ];
                    }
                    $newBetResult[] = $finalExpose;
                    $newBetArray[] = [
                        'exposeArray' => $newBetArray1,
                        'finalExpose' => $finalExpose
                    ];
                }
            }

            return $newBetResult;
        }
    }

    // Function get Profit Loss Binary
    public function getProfitLossBinary($betData)
    {
        $marketId = $betData['marketId'];
        $userId = $betData['userId'];
        $mType = $betData['mType'];
        $betList = DB::table('tbl_bet_pending_5')->select(['bType','price','win','loss'])
            ->where([['mType',$mType],['uid',$userId],['result','Pending'],['mid',$marketId]])
            ->whereIn('status',[1,5])->get();
        if( $betList != null ) {
            $result = $betResult = [];
            $min = $max = 0;

            foreach ($betList as $index => $bet) {
                $betResult[] = [
                    'price' => $bet->price,
                    'bType' => $bet->bType,
                    'loss' => $bet->loss,
                    'win' => $bet->win
                ];
                if ($index == 0) {
                    $min = $bet->price;
                    $max = $bet->price;
                }
                if ($min > $bet->price) {
                    $min = $bet->price;
                }
                if ($max < $bet->price) {
                    $max = $bet->price;
                }
            }

            $min = $min - 1;
            $max = $max + 1;
            $win = $loss = 0;
            $count = $min;
            $totalBetCount = count($betResult);
            foreach ($betResult as $key => $value) {
                $val = $value['price'] - $count;
                $minval = $value['price'] - $min;
                $maxval = $max - $value['price'];
                $bType = $value['bType'];
                $loss = $value['loss'];
                $newResult = [];
                $top = $bottom = $profitCount = $lossCount = 0;

                for ($i = 0; $i < $minval; $i++) {
                    if ($bType == 'no') {
                        $top = $top + $value['win'];
                        $profitCount++;
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => $value['win']
                        ];
                    } else {
                        $bottom = $bottom + $value['loss'];
                        $lossCount++;
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => (-1) * $value['loss']
                        ];
                    }
                    $count++;
                }

                for ($i = 0; $i <= $maxval; $i++) {
                    if ($bType == 'no') {
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => (-1) * $value['loss']
                        ];
                        $bottom = $bottom + $value['loss'];
                        $lossCount++;
                    } else {
                        $top = $top + $value['win'];
                        $profitCount++;
                        $newResult[] = [
                            'count' => $count,
                            'price' => $value['price'],
                            'bType' => $value['bType'],
                            'totalBetCount' => $totalBetCount,
                            'expose' => $value['win']
                        ];
                    }

                    $count++;
                }
                $result[] = [
                    'count' => $value['price'],
                    'bType' => $value['bType'],
                    'profit' => $top,
                    'loss' => $bottom,
                    'profitCount' => $profitCount,
                    'lossCount' => $lossCount,
                    'newArray' => $newResult
                ];
            }

            $newBetArray = $newBetResult = [];
            $totalMaxCount = $max - $min;
            if ($totalMaxCount > 0) {
                for ($i = 0; $i < $totalMaxCount; $i++) {
                    $newBetArray1 = [];
                    $finalExpose = 0;
                    for ($x = 0; $x < $totalBetCount; $x++) {
                        $expose = $result[$x]['newArray'][$i]['expose'];
                        $finalExpose = $finalExpose + $expose;
                        $newBetArray1[] = [
                            'price' => $result[$x]['count'],
                            'bType' => $result[$x]['bType'],
                            'expose' => $expose
                        ];
                    }
                    $newBetResult[] = $finalExpose;
                    $newBetArray[] = [
                        'exposeArray' => $newBetArray1,
                        'finalExpose' => $finalExpose
                    ];
                }
            }
            return $newBetResult;
        }
    }

    // Function get Profit Loss Fancy3
    public function getProfitLossFancy3($betData)
    {
        $yesWin = $yesLoss = $noLoss = $noWin = 0;
        $where = [['result', 'Pending'], ['mType', $betData['mType']], ['uid', $betData['userId']], ['mid', $betData['marketId']]];
        $betList = DB::table('tbl_bet_pending_2')->select(['win','loss','bType'])
            ->where($where)->whereIn('status',[1,5])->get();
        if( $betList->isNotEmpty() ){
            foreach ( $betList as $list ){
                if( $list->bType == 'back' ){
                    $yesWin = $yesWin+$list->win;
                    $yesLoss = $yesLoss+$list->loss;
                }
                if( $list->bType == 'lay' ){
                    $noWin = $noWin+$list->win;
                    $noLoss = $noLoss+$list->loss;
                }
            }
        }

        $yesProfitLoss = round($yesWin-$noLoss);
        $noProfitLoss = round($noWin-$yesLoss) ;

        $newBetResult[] = $yesProfitLoss;
        $newBetResult[] = $noProfitLoss;
        return $newBetResult;
    }

    // Function get Profit Loss Meter
    public function getProfitLossMeter($betData)
    {
        $expose = 0;
        $betList = DB::table('tbl_bet_pending_3')->select(['size'])
            ->where([['mType',$betData['mType']],['uid',$betData['userId']],['result','Pending'],['mid',$betData['marketId']]])
            ->whereIn('status',[1,5])->get();
        if( $betList != null ){
            foreach ($betList as $bet) {
                $expose = $expose+( ($bet->size)*100 );
            }
            $expose = (-1)*$expose;
        }
        return $expose;
    }

    // Function get Jackpot
    public function getProfitLossJackpot($betData,$redis)
    {
        $exposeArr = [];
        $marketList = DB::table('tbl_bet_pending_4')->select('secId','mid','eid','win','loss')
            ->where([['uid',$betData['userId']],['eid',$betData['eventId']],['mid',$betData['marketId']],['result','PENDING']])
            ->whereIn('status',[1,5])->orderBy('created_on' ,'DESC')->get();

        if( $marketList != null ){
            $betArray = $betSecID = $exposeArr = [];

            foreach ( $marketList as $market ){
                $betArray[] = $market;
                $betSecID[] = $market->secId;
            }

            if( !empty( $betSecID ) ){
                $jackpotTypeJson = $redis->get('Jackpot_type');
                $jackpotType = json_decode($jackpotTypeJson);
                foreach ($jackpotType as $jackpot) {
                    $gameId = $jackpot->id;
                    $jackpotMarketJson = $redis->get('Jackpot_' . $gameId . '_' . $betData['marketId']);
                    $jackpotMarket = json_decode($jackpotMarketJson);
                    if( !empty( $jackpotMarket ) ){
                        foreach ( $jackpotMarket as $jackpot ) {
                            $secId = $jackpot->secId;
                            $total = $totalWin = $totalLoss = 0;
                            foreach ( $betArray as $bet ) {
                                if( $secId != $bet->secId ){
                                    $totalWin = $totalWin+$bet->win;
                                }else{
                                    $totalLoss = $totalLoss+$bet->loss;
                                }
                            }
                            $total = $totalWin-$totalLoss;
                            $exposeArr[] = $total;
                        }
                    }
                }
            }
        }

        return $exposeArr;

    }

    // Function get Profit Loss Cricket Casino
    public function getProfitLossCricketCasino($betData,$secId)
    {
        try{
            $total = 0;
            $where = [['mType','cricket_casino'], ['uid', $betData['userId']], ['mid',$betData['marketId']],['eid',$betData['eventId']]];
            $betList = DB::table('tbl_bet_pending_4')->select(['win','loss','secId'])
                ->where($where)->whereIn('status',[1,5])->get();
            $totalWin = $totalLoss = 0;
            if( $betList->isNotEmpty() ){
                foreach ($betList as $list){
                    if( $list->secId == $secId ){
                        $totalWin = $totalWin+$list->win; // IF RUNNER WIN
                    }else{
                        $totalLoss = $totalLoss+$list->loss; // IF RUNNER LOSS
                    }
                }
            }
            $total = $totalWin-$totalLoss;
            return $total;
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Function get user market expose
    public function getAllUserMarketExpose($betData){
        $expose = 0; $marketId = $betData['marketId']; $userId = $betData['userId'];
        $userMarketExpose = DB::table('tbl_user_market_expose')->select('expose')
            ->where([['uid',$userId],['status',1],['mid','!=',$marketId]])->get();
        if( $userMarketExpose->isNotEmpty() ){
            foreach ( $userMarketExpose as $data ){
                if( $data->expose > 0 ){
                    $expose = $expose+(-1)*$data->expose;
                }
            }
        }
        return $expose;
    }

    // Function get user market expose
    public function getNewUserMarketExpose($betData,$redis)
    {
        $mType = $betData['mType']; $returnData = [];
        if( in_array($mType,['goals','set_market','winner','match_odd','completed_match','tied_match','bookmaker','virtual_cricket']) ){
            $marketData = $this->marketData;
            $runnerData = json_decode($marketData->runners);
            if( $runnerData != null ){
                $minExpose = $maxProfit = 0; $profitLossArr = $runnerExpose = [];
                foreach ( $runnerData as $runner ){
                    $profitLossData = $this->getProfitLossMatchOdd($betData,$runner->secId);
                    $profitLossArr[] = $profitLossData['total'];
                    $runnerExpose[] = $profitLossData['runnerExpose'];
                }
                if( $profitLossArr != null ){
                    $minExpose = min($profitLossArr);
                    $maxProfit = max($profitLossArr);
                }
                $returnData = [
                    'expose' => $minExpose,
                    'profit' => $maxProfit,
                    'runnerExpose' => $runnerExpose
                ];
            }
        }elseif ( in_array($mType,['fancy','fancy2','ballbyball','khado']) ){
            $minExpose = $maxProfit = 0;
            $profitLossData = $this->getProfitLossFancy($betData);
            if( $profitLossData != null ){
                $minExpose = min($profitLossData);
                $maxProfit = max($profitLossData);
            }
            $returnData = [
                'expose' => $minExpose,
                'profit' => $maxProfit
            ];
        }elseif ( in_array($mType,['fancy3','oddeven']) ){
            $minExpose = $maxProfit = 0;
            $profitLossData = $this->getProfitLossFancy3($betData);
            if( $profitLossData != null ){
                $minExpose = min($profitLossData);
                $maxProfit = max($profitLossData);
            }
            $returnData = [
                'expose' => $minExpose,
                'profit' => $maxProfit
            ];
        }elseif ( in_array($mType,['meter']) ){
            $minExpose = $maxProfit = 0;
            $minExpose = $this->getProfitLossMeter($betData);
            $returnData = [
                'expose' => $minExpose,
                'profit' => $maxProfit
            ];
        }elseif ( in_array($mType,['jackpot']) ){
            $minExpose = $maxProfit = 0;
            $profitLossData = $this->getProfitLossJackpot($betData,$redis);
            if( $profitLossData != null ){
                $minExpose = min($profitLossData);
                $maxProfit = max($profitLossData);
            }
            $returnData = [
                'expose' => $minExpose,
                'profit' => $maxProfit
            ];
        }elseif ( in_array($mType,['cricket_casino']) ){
            $minExpose = $maxProfit = 0; $profitLossData = [];
            for($n=0;$n<10;$n++){
                $profitLossData[] = $this->getProfitLossCricketCasino($betData,$n);
            }
            if( $profitLossData != null ){
                $minExpose = min($profitLossData);
                $maxProfit = max($profitLossData);
            }
            $returnData = [
                'expose' => $minExpose,
                'profit' => $maxProfit
            ];
        }

        return $returnData;
    }

    // Function check max profit limit
    public function checkMaxProfitLimit($newProfit,$newExpose,$betData)
    {
        try{
            $mType = $betData['mType'];
            if($newExpose < 0) { $newExpose = (-1) * $newExpose; }
            $isTrueExpose = true;
            $type = 'market is over'; $isTrue = false;
            $expose_type = 'market is over';

            $sportMarketLimit = $this->sportMarketLimit;
            if(!empty($sportMarketLimit)) {
                if ( $sportMarketLimit->max_profit_limit != 0 && ( $newProfit > $sportMarketLimit->max_profit_limit ) ) {
                    $isTrue = false;
                    $type = 'market is ' . $sportMarketLimit->max_profit_limit;
                }else{
                    $isTrue = true;
                    $type = 'market is ' . $sportMarketLimit->max_profit_limit;
                }
                if ($sportMarketLimit->max_expose_limit != 0 && ($newExpose > $sportMarketLimit->max_expose_limit)) {
                    $isTrueExpose = false;
                    $expose_type = 'market is ' . $sportMarketLimit->max_expose_limit;
                }
                if($isTrue == false){
                    return ['is_true' => $isTrue, 'msg' => 'Your max profit limit for this ' . $type . ' ! Bet can not placed!!'];
                }
            }

            $marketData = $this->marketData; $maxProfitLimitFancy = 1000000;
            if ( $marketData != null && isset( $marketData->max_profit_limit ) ) { $maxProfitLimitFancy = $marketData->max_profit_limit; }

            if ( $mType == 'meter' && (int)$newExpose > $maxProfitLimitFancy) {
                $isTrue = false; $type = 'market is ' . $maxProfitLimitFancy;
            }elseif((int)$newProfit > $maxProfitLimitFancy) {
                $isTrue = false; $type = 'market is ' . $maxProfitLimitFancy;
            }else{
                $isTrue = true; $type = 'market is ' . $maxProfitLimitFancy;
            }

            if($isTrueExpose == false){
                return ['is_true' => $isTrueExpose, 'msg' => 'Your max expose limit for this ' . $expose_type . ' ! Bet can not placed!!'];
            }else{
                return ['is_true' => $isTrue, 'msg' => 'Your max profit limit for this ' . $type . ' ! Bet can not placed!!'];
            }
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }
    }

    // Function setBookData
    public function setBookData($betData)
    {

        try{
            $marketId = $betData['marketId'];
            $eventId = $betData['eventId'];
            $mType = $betData['mType'];
            $userId = $betData['userId'];
            if( $mType == 'bookmaker' || $mType == 'virtual_cricket'){
                $tbl = 'tbl_book_bookmaker';
            }else{
                $tbl = 'tbl_book_matchodd';
            }

            $marketData = $this->marketData;
            if( $marketData != null && isset( $marketData->runners )){
                $runners = json_decode($marketData->runners);
                if( $runners != null ){
                    $i = 0; $bookDataArr = [];
                    foreach ( $runners as $runner ){
                        $bookDataArr[$i] = [
                            'uid' => $userId,
                            'eid' => $eventId,
                            'mid' => $marketId,
                        ];

                        if($mType == 'bookmaker'){
                            if( $betData['bType'] == 'lay' ){
                                if( $runner->secId == $betData['secId'] ){
                                    $book = ( $betData['bRate']*$betData['bSize'] )/100;
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                                }else{
                                    $book = $betData['bSize'];
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( $book, 2);
                                }

                            }else{

                                if( $runner->secId == $betData['secId'] ){
                                    $book = ( $betData['bRate']*$betData['bSize'] )/100;
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( $book, 2);
                                }else{
                                    $book = $betData['bSize'];
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                                }

                            }
                        }else{
                            if( $betData['bType'] == 'lay' ){
                                if( $runner->secId == $betData['secId'] ){
                                    $book = ( $betData['bRate'] - 1 )*$betData['bSize'];
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                                }else{
                                    $book = $betData['bSize'];
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( $book, 2);
                                }

                            }else{

                                if( $runner->secId == $betData['secId'] ){
                                    $book = ( $betData['bRate'] - 1 )*$betData['bSize'];
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( $book, 2);
                                }else{
                                    $book = $betData['bSize'];
                                    $bookDataArr[$i]['secId'] = $runner->secId;
                                    $bookDataArr[$i]['book'] = round( (-1)*$book , 2);
                                }

                            }
                        }
                        $i++;
                    }

                    if( $bookDataArr != null ){
                        $book = DB::table($tbl)->select(['book'])
                            ->where([['uid',$userId],['eid',$eventId],['mid',$marketId]])->first();
                        if( $book != null ){
                            foreach ( $bookDataArr as $bookData ){
                                $where = [['uid',$userId],['eid',$eventId],['mid',$marketId],['secId',$bookData['secId']]];
                                $bookCheck = DB::table($tbl)->select(['book'])->where($where)->first();
                                if( $bookCheck != null ){
                                    $newBook = round( ($bookCheck->book+$bookData['book']) , 2 );
                                    DB::table($tbl)->where($where)->update(['book'=>$newBook]);
                                }else{
                                    DB::table($tbl)->insert($bookData);
                                }
                            }

                        }else{
                            DB::table($tbl)->insert($bookDataArr);
                        }
                    }
                }
            }
        }catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }

    // Function Update User Bet Expose
    public function updateUserProfitExpose($betData,$rnrData)
    {
        $tbl = 'tbl_user_market_expose_2';
        $where = [['uid',$betData['userId']],['eid',$betData['eventId']],['mid',$betData['marketId']],['status', 1],['secId',$rnrData['secId']]];
        $userExpose = DB::table($tbl)->select(['id'])->where($where)->first();
        if( $userExpose != null ){
            $updateData = [
                'expose' => $rnrData['totalExpose'],
                'profit' => $rnrData['totalWin']
            ];
            DB::table($tbl)->where($where)->update($updateData);
        }else{
            $insertData = [
                'uid' => $betData['userId'],
                'eid' => $betData['eventId'],
                'mid' => $betData['marketId'],
                'sid' => $betData['sportId'],
                'mType' => $betData['mType'],
                'expose' => $rnrData['totalExpose'],
                'profit' => $rnrData['totalWin'],
                'secId' => $rnrData['secId'],
                'status' => 1,
            ];
            DB::table($tbl)->insert($insertData);
        }
    }

    // Function Update User Expose
    public function updateUserProfitExpose2($betData,$newExpose,$newProfit)
    {
        $tbl = 'tbl_user_market_expose';
        if( $newExpose != 0 ){ $newExpose = (-1)*( $newExpose ); }
        $where = [['uid',$betData['userId']],['eid',$betData['eventId']],['mid',$betData['marketId']],['status',1]];
        $userExpose = DB::table($tbl)->where($where)->first();
        if( $userExpose != null ){
            $updateData = [
                'expose' => $newExpose,
                'profit' => $newProfit
            ];
            DB::table($tbl)->where($where)->update($updateData);
        }else{
            $insertData = [
                'uid' => $betData['userId'],
                'systemId' => $betData['systemId'],
                'eid' => $betData['eventId'],
                'mid' => $betData['marketId'],
                'sid' => $betData['sportId'],
                'mType' => $betData['mType'],
                'expose' => $newExpose,
                'profit' => $newProfit,
                'status' => 1,
            ];
            DB::table($tbl)->insert($insertData);
        }
    }

    // Function to get the Client Commission Rate
    public function clientCommissionRate()
    {
        $CCR = 1;// Client Commission Rate
        $setting = DB::table('tbl_common_setting')->select(['value'])
            ->where([['key_name', 'CLIENT_COMMISSION_RATE'],['status',1]])->first();
        if( $setting != null ){
            $CCR = $setting->value;
        }
        return $CCR;
    }

    // Function to get the client IP address
    public function getClientIp() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
