<?php

namespace App\Http\Controllers\Operator;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class OperatorDetailController extends Controller
{

    public function detail(Request $request)
    {

    try{

      $response = ['status'=>0,'message'=>'Bad request !!!','data'=>null];

      if(isset($request->operatorId) && isset($request->system_key)){

      $operatorDetail = DB::table('tbl_system')->select('systemname','operatorId','logo','theme_id','system_key','status','remark')
                         ->where([['operatorId',$request->operatorId],['system_key',$request->system_key]])
                         ->first();
        
 
      	  if(!empty($operatorDetail)){

              if($operatorDetail->status == 1){

                 $logo = "http://new.dreamexch9.com/images/".$operatorDetail->logo;

                 $arr =[
                    
                    'systemname' => $operatorDetail->systemname,
                    'operatorId' => $operatorDetail->operatorId,
                    'logo' => $logo,
                    'theme_id' => $operatorDetail->theme_id,
                   // 'system_key' => $operatorDetail->system_key,

                 ];

              $response = ['status'=>1,'code'=>200,'message'=>'Data Found !!!','data'=>$arr];

              }else{

                  $response = ['status'=>1,'code'=>200,'message'=>'Data Found !!!','data'=>['error' => $operatorDetail->remark]];
              }

        	}else{
            $response = ['status'=>0,'code'=>200,'message'=>'System does not exist .Please contact your administrator !!','data'=>null];
          }
       }else{
       	      $response = ['status'=>1,'code'=>200,'message'=>'Data not Found','data'=>null];
       }


  return $response;

}catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response, 501);
        }

    }


 }