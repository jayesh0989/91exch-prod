<?php

namespace App\Http\Controllers;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Redis;


class AuthController extends Controller
{

    /**
     * Handles siteMode Request
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getCaptcha(Request $request)
    {
        $randomKey=   rand(1234, 9876);
        $date = date('Y-m-d H:i:s');
        if (isset($request->code)) {
            DB::table('tbl_captcha')
                ->where([['code', $request->code],['ip_address',$this->getClientIp()]])
                ->delete();
        }

        $addData = [
            'code' => $randomKey,
            'created_on' => $date,
            'status' => 1,
            'ip_address' => $this->getClientIp()
        ];
      $db_result= DB::table('tbl_captcha')->insert($addData);
      if($db_result) {
          $response = [
              'status' => 1,
              'data' => [
                  'captcha' => $randomKey,
                  'update_on' => $date,
              ]
          ];

          return response()->json($response, 200);
       } else {
            $response = [
                'status' => 0,
                'error' => [ 'message' => 'Something want wrong !' ]
            ];
            return response()->json($response);
        }
    }

    public function siteMode()
    {

       /* $data = DB::connection('mongodb')
            ->collection('tbl_casino_games')->get();
        //$data = Category::all();
        if( $data != null ){
            $response = [ "status" => 1 ,'code'=> 200, "data" => $data ];
        }else{
            $response = [ "status" => 1 ,'code'=> 200, "data" => null ];
        }
        return response()->json($response, 200);*/
        /* $cache = Redis::connection();
                  $sportList=$cache->set("SportList",'Test redis seweeerver');
       $sportList=$cache->get("SportList");
      print_r($sportList); exit;*/

        $setting = DB::table('tbl_common_setting')->select('value')
            ->where([['key_name','SITE_MAINTENANCE_MODE'],['status',1]])->first();

        if ( $setting != null ) {
            $data = json_decode($setting->value,16);
            $response = [
                'status' => 1,
                'data' => [
                    'siteMode' => trim($data['status']),
                    'message' => trim($data['message']),
                ]
            ];
            return response()->json($response, 200);
        } else {
            $response = [
                'status' => 0,
                'error' => [ 'message' => 'Something want wrong !' ]
            ];
            return response()->json($response);
        }

    }
    
    public function register(Request $request)
    {

        
       $this->validate($request, [
            'name' => 'required|min:3',
            'username' => 'required|unique:tbl_user',
            'systemId' => 'required',
             'parentId' => 'required',
            'password' => 'required|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'systemId'=>$request->systemId,
            'role' => $request->role,
            'parentId'=>$request->parentId,
            'password' => bcrypt($request->password)
        ]);

        $token = $user->createToken('TutsForWeb')->accessToken;

        return response()->json(['token' => $token], 200);
    }

   
    public function login(Request $request)
    {

        if( !$this->checkOrigin() ){
            return response()->json(['status' => 0, 'code' => 401, 'data' => null, 'message' => 'Bad request!']);
        }

        $key=$this->haskKey();
        $requestId=$request->tnp;
        $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
        $hashKey = $request->header('hash');
        $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
        if($hashAuth==1) {
            if($request->username=='ce02' || $request->username=='CE02'){
                $credentials = [
                    'username' => $request->username,
                    'password' => $request->password,
                    'status' => 1
                ];
            }else{
                $credentials = [
                    'username' => $request->username,
                    'password' => $request->password,
                    'systemId' => $request->systemId,
                    'status' => 1
                ];
            }

            // if ( strlen($request->recaptcha) != 4 ) {
            //     $response = [
            //         'status' => 0,'code' => 401, 'data' => null,
            //         'message' => 'Captcha verification failed ! Please try again.'
            //     ];
            //     return response()->json($response); exit;
            // }

            // check Captcha
           $ipAddress = $this->getClientIp();
           $captcha = DB::table('tbl_captcha')->select(['code','created_on'])
               ->where([['code', $request->recaptcha],['ip_address',$ipAddress ]])->first();

          /* if ( $captcha != null ) {
               // $cTime = strtotime( date('Y-m-d H:i:s') );
               // $createdOn = strtotime( trim($captcha->created_on) );
               // if( ( $cTime-$createdOn ) > 60 ){
               //     $response = [
               //         'status' => 0,'code' => 401, 'data' => null,
               //         'message' => 'Captcha Expired ! Please try again.'
               //     ];
               //     return response()->json($response); exit;
               // }else{
                   DB::table('tbl_captcha')
                       ->where([['code', $request->recaptcha],['ip_address',$ipAddress ]])->delete();
               // }
           }else{
               $response = [
                   'status' => 0,'code' => 401, 'data' => null,
                   'message' => 'Something wrong ! Please page refresh and try again.'
               ];
               return response()->json($response); exit;
           }*/


            $setting = DB::table('tbl_common_setting')->select('value')
                ->where([['key_name','SITE_MAINTENANCE_MODE'],['status',1]])->first();
            //print_r($setting);
            $data = json_decode($setting->value,16);
            if ( $data['status'] == 1 ) {
                // do nothing
            }else{
               
                return response()->json(['status' => 0, 'code' => 401, 'data' => null, 'message' => $data['message']]);
                exit;
            }
            
            if (auth()->attempt($credentials)) {

                if (isset($request->platform) && $request->platform != "web") {

                    $appVersion = DB::table('tbl_android_version')->select('version_name')->where('systemId', $request->systemId)->first();
                    if ($request->version_name != $appVersion->version_name) {
                        $response = ['status' => 5, 'code' => 200, 'Pls update you app'];
                        return $response;
                        exit;
                    }

                }
                $details = auth()->user();
                $userBlock = DB::table('tbl_user_block_status')
                    ->where([['uid', $details->id], ['type', 1]])->first();
 
                if (empty($userBlock) && $userBlock == null) {

                    if ($details->role == 4) {
                        usleep(100000);
                        if($details->id != 22121 && $details->id !=22122){

                                if (!empty($details)) {

                                    $accessToken = DB::table('oauth_access_tokens')->select('*')->where('user_id', $details->id)->delete();
                                }
                            }

                            $token = auth()->user()->createToken('TutsForWeb')->accessToken;
                            $data = [
                                "username" => $details->username,
                                "token" => $token,
                                "is_password_updated" => $details->is_password_updated,
                                "role" => $details->role,
                                "banner" => $this->getBannerData()
                            ];

                            $profile = User::where(['id' => $details->id])->first();
                            $profile->is_login = 1;
                            $profile->update();

                            $operatorInfo = DB::table('tbl_system')->select('*')
                                ->where([['operatorId', $details->systemId], ['status', 1]])
                                ->first();


                            $data['operatorId'] = $operatorInfo->operatorId;
                            $data['operator_name'] = $operatorInfo->systemname;
                            $data['logo'] = url('/operator_images') . '/' . $operatorInfo->logo;
                            $data['theme_id'] = $operatorInfo->theme_id;
                    } else {
                        return response()->json(['status' => 0, 'code' => 401, 'data' => null, 'message' => 'Incorrect username or password !']);
                    }


                    $data1['title'] = 'Login';
                    $data1['description'] = 'Login Activity';
                    $this->addNotification($data1);

                    $message = 'Login Activity | User: [' . $details->id . ' | ' . $details->username . '] | ipAddress: ' . $this->getClientIp();
                    $this->loginLog($message);

                    return response()->json(['status' => 1, 'data' => $data, "message" => "Logged In successfully!", 'code' => 200]);
                } else {
                    return response()->json(['status' => 1, 'data' => null, "message" => "This user has been blocked !!", 'code' => 200]);
                }
            } else {
                return response()->json(['status' => 0, 'code' => 401, 'data' => null, 'message' => 'Incorrect username or password !']);
            }
        }else{
            return response()->json(['status' => 0, 'code' => 401, 'data' => null, 'message' => 'Bad request!']);
        }
    }

  public function logout(Request $request)
    {

        if (Auth::check()) {
            Auth::user()->token()->revoke();
             $details = auth()->user();
             $profile = User::where(['id'=>$details->id])->first();
                $profile->is_login = 0;
                $profile->update();
            $data1['title'] = 'Logout';
            $data1['description'] = 'Logout Activity';
            $this->addNotification($data1);


            return response()->json([ "status" => 1 ,'code'=>200, "data" =>null , "message" => "Logout successfully !" ]); 
        }else{
            return response()->json([ "status" => 0 , 'code'=>401,"data" =>null , "message" => "Something went wrong !" ]);
        }


    }

/**
     * getClientIp
     */
    public function getClientIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }




//change password
public function changePassword(Request $request) {
    try{
    $response =  [ "status" => 0 ,"code" => 400 ,"data"=>null, "message" => "Bad request!"];
    $key=$this->haskKey();
    $requestId=$request->tnp;
    $generatehashkey = base64_encode(hash_hmac("sha256", $requestId, $key, true));
    $hashKey = $request->header('hash');
    $hashAuth=  $this->HashAuth($generatehashkey,$hashKey);
    if($hashAuth==1) {
        $data = $request->all();
        $user = Auth::guard('api')->user();

        if($user->id != 9986 && $user->id != 9987 && $user->id != 9988 && $user->id != 9989 && $user->id != 9990){
        
            if ($user != null || $user != '') {

                if ( isset( $data['password'] ) && isset( $data['cpassword'] ) ){
                    if ( $data['password'] != $data['cpassword'] ){
                        $message = 'Password & confirm password did not match !!';
                        $response = [ 'status' => 0, 'code' => 403, 'data' => null,'message' => $message];
                        return response()->json($response);
                    }
                    if( strlen($data['password']) < 6){
                        $message = 'Password length must be minimum 6 char !!';
                        $response = [ 'status' => 0, 'code' => 403, 'data' => null,'message' => $message];
                        return response()->json($response);
                    }

                    //$uppercase = preg_match('@[A-Z]@', $data['password']);
                    //$number    = preg_match('@[0-9]@', $data['password']);

                    // if( !$uppercase || !$number ) {
                    //     $message = 'Password must like this format. example : Xyz$@123';
                    //     $response = [ 'status' => 0, 'code' => 403, 'data' => null,'message' => $message];
                    //     return response()->json($response);
                    // }

                }

                if (isset($data['oldpassword']) && !empty($data['oldpassword']) && $data['oldpassword'] !== "" && $data['oldpassword'] !== 'undefined'
                    && isset($data['password']) && !empty($data['password']) && $data['password'] !== "" && $data['password'] !== 'undefined' ) {

                    if( $data['oldpassword'] == $data['password'] ){
                        $message = 'old password and new password must be different !!';
                        $response = [ 'status' => 0, 'code' => 403, 'data' => null,'message' => $message];
                        return response()->json($response);
                    }

                    $check = Auth::guard('web')->attempt([
                        'username' => $user->username,
                        'password' => $data['oldpassword']
                    ]);
                    if ($check) {
                        $user->password = bcrypt($data['password']);
                        $user->token()->revoke();
                        $user->is_password_updated = 1;
                        $token = $user->createToken('newToken')->accessToken;
                        $user->save();
                        $data1['title'] = 'Change Password';
                        $data1['description'] = 'Change Password Activity';
                        $this->addNotification($data1);

                        return response()->json(['status' => 1, 'code' => 200, 'data' => null, 'message' => 'Password changed successfully!']);
                        //return json_encode(array('token' => $token));

                    } else {
                        return response()->json(['status' => 0, 'code' => 403, 'data' => null, "message" => "Please enter correct old password.!!"]);
                    }
                }
            } else {
                return response()->json(['status' => 0, 'code' => 401, 'data' => null, "message" => "Unauthorized access !!"]);
            }
            
        }
    }else{
        return $response;
    }
    }
    catch (\Exception $e) {
            $response = $this->errorLog($e);
            return response()->json($response);
    }
}

   
public function details()
    {
        return response()->json(['user' => auth()->user()], 200);
    }

      public function getBannerData(){

        
        $banner = DB::table('tbl_banner')->select('url','width','height')
                  ->where([['status',1],['type',2]])
                  ->first();

        if( $banner != null ){
            return $banner;
        }else{
            return null;
        }

    }
}
