<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function haskKey(){
       return $HASH_KEY='0ac045353-de4b-54asd-92fb-4e5eab4f466';
    }

    /**
     * checkOrigin
     */
    public function checkOrigin()
    {
        return true;
        $removeChar = ["https://", "http://", "/"];
        if( isset($_SERVER['HTTP_ORIGIN']) ){
            $allowedOrigins = ['3.248.221.146','localhost:10001','dev-dreamexch9.com'];
            $origin = $_SERVER['HTTP_ORIGIN']; //$_SERVER['HTTP_REFERER'];
            $origin = str_replace($removeChar, '', $origin);
            if (in_array($origin, $allowedOrigins)) {
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * errorLog
     */
    public function errorLog($e)
    {
        $uid = Auth::id();
        $message = 'Error: '.$e->getMessage().' | File: '.$e->getFile().' | Line: '.$e->getLine().' | User ID: '.$uid;
        Log::channel('daily-log')->info($message);
        return [ 'status' => 0, 'error' => [ 'message' => $message ] ];
    }

    public function loginLog($message)
    {
       // $uid = Auth::id();
        Log::channel('login-activity-log')->info($message);
    }

    public function activityLog($message)
    {
        // $uid = Auth::id();
        Log::channel('activity-log')->info($message);
    }

    public function HashAuth($generatehashkey,$hashKey){
        $hash_active_status=1; //1=default active(Development)  0=auth validate complusury(Production)
        if($hash_active_status==0){
            if($generatehashkey==$hashKey){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 1;
        }
    }

    /**
     * Notification
     */
    public function addNotification($data)
    {
        if( isset($data['title']) && isset($data['description']) ){
            $user = Auth::user();
            $description = $data['description'].' by '.$user->username;
            if( isset($data['request'])) {
                $description =  $description . ' Request -'.$data['request'];
            }
            $updateArr = [
                'uid' => $user->id,
                'systemId' => $user->systemId,
                'title' => $data['title'],
                'description' => $description,
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $this->getClientServerIp()
            ];

            $message = json_encode($updateArr);
            Log::channel('activity-log')->info($message);

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status', 1], ['key_name', 'ACTIVITY_LOG_USER_ID']])->first();
            if ($setting != null) {
                $userArr = json_decode($setting->value);
                if( $userArr == null ){ $userArr = [1]; }
            } else { $userArr = [1]; }

            if( in_array($user->id,$userArr) ){
                DB::table('tbl_user_activity_log')->insert($updateArr);
                //return;
            }
            //return;
        }

    }

    /**
     * Notification Bet Place
     */
    public function addNotificationBetPlace($data)
    {

        if( isset($data['title']) && isset($data['description']) ){
            $user = Auth::user();
            $description = $data['description'].' by '.$user->username;
            $updateArr = [
                'uid' => $user->id,
                'systemId' => $user->systemId,
                'title' => $data['title'],
                'description' => $description,
                'request' => isset($data['request']) ? $data['request'] : 'No request data!',
                'response' => isset($data['response']) ? $data['response'] : 'No response data!',
                'currentOdds' => isset($data['currentOdds']) ? json_decode($data['currentOdds'],16) : 'No currentOdds data!',
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $this->getClientServerIp()
            ];

            $message = json_encode($updateArr);
            Log::channel('activity-log')->info($message);

            $setting = DB::table('tbl_common_setting')->select(['value'])
                ->where([['status', 1], ['key_name', 'ACTIVITY_LOG_USER_ID']])->first();
            if ($setting != null) {
                $userArr = json_decode($setting->value);
                if( $userArr == null ){ $userArr = [1]; }
            } else { $userArr = [1]; }

            if( isset($data['request']) ){
                $description = $description . ' - Request Data : ' . json_encode($data['request']);
            }
            if( isset($data['response']) ){
                $description = $description . ' - Response Data : ' . json_encode($data['response']);
            }
            if( isset($data['currentOdds']) ){
                $description = $description . ' - CurrentOdds Data : ' . $data['currentOdds'];
            }

            $updateArr = [
                'uid' => $user->id,
                'systemId' => $user->systemId,
                'title' => $data['title'],
                'description' => $description,
                'created_on' => date('Y-m-d H:i:s'),
                'ip_address' => $this->getClientServerIp()
            ];

            if( in_array($user->id,$userArr) ){
                DB::table('tbl_user_activity_log')->insert($updateArr);
            }
        }

    }

    /**
     * getClientIp
     */
    public function getClientServerIp()
    {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


}
