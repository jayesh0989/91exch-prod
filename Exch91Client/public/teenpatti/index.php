<html>
<head>
    <title>Live Teenpatti</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style>
        .tenn-iframe, .tenn-iframe{
            height: 100vh;
            overflow: hidden !important;
        }
        .tenn-iframe, .tenn-iframe .inner-scroll {
            overflow: hidden !important;
        }
        .tenn-iframe iframe{
            /* height: inherit !important;
            overflow: hidden !important; */
        }
        .tenn-po{
            position: relative;
            height: 400px;
        }
        .screen-detail{
            overflow: scroll !important;
        }
        .i-suspended{
            position: absolute;
            top: 0;
            text-align: center;
            vertical-align: middle;
            align-items: center;
            display: grid;
            width: 100%;
            height: 100%;
            margin: 0 auto;
            background: rgba(0, 0, 0, 0.6784313725490196);
            color: #fff;
            text-transform: uppercase;
            font-size: 17px;
            letter-spacing: 2px;
            font-weight: 900;
            cursor: not-allowed;
            display: none;
        }

        .teen-title{
            text-align: left;
            background: #000;
            color: #fff;
            padding:8px 8px;
        }
        .teen-title span{
            font-weight: 600;
            font-size: 15px;
        }
        .teen-title span em{
            font-style: normal;
        }
        .teen-title span.expose-teen{
            box-shadow: inset 0px 0px 10px 2px #252525;
            padding: 5px;
            background: #0f2327;
            border-radius: 3px;
        }
        .mmm-teen{
            background: #f9f9f9;
        }
        .mmm-teen span{
            font-weight: 500;
            font-size: 11px;
            text-transform: capitalize;
        }
        .mmm-teen span em{
            font-style: normal;
            font-size: 9px;
            font-weight: 600;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
    var tokenVal = '';
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    function createCookie(name, value, days) {
        var expires;
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        }
        else {
            expires = "";
        }
        document.cookie = escape(name) + "=" + escape(value) + expires + "; path=/";
    }

    $(document).ready(function(){

        window.localStorage.setItem('isLoad', true);
        if(window.localStorage.getItem('token') === null){
            var token = getParameterByName('token',window.location.href);
            window.localStorage.setItem('token', token);
            createCookie("token", token, "10");
            tokenVal = token;
            if(window.localStorage.getItem('isLoad')){
                window.location.href = window.location.origin+window.location.pathname;
                window.localStorage.setItem('isLoad', false);
            }
        }else{
            var token = getParameterByName('token',window.location.href);
            //window.localStorage.setItem('token', token);
            if(token !== null){
                window.localStorage.setItem('token', token);
                window.localStorage.setItem('isLoad', false);
                createCookie("token", token, "10");
                window.location.href = window.location.origin+window.location.pathname;
            }
        }

    });
    </script>
</head>
<body>
<div class="row teen-title">
    <div class="col-6">

    </div>
    <div class="col-6 text-right">
        <div>
            <span class="expose-teen update-expose-data"> Expose: <em>0</em></span>
        </div>
    </div>
</div>
<div class="row mmm-teen text-center update-event-data">
    <div class="col-4">
        <span class="update-event-data-min-stake">Min Stake: <em>1000</em></span>
    </div>
    <div class="col-4 px-0">
        <span class="update-event-data-max-stake">Max Stake: <em>10000</em></span>
    </div>
    <div class="col-4 pl-0">
        <span class="update-event-data-max-profit">Max Profit: <em>100000</em></span>
    </div>
</div>
<?php if( isset( $_COOKIE["token"] ) && $_COOKIE["token"] != ''){ ?>
    <div class="tenn-iframe cf" id="div-iframe">
        <iframe class="w-100" src="https://m2.fawk.app/#/splash-screen/<?php echo $_COOKIE["token"];?>/9262" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen style="height: 100%;"></iframe>
    </div>
<?php }else{ ?>
    <div class="i-suspended" id="div-iframe">
        suspended
    </div>
<?php } ?>
<div class="i-suspended" id="div-suspend">
    suspended
</div>
</body>
<script>

    function close_window() {
        if (confirm("Close Window?")) {
            window.close();
        }
    }

    var clearInt = setInterval(function() {
        clearInterval(clearInt); getUserData();
    }, 1000);

    function getUserData() {
        var token = window.localStorage.getItem('token') === null ? '<?php echo $_COOKIE["token"];?>' : window.localStorage.getItem('token');

        var request = $.ajax({
           // url: "http://livegame.dreamexch9.com/api/poker/app-user-auth",
            url: "https://api.91exch.com/api/poker/app-user-auth",
            type: "POST",
            data: {token : token, operatorId: 9262 },
            dataType: "json"
        });

        request.done(function(res) {
            if( res.status == 1 ){
                $('#div-suspend').hide();

                var exposeData = 'Expose: <em>'+res.data.expose+'</em>';
                $('.update-expose-data').html(exposeData);

                if( res.data.eventData != null ){
                    $('.update-event-data-min-stake').html('Min Stake: <em>'+res.data.eventData.min_stack+'</em>');
                    $('.update-event-data-max-stake').html('Max Stake: <em>'+res.data.eventData.max_stack+'</em>');
                    $('.update-event-data-max-profit').html('Max Profit: <em>'+res.data.eventData.max_profit_all_limit+'</em>');
                }
                setTimeout( getUserData, 5000);

            }else{
                $('#div-suspend').show();
                $('#div-iframe').hide();
            }
        });
    }


</script>
</html>

